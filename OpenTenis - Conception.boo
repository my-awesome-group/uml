<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{CFC1D5AD-E0DF-4218-BC3F-18980C26122F}" Label="" LastModificationDate="1576511528" Name="OpenTenis" Objects="777" Symbols="317" Target="Java" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.3.0.3248"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:ObjectID>
<a:Name>OpenTenis</a:Name>
<a:Code>OpenTenis</a:Code>
<a:CreationDate>1574700250</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=Yes
DisplayName=Yes
EnableTrans=Yes
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=int
DeftParm=int
DeftCont=java.util.Collection
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%.3:PARENT%_%COLUMN%
ColnFKNameUse=No

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No</a:ModelOptionsText>
<c:ObjectLanguage>
<o:Shortcut Id="o3">
<a:ObjectID>4AD26D48-E112-4480-A3D1-74F8172D7D0F</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1574700249</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700249</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ExtendedModelDefinitions>
<o:Shortcut Id="o4">
<a:ObjectID>16D6EB64-F643-478F-BD5B-80E069A09137</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1574700250</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700250</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetID>
<a:TargetClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetClassID>
</o:Shortcut>
</c:ExtendedModelDefinitions>
<c:Packages>
<o:Package Id="o5">
<a:ObjectID>DAE55521-F3CE-48A2-B83E-708BBB48130E</a:ObjectID>
<a:Name>Billetterie</a:Name>
<a:Code>billetterie</a:Code>
<a:CreationDate>1574700346</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576511331</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:ClassDiagrams>
<o:ClassDiagram Id="o6">
<a:ObjectID>F221E44B-5722-4BF1-AA67-3C7D28D9BBD9</a:ObjectID>
<a:Name>2. Classes - Billetterie</a:Name>
<a:Code>2__Classes___Billetterie</a:Code>
<a:CreationDate>1574935557</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510753</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=Yes
Generalization.DisplayName=No
Generalization.DisplayedRules=Yes
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Realization.DisplayedStereotype=Yes
Realization.DisplayName=No
Realization.DisplayedRules=Yes
Realization_SymbolLayout=
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=Yes
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Class.Stereotype=Yes
Class.Constraint=Yes
Class.Attributes=Yes
Class.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Class.Attributes._Limit=-3
Class.Operations=Yes
Class.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Class.Operations._Limit=-3
Class.InnerClassifiers=Yes
Class.Comment=No
Class.IconPicture=No
Class.TextStyle=No
Class_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de classe&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Interface.Stereotype=Yes
Interface.Constraint=Yes
Interface.Attributes=Yes
Interface.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Interface.Attributes._Limit=-3
Interface.Operations=Yes
Interface.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Interface.Operations._Limit=-3
Interface.InnerClassifiers=Yes
Interface.Comment=No
Interface.IconPicture=No
Interface.TextStyle=No
Interface_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom d&amp;#39;interface&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Port.IconPicture=No
Port.TextStyle=No
Port_SymbolLayout=
Association.RoleAMultiplicity=Yes
Association.RoleAName=Yes
Association.RoleAOrdering=Yes
Association.DisplayedStereotype=No
Association.DisplayName=No
Association.DisplayedRules=Yes
Association.RoleBMultiplicity=Yes
Association.RoleBName=Yes
Association.RoleBOrdering=Yes
Association.RoleMultiplicitySymbol=No
Association_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité A&quot; Attribute=&quot;RoleAMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle A&quot; Attribute=&quot;RoleAName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre A&quot; Attribute=&quot;RoleAOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité B&quot; Attribute=&quot;RoleBMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle B&quot; Attribute=&quot;RoleBName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre B&quot; Attribute=&quot;RoleBOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
RequireLink.DisplayedStereotype=Yes
RequireLink.DisplayName=No
RequireLink.DisplayedRules=Yes
RequireLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
PortShowName=Yes
PortShowType=No
PortShowMult=No

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:AssociationSymbol Id="o7">
<a:CreationDate>1574935668</a:CreationDate>
<a:ModificationDate>1575967782</a:ModificationDate>
<a:Rect>((-7386,18603), (7662,20749))</a:Rect>
<a:ListOfPoints>((-7386,19575),(2327,19575),(2327,19777),(7662,19777))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o8"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o9"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o10"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o11">
<a:CreationDate>1574936454</a:CreationDate>
<a:ModificationDate>1575296270</a:ModificationDate>
<a:DestinationTextOffset>(150, 587)</a:DestinationTextOffset>
<a:Rect>((-36308,17859), (-24647,20265))</a:Rect>
<a:ListOfPoints>((-24647,19033),(-29121,19033),(-29121,19091),(-36308,19091))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o12"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o13"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o14"/>
</c:Object>
</o:AssociationSymbol>
<o:GeneralizationSymbol Id="o15">
<a:CreationDate>1574937636</a:CreationDate>
<a:ModificationDate>1574937740</a:ModificationDate>
<a:Rect>((-34730,9657), (-26281,20335))</a:Rect>
<a:ListOfPoints>((-34730,9657),(-26281,9657),(-26281,20335))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o16"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o12"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o17"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o18">
<a:CreationDate>1574937637</a:CreationDate>
<a:ModificationDate>1574937750</a:ModificationDate>
<a:Rect>((-31414,5689), (-24879,18617))</a:Rect>
<a:ListOfPoints>((-31414,5689),(-24879,5689),(-24879,18617))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o19"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o12"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o20"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o21">
<a:CreationDate>1574937638</a:CreationDate>
<a:ModificationDate>1574937945</a:ModificationDate>
<a:Rect>((-24157,-261), (-23157,20441))</a:Rect>
<a:ListOfPoints>((-23657,-261),(-23657,20441))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o22"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o12"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o23"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o24">
<a:CreationDate>1574937639</a:CreationDate>
<a:ModificationDate>1575293341</a:ModificationDate>
<a:Rect>((-21979,5722), (-16749,18618))</a:Rect>
<a:ListOfPoints>((-16749,5722),(-21979,5722),(-21979,18618))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o25"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o12"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o26"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o27">
<a:CreationDate>1574937640</a:CreationDate>
<a:ModificationDate>1574937744</a:ModificationDate>
<a:Rect>((-20674,10089), (-14585,18900))</a:Rect>
<a:ListOfPoints>((-14585,10089),(-20674,10089),(-20674,18900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o28"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o12"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o29"/>
</c:Object>
</o:GeneralizationSymbol>
<o:AssociationSymbol Id="o30">
<a:CreationDate>1574937841</a:CreationDate>
<a:ModificationDate>1574937872</a:ModificationDate>
<a:Rect>((-33201,-7202), (-23868,-1190))</a:Rect>
<a:ListOfPoints>((-23868,-1190),(-32313,-1190),(-31974,-7202))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o22"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o31"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o32"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o33">
<a:CreationDate>1574937845</a:CreationDate>
<a:ModificationDate>1574937863</a:ModificationDate>
<a:Rect>((-36120,-8503), (-34202,4203))</a:Rect>
<a:ListOfPoints>((-35112,4203),(-35049,-8503))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o19"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o31"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o34"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o35">
<a:CreationDate>1575967790</a:CreationDate>
<a:ModificationDate>1575967790</a:ModificationDate>
<a:Rect>((-18862,18480), (-8212,20828))</a:Rect>
<a:ListOfPoints>((-18862,19654),(-8212,19654))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o12"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o8"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o36"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o12">
<a:CreationDate>1574935614</a:CreationDate>
<a:ModificationDate>1574936485</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-27611,15716), (-18215,22609))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o37"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o9">
<a:CreationDate>1574935616</a:CreationDate>
<a:ModificationDate>1575967704</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((4372,15941), (14902,22684))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o38"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o19">
<a:CreationDate>1574935616</a:CreationDate>
<a:ModificationDate>1574937750</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-37044,2960), (-27904,6781))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o39"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o25">
<a:CreationDate>1574935617</a:CreationDate>
<a:ModificationDate>1575293341</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17201,955), (-6285,6724))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o40"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o13">
<a:CreationDate>1574935619</a:CreationDate>
<a:ModificationDate>1574937257</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-41837,15147), (-34061,22864))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o41"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o16">
<a:CreationDate>1574935620</a:CreationDate>
<a:ModificationDate>1574937740</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-36384,7485), (-29486,11306))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o42"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o22">
<a:CreationDate>1574935621</a:CreationDate>
<a:ModificationDate>1574937755</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-27690,-3259), (-18550,562))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o43"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o28">
<a:CreationDate>1574935622</a:CreationDate>
<a:ModificationDate>1574937744</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17498,7544), (-11650,11365))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o44"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o31">
<a:CreationDate>1574937762</a:CreationDate>
<a:ModificationDate>1574937849</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-40086,-11017), (-29248,-5248))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o45"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o8">
<a:CreationDate>1575967710</a:CreationDate>
<a:ModificationDate>1575967782</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-11080,17665), (-3692,21485))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o46"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o47"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o47">
<a:ObjectID>C563506B-865C-4E35-A466-C3D937EEF81D</a:ObjectID>
<a:Name>1. Cas d&#39;Utilisation - Billetterie</a:Name>
<a:Code>1__Cas_d_Utilisation___Billetterie</a:Code>
<a:CreationDate>1574700346</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510502</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o48">
<a:CreationDate>1574933965</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((3449,14709), (59249,-30165))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o49">
<a:Text>Systeme Achat billets</a:Text>
<a:CreationDate>1574933972</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((3744,14371), (11695,10773))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:RectangleSymbol Id="o50">
<a:CreationDate>1574934410</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-19436,-32636), (59180,-60974))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o51">
<a:Text>Systeme Gestion Billet (back-office)</a:Text>
<a:CreationDate>1574934416</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-19744,-32795), (-5857,-36395))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:DependencySymbol Id="o52">
<a:CreationDate>1574934072</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((23417,-17790), (28517,-3090))</a:Rect>
<a:ListOfPoints>((25967,-3090),(25967,-17790))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o53"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o54"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o55"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o56">
<a:CreationDate>1574934198</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((12825,-4214), (26529,-2668))</a:Rect>
<a:ListOfPoints>((26529,-3914),(12825,-3914))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o53"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o57"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o58"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o59">
<a:CreationDate>1574934752</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-7688,-44855), (20212,-38735))</a:Rect>
<a:ListOfPoints>((20212,-39981),(-5138,-39981),(-5139,-44855))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o60"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o62"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o63">
<a:CreationDate>1574934753</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:CenterTextOffset>(8588, 75)</a:CenterTextOffset>
<a:Rect>((-4538,-52177), (19762,-46280))</a:Rect>
<a:ListOfPoints>((19762,-51006),(-4538,-51006),(-4339,-46280))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o64"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o65"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o66">
<a:CreationDate>1574934806</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((22537,-47781), (70387,-39306))</a:Rect>
<a:ListOfPoints>((70387,-47781),(70387,-39306),(22537,-39306))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o67"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o60"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o68"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o69">
<a:CreationDate>1574934808</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((24712,-53181), (70387,-47706))</a:Rect>
<a:ListOfPoints>((70387,-47706),(70387,-53181),(24712,-53181))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o67"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o64"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o70"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o71">
<a:CreationDate>1574935344</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((12675,-21111), (17775,-15111))</a:Rect>
<a:ListOfPoints>((15375,-15111),(15375,-17920),(15075,-17920),(15075,-21111))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o72"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o73"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o74"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o75">
<a:CreationDate>1574936009</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((17393,-2167), (24968,5304))</a:Rect>
<a:ListOfPoints>((24968,-921),(19943,-921),(19943,5304))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o53"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o76"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o77"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o78">
<a:CreationDate>1574936998</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((24668,-47808), (70043,-45108))</a:Rect>
<a:ListOfPoints>((70043,-47808),(70043,-45108),(24668,-45108))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o67"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o79"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o80"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o81">
<a:CreationDate>1574937001</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-3607,-45520), (13193,-43974))</a:Rect>
<a:ListOfPoints>((13193,-45220),(-3607,-45220))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o79"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o82"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o83">
<a:CreationDate>1574938080</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((13748,-57913), (70503,-47682))</a:Rect>
<a:ListOfPoints>((70503,-47682),(70503,-57913),(13748,-57913))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o67"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o84"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o85"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o86">
<a:CreationDate>1574938085</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-8443,-58337), (16287,-45516))</a:Rect>
<a:ListOfPoints>((16287,-57091),(-5893,-57091),(-5893,-45516))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o84"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o61"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o87"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o88">
<a:CreationDate>1575296752</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((12525,-12974), (17625,-5474))</a:Rect>
<a:ListOfPoints>((14250,-5474),(14250,-8901),(15900,-8901),(15900,-12974))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o57"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o72"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o89"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o90">
<a:CreationDate>1575297030</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((26925,-2774), (64275,6676))</a:Rect>
<a:ListOfPoints>((64275,6676),(26925,6676),(26925,-2774))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o91"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o53"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o92"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o93">
<a:CreationDate>1575297076</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((26700,-9074), (40050,-1903))</a:Rect>
<a:ListOfPoints>((26700,-3149),(37500,-3149),(37500,-9074))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o53"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o94"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o95"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o96">
<a:CreationDate>1575297119</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((35587,-22799), (40461,-12224))</a:Rect>
<a:ListOfPoints>((38024,-22799),(38024,-12224))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o97"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o94"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o98"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o99">
<a:CreationDate>1575297119</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((39825,-17549), (52162,-11337))</a:Rect>
<a:ListOfPoints>((49725,-17549),(49725,-11924),(39825,-11924))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o100"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o94"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o101"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseSymbol Id="o53">
<a:CreationDate>1574700915</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((22300,-5340), (29499,59))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o102"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o100">
<a:CreationDate>1574701045</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((45764,-21301), (55261,-15902))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o103"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o97">
<a:CreationDate>1574701060</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((32777,-25893), (43973,-20494))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o104"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o91">
<a:CreationDate>1574933338</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((61585,5402), (66384,9001))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o105"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o54">
<a:CreationDate>1574934059</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((22404,-20940), (29603,-15541))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o106"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o72">
<a:CreationDate>1574934183</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((11005,-16229), (19702,-10830))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o107"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o60">
<a:CreationDate>1574934479</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((8794,-41854), (25790,-36455))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o108"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o64">
<a:CreationDate>1574934505</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((13494,-54904), (28590,-49505))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o109"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o61">
<a:CreationDate>1574934538</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((-9933,-48530), (-336,-43131))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o110"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o67">
<a:CreationDate>1574934797</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((68137,-49356), (72936,-45757))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o111"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o73">
<a:CreationDate>1574935314</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((11812,-25161), (19011,-19762))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o112"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o76">
<a:CreationDate>1574935974</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((13119,3207), (23316,8606))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o113"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o79">
<a:CreationDate>1574936895</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((11972,-47732), (26566,-42333))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o114"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o84">
<a:CreationDate>1574938053</a:CreationDate>
<a:ModificationDate>1576511528</a:ModificationDate>
<a:Rect>((9921,-60313), (18918,-54914))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o115"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o57">
<a:CreationDate>1575296698</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((8876,-6973), (16773,-1574))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o116"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o94">
<a:CreationDate>1575297050</a:CreationDate>
<a:ModificationDate>1576511509</a:ModificationDate>
<a:Rect>((32026,-14098), (42823,-8699))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o117"/>
</c:Object>
</o:UseCaseSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:SequenceDiagrams>
<o:SequenceDiagram Id="o118">
<a:ObjectID>0F586500-E818-4672-9E78-EEC53024CBB5</a:ObjectID>
<a:Name>3. Sequence Achat de Billet - Billetterie</a:Name>
<a:Code>3__Sequence_Achat_de_Billet___Billetterie</a:Code>
<a:CreationDate>1575963081</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510530</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\SQD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
InteractionSymbol.IconPicture=No
InteractionSymbol.TextStyle=No
InteractionSymbol_SymbolLayout=
UMLObject.Stereotype=Yes
UMLObject.HeaderAlwaysVisible=Yes
UMLObject.IconPicture=No
UMLObject.TextStyle=No
UMLObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivationSymbol.IconPicture=No
ActivationSymbol.TextStyle=No
ActivationSymbol_SymbolLayout=
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
InteractionReference.IconPicture=No
InteractionReference.TextStyle=No
InteractionReference_SymbolLayout=
InteractionFragment.IconPicture=No
InteractionFragment.TextStyle=No
InteractionFragment_SymbolLayout=
Message.BeginTime=Yes
Message.Stereotype=Yes
Message.NameOrCode=No
Message.NameOrOper=No
Message.OperAndArgs=No
Message.CondAndOper=No
Message.CondOperAndSign=Yes
Message.EndTime=Yes
Message.ActivationAttachment=No
Message_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Heure de début&quot; Attribute=&quot;BeginTime&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;NameOrCode&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;Nom&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;No&quot; Display=&quot;VerticalRadios&quot; &gt;[CRLF]   &lt;StandardAttribute Name=&quot;Nom d&amp;#39;opération&quot; Attribute=&quot;NameOrOper&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Nom d&amp;#39;opération avec signature&quot; Attribute=&quot;OperAndArgs&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Expression de séquence et nom d&amp;#39;opération&quot; Attribute=&quot;CondAndOper&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Expression de séquence et nom d&amp;#39;opération avec signature&quot; Attribute=&quot;CondOperAndSign&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;/ExclusiveChoice&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Heure de fin&quot; Attribute=&quot;EndTime&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
IRefShowStrn=Yes
FragShowLife=Yes
ShowIntrSym=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SINT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=22000
Height=28800
Brush color=255 255 255
Fill Color=No
Brush style=4
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 208 208 232
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SQDOBJT]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,U
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACTVSYM]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=900
Height=2400
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\IREF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=1031
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 208 208 232
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\IFRG]
KWRDFont=Arial,8,N
KWRDFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 255
Fill Color=Yes
Brush style=4
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=208 208 232
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SQDMSSG]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:InteractionSymbol Id="o119">
<a:ModificationDate>1575972904</a:ModificationDate>
<a:Rect>((-21550,-52351), (43752,14392))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:BaseSymbol.Flags>4</a:BaseSymbol.Flags>
<a:LineColor>15257808</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:SequenceDiagram Ref="o118"/>
</c:Object>
</o:InteractionSymbol>
<o:MessageSymbol Id="o120">
<a:CreationDate>1575972822</a:CreationDate>
<a:ModificationDate>1575972822</a:ModificationDate>
<a:Rect>((-13848,-48375), (-975,-46829))</a:Rect>
<a:ListOfPoints>((-975,-48075),(-13848,-48075))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o123"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o124">
<a:CreationDate>1575972780</a:CreationDate>
<a:ModificationDate>1575972780</a:ModificationDate>
<a:Rect>((-1350,-45900), (38625,-44354))</a:Rect>
<a:ListOfPoints>((38625,-45600),(-1350,-45600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o125"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o126"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o127">
<a:CreationDate>1575972766</a:CreationDate>
<a:ModificationDate>1575972798</a:ModificationDate>
<a:Rect>((-825,-43800), (38391,-42329))</a:Rect>
<a:ListOfPoints>((-825,-43575),(38391,-43575))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o125"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o128"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o129">
<a:CreationDate>1575972665</a:CreationDate>
<a:ModificationDate>1575972665</a:ModificationDate>
<a:Rect>((-13848,-41025), (-750,-39479))</a:Rect>
<a:ListOfPoints>((-750,-40725),(-13848,-40725))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o130"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o131">
<a:CreationDate>1575972621</a:CreationDate>
<a:ModificationDate>1575972651</a:ModificationDate>
<a:Rect>((-1358,-38875), (6666,-36829))</a:Rect>
<a:ListOfPoints>((-946,-37275),(2654,-37275),(2654,-38875),(-946,-38875))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o132"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o133"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o134">
<a:CreationDate>1575972504</a:CreationDate>
<a:ModificationDate>1575972504</a:ModificationDate>
<a:Rect>((-13848,-36900), (-825,-35354))</a:Rect>
<a:ListOfPoints>((-13848,-36600),(-825,-36600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o135"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o136">
<a:CreationDate>1575972469</a:CreationDate>
<a:ModificationDate>1575972473</a:ModificationDate>
<a:Rect>((-13848,-35175), (-900,-33629))</a:Rect>
<a:ListOfPoints>((-900,-34875),(-13848,-34875))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o137"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o138">
<a:CreationDate>1575972441</a:CreationDate>
<a:ModificationDate>1575972441</a:ModificationDate>
<a:Rect>((-750,-32775), (38250,-31229))</a:Rect>
<a:ListOfPoints>((38250,-32475),(-750,-32475))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o139"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o140"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o141">
<a:CreationDate>1575972307</a:CreationDate>
<a:ModificationDate>1575972422</a:ModificationDate>
<a:Rect>((-1125,-31200), (38391,-29729))</a:Rect>
<a:ListOfPoints>((-1125,-30975),(38391,-30975))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o139"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o142"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o143">
<a:CreationDate>1575972277</a:CreationDate>
<a:ModificationDate>1575972725</a:ModificationDate>
<a:Rect>((-13848,-28236), (-946,-26765))</a:Rect>
<a:ListOfPoints>((-13848,-28011),(-946,-28011))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o121"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o144"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o145">
<a:CreationDate>1575972073</a:CreationDate>
<a:ModificationDate>1575972073</a:ModificationDate>
<a:Rect>((-13848,-25875), (-750,-24329))</a:Rect>
<a:ListOfPoints>((-750,-25575),(-13848,-25575))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o147"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o148">
<a:CreationDate>1575971928</a:CreationDate>
<a:ModificationDate>1575971928</a:ModificationDate>
<a:Rect>((-975,-22725), (30000,-21179))</a:Rect>
<a:ListOfPoints>((30000,-22425),(-975,-22425))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o149"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o150"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o151">
<a:CreationDate>1575971922</a:CreationDate>
<a:ModificationDate>1575971922</a:ModificationDate>
<a:Rect>((-900,-20775), (30106,-19304))</a:Rect>
<a:ListOfPoints>((-900,-20550),(30106,-20550))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o149"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o152"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o153">
<a:CreationDate>1575971868</a:CreationDate>
<a:ModificationDate>1575971868</a:ModificationDate>
<a:Rect>((-13848,-20250), (-675,-18704))</a:Rect>
<a:ListOfPoints>((-13848,-19950),(-675,-19950))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o154"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o155">
<a:CreationDate>1575971746</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-1350,-14025), (30150,-12479))</a:Rect>
<a:ListOfPoints>((30150,-13725),(-1350,-13725))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o156"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o157"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o158">
<a:CreationDate>1575971684</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-975,-12675), (30106,-11204))</a:Rect>
<a:ListOfPoints>((-975,-12450),(30106,-12450))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o156"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o159"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o160">
<a:CreationDate>1575971611</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-13848,-12150), (-900,-10604))</a:Rect>
<a:ListOfPoints>((-13848,-11850),(-900,-11850))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o161"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o162">
<a:CreationDate>1575971480</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-975,-19276), (30450,-17730))</a:Rect>
<a:ListOfPoints>((30450,-18976),(-975,-18976))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o163"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o164"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o165">
<a:CreationDate>1575971462</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-900,-17596), (30106,-16125))</a:Rect>
<a:ListOfPoints>((-900,-17371),(30106,-17371))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o163"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o166"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o167">
<a:CreationDate>1575971011</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-525,-10200), (20100,-8654))</a:Rect>
<a:ListOfPoints>((20100,-9900),(-525,-9900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o169"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o170">
<a:CreationDate>1575971005</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((20400,-8850), (30000,-7304))</a:Rect>
<a:ListOfPoints>((30000,-8550),(20400,-8550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o171"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o172"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o173">
<a:CreationDate>1575970984</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((20325,-6600), (30106,-5129))</a:Rect>
<a:ListOfPoints>((20325,-6375),(30106,-6375))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o171"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o174"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o175">
<a:CreationDate>1575970980</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((-900,-5615), (20315,-4144))</a:Rect>
<a:ListOfPoints>((-900,-5390),(20315,-5390))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o176"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o177">
<a:CreationDate>1575970450</a:CreationDate>
<a:ModificationDate>1575972053</a:ModificationDate>
<a:Rect>((-13848,-2480), (-946,-1009))</a:Rect>
<a:ListOfPoints>((-13848,-2255),(-946,-2255))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o146"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o178"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o179">
<a:CreationDate>1575967630</a:CreationDate>
<a:ModificationDate>1575970389</a:ModificationDate>
<a:Rect>((-13848,524), (-869,2070))</a:Rect>
<a:ListOfPoints>((-869,824),(-13848,824))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o180"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o181"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o182">
<a:CreationDate>1575967388</a:CreationDate>
<a:ModificationDate>1575967388</a:ModificationDate>
<a:Rect>((-617,2119), (8908,3665))</a:Rect>
<a:ListOfPoints>((8908,2419),(-617,2419))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o183"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o180"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o184"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o185">
<a:CreationDate>1575967382</a:CreationDate>
<a:ModificationDate>1575967382</a:ModificationDate>
<a:Rect>((-842,4444), (9132,5915))</a:Rect>
<a:ListOfPoints>((-842,4669),(9132,4669))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o180"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o183"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o186"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o187">
<a:CreationDate>1575963769</a:CreationDate>
<a:ModificationDate>1575970389</a:ModificationDate>
<a:Rect>((-13848,5607), (-946,7078))</a:Rect>
<a:ListOfPoints>((-13848,5832),(-946,5832))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o122"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o180"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o188"/>
</c:Object>
</o:MessageSymbol>
<o:ActorSequenceSymbol Id="o122">
<a:CreationDate>1575963401</a:CreationDate>
<a:ModificationDate>1575967835</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16248,7942), (-11449,11541))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o189">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((-13848,-50700), (-13748,7942))</a:Rect>
<a:ListOfPoints>((-13848,7942),(-13848,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:Actor Ref="o105"/>
</c:Object>
</o:ActorSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o190">
<a:CreationDate>1575963449</a:CreationDate>
<a:ModificationDate>1575972621</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3346,7942), (1453,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o191">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((-946,-50700), (-846,7942))</a:Rect>
<a:ListOfPoints>((-946,7942),(-946,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o180">
<a:CreationDate>1575963769</a:CreationDate>
<a:ModificationDate>1575970389</a:ModificationDate>
<a:Rect>((-1396,824), (-496,5842))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o146">
<a:CreationDate>1575970450</a:CreationDate>
<a:ModificationDate>1575972073</a:ModificationDate>
<a:Rect>((-1396,-25575), (-496,-2245))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o121">
<a:CreationDate>1575972277</a:CreationDate>
<a:ModificationDate>1575972822</a:ModificationDate>
<a:Rect>((-1396,-48075), (-496,-28001))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o132">
<a:CreationDate>1575972621</a:CreationDate>
<a:ModificationDate>1575972651</a:ModificationDate>
<a:Rect>((-1096,-40425), (-196,-38865))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o192"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o193">
<a:CreationDate>1575963560</a:CreationDate>
<a:ModificationDate>1575972766</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((35992,7942), (40791,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o194">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((38391,-50700), (38491,7942))</a:Rect>
<a:ListOfPoints>((38391,7942),(38391,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o139">
<a:CreationDate>1575972307</a:CreationDate>
<a:ModificationDate>1575972441</a:ModificationDate>
<a:Rect>((37941,-32475), (38841,-30965))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o125">
<a:CreationDate>1575972766</a:CreationDate>
<a:ModificationDate>1575972798</a:ModificationDate>
<a:Rect>((37941,-45600), (38841,-43565))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o195"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o196">
<a:CreationDate>1575963585</a:CreationDate>
<a:ModificationDate>1575970980</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((17916,7942), (22715,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o197">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((20315,-50700), (20415,7942))</a:Rect>
<a:ListOfPoints>((20315,7942),(20315,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o168">
<a:CreationDate>1575970980</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((19865,-9900), (20765,-5380))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o198"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o199">
<a:CreationDate>1575963919</a:CreationDate>
<a:ModificationDate>1575967835</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((6733,7942), (11532,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o200">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((9132,-50700), (9232,7942))</a:Rect>
<a:ListOfPoints>((9132,7942),(9132,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o183">
<a:CreationDate>1575967382</a:CreationDate>
<a:ModificationDate>1575967388</a:ModificationDate>
<a:Rect>((8682,2419), (9582,4679))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o201"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o202">
<a:CreationDate>1575967833</a:CreationDate>
<a:ModificationDate>1575971922</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((27707,7942), (32506,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o203">
<a:ModificationDate>1575972838</a:ModificationDate>
<a:Rect>((30106,-50700), (30206,7942))</a:Rect>
<a:ListOfPoints>((30106,7942),(30106,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o171">
<a:CreationDate>1575970984</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((29656,-8550), (30556,-6365))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o163">
<a:CreationDate>1575971462</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((29656,-18976), (30556,-17361))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o156">
<a:CreationDate>1575971684</a:CreationDate>
<a:ModificationDate>1575971843</a:ModificationDate>
<a:Rect>((29656,-13725), (30556,-12440))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o149">
<a:CreationDate>1575971922</a:CreationDate>
<a:ModificationDate>1575971928</a:ModificationDate>
<a:Rect>((29656,-22425), (30556,-20540))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o204"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:InteractionFragmentSymbol Id="o205">
<a:CreationDate>1575970624</a:CreationDate>
<a:ModificationDate>1575972049</a:ModificationDate>
<a:RegionConditionPositionList>((2500,300),(300,300))</a:RegionConditionPositionList>
<a:Rect>((-20325,-23402), (42751,-3902))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:AttachedLifelines>
<o:UMLObjectSequenceSymbol Ref="o190"/>
<o:UMLObjectSequenceSymbol Ref="o193"/>
<o:UMLObjectSequenceSymbol Ref="o196"/>
<o:UMLObjectSequenceSymbol Ref="o199"/>
<o:UMLObjectSequenceSymbol Ref="o202"/>
<o:ActorSequenceSymbol Ref="o122"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o206"/>
</c:Object>
</o:InteractionFragmentSymbol>
<o:InteractionFragmentSymbol Id="o207">
<a:CreationDate>1575972324</a:CreationDate>
<a:ModificationDate>1575972457</a:ModificationDate>
<a:RegionConditionPositionList>((2500,300))</a:RegionConditionPositionList>
<a:Rect>((-19875,-33451), (42075,-29400))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:AttachedLifelines>
<o:ActorSequenceSymbol Ref="o122"/>
<o:UMLObjectSequenceSymbol Ref="o190"/>
<o:UMLObjectSequenceSymbol Ref="o193"/>
<o:UMLObjectSequenceSymbol Ref="o196"/>
<o:UMLObjectSequenceSymbol Ref="o199"/>
<o:UMLObjectSequenceSymbol Ref="o202"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o208"/>
</c:Object>
</o:InteractionFragmentSymbol>
<o:InteractionFragmentSymbol Id="o209">
<a:CreationDate>1575972742</a:CreationDate>
<a:ModificationDate>1575972742</a:ModificationDate>
<a:RegionConditionPositionList>((2500,300))</a:RegionConditionPositionList>
<a:Rect>((-20025,-46350), (41400,-42150))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<c:AttachedLifelines>
<o:ActorSequenceSymbol Ref="o122"/>
<o:UMLObjectSequenceSymbol Ref="o190"/>
<o:UMLObjectSequenceSymbol Ref="o193"/>
<o:UMLObjectSequenceSymbol Ref="o196"/>
<o:UMLObjectSequenceSymbol Ref="o199"/>
<o:UMLObjectSequenceSymbol Ref="o202"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o210"/>
</c:Object>
</o:InteractionFragmentSymbol>
</c:Symbols>
</o:SequenceDiagram>
</c:SequenceDiagrams>
<c:InteractionFragments>
<o:InteractionFragment Id="o206">
<a:ObjectID>BF525ABE-EF4B-46A9-A5DB-BC87B611AECF</a:ObjectID>
<a:CreationDate>1575970624</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970649</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>1000</a:Size>
<a:FragmentType>alt</a:FragmentType>
<c:Regions>
<o:InteractionFragment Id="o211">
<a:ObjectID>88CD167E-DB95-48B3-A13F-70CCC88E3226</a:ObjectID>
<a:CreationDate>1575970635</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971589</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>11851</a:Size>
<a:FragmentType>opt</a:FragmentType>
<a:Condition>Cours Central = oui</a:Condition>
</o:InteractionFragment>
<o:InteractionFragment Id="o212">
<a:ObjectID>16F73E1F-58B8-4B7A-97FB-6862C4C929D5</a:ObjectID>
<a:CreationDate>1575970635</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972049</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>7650</a:Size>
<a:FragmentType>opt</a:FragmentType>
<a:Condition>Cours Central = non</a:Condition>
</o:InteractionFragment>
</c:Regions>
</o:InteractionFragment>
<o:InteractionFragment Id="o208">
<a:ObjectID>270B14D4-A7FF-4B2B-8C93-95FF274A26BC</a:ObjectID>
<a:CreationDate>1575972324</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972457</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>4051</a:Size>
<a:FragmentType>loop</a:FragmentType>
<a:Condition>foreach billet</a:Condition>
</o:InteractionFragment>
<o:InteractionFragment Id="o210">
<a:ObjectID>260581B7-2BCC-4535-BD5A-7892E287AA3D</a:ObjectID>
<a:CreationDate>1575972742</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972755</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>1000</a:Size>
<a:FragmentType>loop</a:FragmentType>
<a:Condition>foreach bilet</a:Condition>
</o:InteractionFragment>
</c:InteractionFragments>
<c:Classes>
<o:Class Id="o37">
<a:ObjectID>4EEE8D4C-8BE3-4BBA-A210-59505407B15A</a:ObjectID>
<a:Name>Billet</a:Name>
<a:Code>Billet</a:Code>
<a:CreationDate>1574935614</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970785</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o213">
<a:ObjectID>E4B5198F-16A2-4CD1-ACB2-38B861DAB6E1</a:ObjectID>
<a:Name>numBillet</a:Name>
<a:Code>numBillet</a:Code>
<a:CreationDate>1574935752</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574935771</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o214">
<a:ObjectID>E2C74504-5268-47F2-B610-C998A2F6D451</a:ObjectID>
<a:Name>dateValidite</a:Name>
<a:Code>dateValidite</a:Code>
<a:CreationDate>1574936210</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936246</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o215">
<a:ObjectID>E05497DA-781A-4D1B-B0B1-43C306F48F13</a:ObjectID>
<a:Name>terrain</a:Name>
<a:Code>terrain</a:Code>
<a:CreationDate>1575292768</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292786</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o216">
<a:ObjectID>6508E540-AC00-4BCC-B379-3D697A8C4A32</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1574938324</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938343</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o38">
<a:ObjectID>F66AA8FA-5184-42EE-8363-2D6E59CB2EC4</a:ObjectID>
<a:Name>Categorie</a:Name>
<a:Code>Categorie</a:Code>
<a:CreationDate>1574935616</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967781</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o217">
<a:ObjectID>C7554FE8-17C9-4D17-8890-0C311D8668E1</a:ObjectID>
<a:Name>numCat</a:Name>
<a:Code>numCat</a:Code>
<a:CreationDate>1574936515</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936522</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o218">
<a:ObjectID>296A9ABD-4E96-40C1-A93C-2D49E2813BE5</a:ObjectID>
<a:Name>prix</a:Name>
<a:Code>prix</a:Code>
<a:CreationDate>1574936524</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936548</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o219">
<a:ObjectID>25803A13-D6C8-4F92-B55E-306E7A1E1A9D</a:ObjectID>
<a:Name>nbPlaces</a:Name>
<a:Code>nbPlaces</a:Code>
<a:CreationDate>1575967478</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967487</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o220">
<a:ObjectID>7A041DB0-0A6F-4FC2-ACE6-9DF3CB116170</a:ObjectID>
<a:Name>definirPrixCat</a:Name>
<a:Code>definirPrixCat</a:Code>
<a:CreationDate>1575292796</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o221">
<a:ObjectID>ECBCBD6B-93DB-4C70-95DA-118C53BF2E3E</a:ObjectID>
<a:Name>definirNbPlaces</a:Name>
<a:Code>definirNbPlaces</a:Code>
<a:CreationDate>1575293229</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575293258</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o39">
<a:ObjectID>76961808-EC73-4EC6-AB16-677FC30C7CB9</a:ObjectID>
<a:Name>BilletSolidarite</a:Name>
<a:Code>BilletSolidarite</a:Code>
<a:CreationDate>1574935616</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938437</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Operations>
<o:Operation Id="o222">
<a:ObjectID>5D880996-D1E0-4007-80CE-3F6E1923AACE</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1574938427</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938437</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o40">
<a:ObjectID>403336A3-FC17-4F52-9A62-04289117AF7F</a:ObjectID>
<a:Name>BilletLicencie</a:Name>
<a:Code>BilletLicencie</a:Code>
<a:CreationDate>1574935617</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575293173</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o223">
<a:ObjectID>EFF751C0-AC0C-4665-90FA-BF685FF18188</a:ObjectID>
<a:Name>numLicence</a:Name>
<a:Code>numLicence</a:Code>
<a:CreationDate>1574937424</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937603</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o224">
<a:ObjectID>BDF3A9A6-0BF4-4004-8798-6ADD044B3D6D</a:ObjectID>
<a:Name>reduction</a:Name>
<a:Code>reduction</a:Code>
<a:CreationDate>1575292974</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292994</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Static>1</a:Static>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o225">
<a:ObjectID>8BFA8366-DE3F-48D3-9987-DB0E75BE7B2C</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1574938381</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938408</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o226">
<a:ObjectID>1399EE53-6D1D-4165-AD65-1B687C19826A</a:ObjectID>
<a:Name>definirReduction</a:Name>
<a:Code>definirReduction</a:Code>
<a:CreationDate>1575293159</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575293180</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o41">
<a:ObjectID>E84BE3D0-65DB-4B82-9AA7-049862E6C6A2</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1574935619</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296292</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o227">
<a:ObjectID>FB2CC89A-27D6-4E26-9172-8CEE599275C6</a:ObjectID>
<a:Name>idClient</a:Name>
<a:Code>idClient</a:Code>
<a:CreationDate>1575292512</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292522</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o228">
<a:ObjectID>EC964C60-F0CE-403E-BEA9-A056822016BD</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1575292524</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292680</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o229">
<a:ObjectID>7981E074-7A55-457B-9B0F-826BFBF9C1E8</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1575292524</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292680</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o230">
<a:ObjectID>BF285AA0-2BAE-4ADA-95F4-535CBC24A522</a:ObjectID>
<a:Name>mail</a:Name>
<a:Code>mail</a:Code>
<a:CreationDate>1575292524</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292680</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o231">
<a:ObjectID>E23ABA8A-BF5C-49AA-8F87-B5D6CD41D382</a:ObjectID>
<a:Name>adresse</a:Name>
<a:Code>adresse</a:Code>
<a:CreationDate>1575292543</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575292680</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o42">
<a:ObjectID>E0610823-0959-42A5-A75B-2A56533CA1C9</a:ObjectID>
<a:Name>BilletGrandPublic</a:Name>
<a:Code>BilletGrandPublic</a:Code>
<a:CreationDate>1574935620</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937162</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o43">
<a:ObjectID>7BEEA596-AC35-460D-81E9-4F291027E2A9</a:ObjectID>
<a:Name>BilletPromo</a:Name>
<a:Code>BilletPromo</a:Code>
<a:CreationDate>1574935621</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938425</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Operations>
<o:Operation Id="o232">
<a:ObjectID>9E384C10-586E-40C6-9279-262D18086A64</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1574938410</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938425</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o44">
<a:ObjectID>926FCF32-8673-4EF1-ACCC-6E5C08F33460</a:ObjectID>
<a:Name>BilletBigMatch</a:Name>
<a:Code>BilletBigMatch</a:Code>
<a:CreationDate>1574935622</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937216</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o45">
<a:ObjectID>395E8D46-63B0-4922-AA9A-625225065AD7</a:ObjectID>
<a:Name>CodePromo</a:Name>
<a:Code>CodePromo</a:Code>
<a:CreationDate>1574937762</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575293195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o233">
<a:ObjectID>6BF40EE0-DF17-4A0D-B33A-18BE212FCA8E</a:ObjectID>
<a:Name>nbUtilisation</a:Name>
<a:Code>nbUtilisation</a:Code>
<a:CreationDate>1574937764</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937836</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o234">
<a:ObjectID>40A18EA0-5E43-4848-930B-C4EDA21C755B</a:ObjectID>
<a:Name>code</a:Name>
<a:Code>code</a:Code>
<a:CreationDate>1574937764</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937836</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o235">
<a:ObjectID>1A8F5E0B-53E1-4F85-A847-7366DAAAFC1C</a:ObjectID>
<a:Name>reduction</a:Name>
<a:Code>reduction</a:Code>
<a:CreationDate>1574937764</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937836</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o236">
<a:ObjectID>D80FE90C-93DB-4FC2-9C6C-4AD516FA5657</a:ObjectID>
<a:Name>definirReduction</a:Name>
<a:Code>definirReduction</a:Code>
<a:CreationDate>1575293182</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575293201</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o46">
<a:ObjectID>13AB075E-A6D6-4D49-A1AF-A5CFCD205DD9</a:ObjectID>
<a:Name>Place</a:Name>
<a:Code>Place</a:Code>
<a:CreationDate>1575967710</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970785</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o237">
<a:ObjectID>5932AF78-58DC-4B65-8EAB-1916F977FEBB</a:ObjectID>
<a:Name>numPlace</a:Name>
<a:Code>numPlace</a:Code>
<a:CreationDate>1575967716</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967726</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o10">
<a:ObjectID>CF63A57A-40EB-4328-B41B-49EEBC58553D</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>association1</a:Code>
<a:CreationDate>1574935668</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967781</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o38"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o46"/>
</c:Object2>
</o:Association>
<o:Association Id="o14">
<a:ObjectID>DCDCA8A7-C57A-4C71-9AD7-EB1F78892F96</a:ObjectID>
<a:Name>Association_2</a:Name>
<a:Code>association2</a:Code>
<a:CreationDate>1574936454</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296292</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>0..3</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o41"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o37"/>
</c:Object2>
</o:Association>
<o:Association Id="o32">
<a:ObjectID>A4F8A05A-9BAE-4BC5-9989-97E0CBB0F1D1</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>association3</a:Code>
<a:CreationDate>1574937841</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937910</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o45"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o43"/>
</c:Object2>
</o:Association>
<o:Association Id="o34">
<a:ObjectID>A52A5512-6A6F-44C5-9640-CCEE912F7DA5</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>association4</a:Code>
<a:CreationDate>1574937845</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937920</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o45"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o39"/>
</c:Object2>
</o:Association>
<o:Association Id="o36">
<a:ObjectID>A0A1DB54-88AB-439A-A71D-CD038F45F1A1</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>association5</a:Code>
<a:CreationDate>1575967790</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970785</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o46"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o37"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:Generalizations>
<o:Generalization Id="o17">
<a:ObjectID>31955DAD-5C3E-41DD-BB86-FAEC660E8701</a:ObjectID>
<a:Name>Generalisation_6</a:Name>
<a:Code>Generalisation_6</a:Code>
<a:CreationDate>1574937636</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937636</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o37"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o42"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o20">
<a:ObjectID>86034A41-DAB8-4F8D-9D65-68922284C136</a:ObjectID>
<a:Name>Generalisation_7</a:Name>
<a:Code>Generalisation_7</a:Code>
<a:CreationDate>1574937637</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937637</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o37"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o39"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o23">
<a:ObjectID>A5B0EE3F-1C38-4321-9A0E-17F80924AA4F</a:ObjectID>
<a:Name>Generalisation_8</a:Name>
<a:Code>Generalisation_8</a:Code>
<a:CreationDate>1574937638</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937638</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o37"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o43"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o26">
<a:ObjectID>2105DC48-5D2C-44AB-8806-9FF15D9A36A0</a:ObjectID>
<a:Name>Generalisation_9</a:Name>
<a:Code>Generalisation_9</a:Code>
<a:CreationDate>1574937639</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937639</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o37"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o40"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o29">
<a:ObjectID>04256870-0970-4F54-BA96-5F52B420D7D9</a:ObjectID>
<a:Name>Generalisation_10</a:Name>
<a:Code>Generalisation_10</a:Code>
<a:CreationDate>1574937640</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937640</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o37"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o44"/>
</c:Object2>
</o:Generalization>
</c:Generalizations>
<c:Dependencies>
<o:Dependency Id="o55">
<a:ObjectID>6A649A07-C0E6-4A84-B5F1-CA6F4606BCF8</a:ObjectID>
<a:Name>Dependance_4</a:Name>
<a:Code>Dependance_4</a:Code>
<a:CreationDate>1574934072</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934077</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o106"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o102"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o58">
<a:ObjectID>8DF18C34-8B0F-4192-858A-DFC7703A54B5</a:ObjectID>
<a:Name>Dependance_5</a:Name>
<a:Code>Dependance_5</a:Code>
<a:CreationDate>1574934198</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296732</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o116"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o102"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o62">
<a:ObjectID>5034D996-8996-4E9D-A3B1-1D307961D791</a:ObjectID>
<a:Name>Dependance_9</a:Name>
<a:Code>Dependance_9</a:Code>
<a:CreationDate>1574934752</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934764</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o110"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o108"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o65">
<a:ObjectID>2B36EBF1-4018-40ED-8CDC-672B161185E6</a:ObjectID>
<a:Name>Dependance_10</a:Name>
<a:Code>Dependance_10</a:Code>
<a:CreationDate>1574934753</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934768</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o110"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o109"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o74">
<a:ObjectID>8EE6F839-163D-4635-9922-3BAF70838E9C</a:ObjectID>
<a:Name>Dependance_11</a:Name>
<a:Code>Dependance_11</a:Code>
<a:CreationDate>1574935344</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574935355</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o112"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o107"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o77">
<a:ObjectID>F0324474-3C4B-4486-A0A0-E033430977A4</a:ObjectID>
<a:Name>Dependance_12</a:Name>
<a:Code>Dependance_12</a:Code>
<a:CreationDate>1574936009</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936021</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o113"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o102"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o82">
<a:ObjectID>0F16B282-A016-4764-A05D-851B61F42DD4</a:ObjectID>
<a:Name>Dependance_13</a:Name>
<a:Code>Dependance_13</a:Code>
<a:CreationDate>1574937001</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937007</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o110"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o114"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o87">
<a:ObjectID>37A7DD45-7249-469E-8694-F18F71651A34</a:ObjectID>
<a:Name>Dependance_14</a:Name>
<a:Code>Dependance_14</a:Code>
<a:CreationDate>1574938085</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938096</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o110"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o115"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o89">
<a:ObjectID>A847684D-F5B5-4CF0-9939-584AF8C62EB2</a:ObjectID>
<a:Name>Dependance_15</a:Name>
<a:Code>Dependance_15</a:Code>
<a:CreationDate>1575296752</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296763</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o116"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o95">
<a:ObjectID>6D580827-A152-4F8C-9C1D-812FB8E7CC01</a:ObjectID>
<a:Name>Dependance_16</a:Name>
<a:Code>Dependance_16</a:Code>
<a:CreationDate>1575297076</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o117"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o102"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o98">
<a:ObjectID>F5DF42C4-C623-42D6-8394-E63F71E16B61</a:ObjectID>
<a:Name>Dependance_17</a:Name>
<a:Code>Dependance_17</a:Code>
<a:CreationDate>1575297119</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297133</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>extend</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o117"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o104"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o101">
<a:ObjectID>37790465-A0C5-4DD7-BD3B-21081876720F</a:ObjectID>
<a:Name>Dependance_18</a:Name>
<a:Code>Dependance_18</a:Code>
<a:CreationDate>1575297119</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>extend</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o117"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o103"/>
</c:Object2>
</o:Dependency>
</c:Dependencies>
<c:Actors>
<o:Actor Id="o105">
<a:ObjectID>5DC3DC66-82F9-47D7-9DAF-5197E9DFC96C</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1574933338</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574933347</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
<o:Actor Id="o111">
<a:ObjectID>F543F67C-015E-4FA6-8E3C-0EE1D7846EC4</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1574934797</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934800</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
</c:Actors>
<c:UseCases>
<o:UseCase Id="o102">
<a:ObjectID>58DC3C76-609A-4EE2-B3BD-B400CCD4ECCF</a:ObjectID>
<a:Name>Acheter billet</a:Name>
<a:Code>Acheter_billet</a:Code>
<a:CreationDate>1574700915</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o103">
<a:ObjectID>E7EB89CC-C44D-4348-8BD5-0DE6825979CC</a:ObjectID>
<a:Name>Entrer code promo</a:Name>
<a:Code>Entrer_code_promo</a:Code>
<a:CreationDate>1574701045</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o104">
<a:ObjectID>00B3F42A-1E9E-4C5D-8705-F09FC11F26D0</a:ObjectID>
<a:Name>Entrer numero licence</a:Name>
<a:Code>Entrer_numero_licence</a:Code>
<a:CreationDate>1574701060</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297133</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o106">
<a:ObjectID>F81EFE1A-F6ED-4138-8112-9BC073C53DC9</a:ObjectID>
<a:Name>Payer</a:Name>
<a:Code>Payer</a:Code>
<a:CreationDate>1574934059</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934077</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o107">
<a:ObjectID>9F72E104-7AB8-40C3-A567-895831B62734</a:ObjectID>
<a:Name>Choisir categorie</a:Name>
<a:Code>Choisir_categorie</a:Code>
<a:CreationDate>1574934183</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296763</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o108">
<a:ObjectID>2634929D-9012-4021-B8FF-2E1926B7D3A9</a:ObjectID>
<a:Name>Definir nombre billet par categorie</a:Name>
<a:Code>Definir_nombre_billet_par_categorie</a:Code>
<a:CreationDate>1574934479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934764</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o109">
<a:ObjectID>5F14806E-F601-49B7-85B7-BB750EB8553F</a:ObjectID>
<a:Name>Definir prix billet par categorie</a:Name>
<a:Code>Definir_prix_billet_par_categorie</a:Code>
<a:CreationDate>1574934505</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934768</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o110">
<a:ObjectID>104972A4-0F37-4814-89E2-E4BB9A21DCAC</a:ObjectID>
<a:Name>S&#39;authentifier Staff</a:Name>
<a:Code>S_authentifier_Staff</a:Code>
<a:CreationDate>1574934538</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938096</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o112">
<a:ObjectID>E0FED134-3711-47D6-8111-CF8347DF7E67</a:ObjectID>
<a:Name>Choisir place</a:Name>
<a:Code>Choisir_place</a:Code>
<a:CreationDate>1574935314</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574935355</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o113">
<a:ObjectID>69B258EE-EE01-4B3C-BF86-1758DE9833FF</a:ObjectID>
<a:Name>S&#39;authentifier Client</a:Name>
<a:Code>S_authentifier_Client</a:Code>
<a:CreationDate>1574935974</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936021</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o114">
<a:ObjectID>6262727E-F3D2-4D89-B8D8-35F16CDB30B5</a:ObjectID>
<a:Name>Definir nombre billet par type</a:Name>
<a:Code>Definir_nombre_billet_par_type</a:Code>
<a:CreationDate>1574936895</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574937007</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o115">
<a:ObjectID>0CD6CD2F-88DA-442F-9026-01D66EDF59A7</a:ObjectID>
<a:Name>Definir reductions</a:Name>
<a:Code>Definir_reductions</a:Code>
<a:CreationDate>1574938053</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938096</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o116">
<a:ObjectID>0BF2D07C-B751-4D79-80CE-294D70A51DB2</a:ObjectID>
<a:Name>Choisir journée</a:Name>
<a:Code>Choisir_journee</a:Code>
<a:CreationDate>1575296698</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575296763</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o117">
<a:ObjectID>CD22E091-566A-4E65-BAD9-2E1D44ACEAF5</a:ObjectID>
<a:Name>Choisir type de billlet</a:Name>
<a:Code>Choisir_type_de_billlet</a:Code>
<a:CreationDate>1575297050</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
</c:UseCases>
<c:Package.Objects>
<o:UMLObject Id="o192">
<a:ObjectID>7DE9DCE3-3D0F-4638-8D8B-663586B2D335</a:ObjectID>
<a:Name>Application</a:Name>
<a:Code>Application</a:Code>
<a:CreationDate>1575963449</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575963455</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UMLObject>
<o:UMLObject Id="o195">
<a:ObjectID>822B75C1-7F23-4F64-922B-EB2A866FA07F</a:ObjectID>
<a:CreationDate>1575963560</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575963560</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:InstantiationClass>
<o:Class Ref="o37"/>
</c:InstantiationClass>
</o:UMLObject>
<o:UMLObject Id="o198">
<a:ObjectID>9439A23E-7F24-413D-8F20-0E76B8229173</a:ObjectID>
<a:CreationDate>1575963585</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575963585</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:InstantiationClass>
<o:Class Ref="o38"/>
</c:InstantiationClass>
</o:UMLObject>
<o:UMLObject Id="o201">
<a:ObjectID>4E7340C7-04FB-4AAA-91D7-965F8B325006</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>Planning</a:Code>
<a:CreationDate>1575963919</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575963923</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UMLObject>
<o:UMLObject Id="o204">
<a:ObjectID>32409693-48AE-4EBB-80FB-927A8403AB93</a:ObjectID>
<a:CreationDate>1575967833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:InstantiationClass>
<o:Class Ref="o46"/>
</c:InstantiationClass>
</o:UMLObject>
</c:Package.Objects>
<c:Messages>
<o:Message Id="o188">
<a:ObjectID>F06C3E6E-97B1-40F4-A4EC-A8EC1B5A4295</a:ObjectID>
<a:Name>acheterBillet</a:Name>
<a:Code>acheterBillet</a:Code>
<a:CreationDate>1575963769</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970475</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o186">
<a:ObjectID>60E5FF26-6675-4D5B-BD92-60A20F5FD9FD</a:ObjectID>
<a:Name>getListJournee()</a:Name>
<a:Code>getListJournee__</a:Code>
<a:CreationDate>1575967382</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967601</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o201"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o184">
<a:ObjectID>038A8FDE-A4E4-420F-AABF-EABEE7A739AB</a:ObjectID>
<a:Name>listJournee</a:Name>
<a:Code>listJournee</a:Code>
<a:CreationDate>1575967388</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575967597</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o201"/>
</c:Object2>
</o:Message>
<o:Message Id="o181">
<a:ObjectID>33C07A34-F460-4FD8-B3AD-02F2E9D91E7F</a:ObjectID>
<a:Name>Afficher journees</a:Name>
<a:Code>Afficher_journees</a:Code>
<a:CreationDate>1575967630</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970401</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o105"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o178">
<a:ObjectID>FCADF34B-3294-4C64-9CD9-0B3F8AEC8726</a:ObjectID>
<a:Name>Choix journee et cours</a:Name>
<a:Code>Choix_journee_et_cours</a:Code>
<a:CreationDate>1575970450</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970599</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o176">
<a:ObjectID>F53D2600-0EC6-4008-AD7B-9320F0BCA824</a:ObjectID>
<a:Name>categorieDisponible()</a:Name>
<a:Code>categorieDisponible__</a:Code>
<a:CreationDate>1575970980</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o198"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o174">
<a:ObjectID>6B9E6B47-DBB4-4E0C-8232-EA3BBBC4E418</a:ObjectID>
<a:Name>placeDisponible()</a:Name>
<a:Code>placeDisponible__</a:Code>
<a:CreationDate>1575970984</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971070</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o204"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o198"/>
</c:Object2>
</o:Message>
<o:Message Id="o172">
<a:ObjectID>AB752281-D21A-4E0F-9CB4-121C7534E93C</a:ObjectID>
<a:Name>listPlace</a:Name>
<a:Code>listPlace</a:Code>
<a:CreationDate>1575971005</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971087</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o198"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o204"/>
</c:Object2>
</o:Message>
<o:Message Id="o169">
<a:ObjectID>B4280BFB-E462-430C-9343-111A8B82EFCC</a:ObjectID>
<a:Name>listCategorie</a:Name>
<a:Code>listCategorie</a:Code>
<a:CreationDate>1575971011</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971101</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o198"/>
</c:Object2>
</o:Message>
<o:Message Id="o166">
<a:ObjectID>68C520A2-908D-4FC3-BFE8-8BE6E67CE70A</a:ObjectID>
<a:Name>getNombrePlacesRestantes(court)</a:Name>
<a:Code>getNombrePlacesRestantes_court_</a:Code>
<a:CreationDate>1575971462</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971527</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o204"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o164">
<a:ObjectID>55E5FB79-0908-46C9-BB12-A74A6337495F</a:ObjectID>
<a:Name>nombrePlaces</a:Name>
<a:Code>nombrePlaces</a:Code>
<a:CreationDate>1575971480</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971538</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o204"/>
</c:Object2>
</o:Message>
<o:Message Id="o161">
<a:ObjectID>9879A3B1-5EAA-4D2A-9071-2D92366294A9</a:ObjectID>
<a:Name>choixEmplacement)</a:Name>
<a:Code>choixEmplacement_</a:Code>
<a:CreationDate>1575971611</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971726</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o159">
<a:ObjectID>830D4C9C-8272-4B20-8AB8-9AA9FCA84F54</a:ObjectID>
<a:Name>testDisponibilité()</a:Name>
<a:Code>testDisponibilite__</a:Code>
<a:CreationDate>1575971684</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971740</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o204"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o157">
<a:ObjectID>93FDEBC9-073E-423D-A0E2-2E7E5F942B2D</a:ObjectID>
<a:Name>estDisponible</a:Name>
<a:Code>estDisponible</a:Code>
<a:CreationDate>1575971746</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971786</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o204"/>
</c:Object2>
</o:Message>
<o:Message Id="o154">
<a:ObjectID>235F9593-72F2-4340-A382-0F7A4F789436</a:ObjectID>
<a:Name>choixNombrePlaces</a:Name>
<a:Code>choixNombrePlaces</a:Code>
<a:CreationDate>1575971868</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575971888</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o152">
<a:ObjectID>B3CA2FEF-94A4-4714-BE50-98E9EAE70507</a:ObjectID>
<a:Name>getNombrePlacesRestantes(court)</a:Name>
<a:Code>getNombrePlacesRestantes_court_</a:Code>
<a:CreationDate>1575971922</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972028</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o204"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o150">
<a:ObjectID>3094EE6A-82FB-4E5D-B993-62C432F72627</a:ObjectID>
<a:Name>nombrePlaces</a:Name>
<a:Code>nombrePlaces</a:Code>
<a:CreationDate>1575971928</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972033</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o204"/>
</c:Object2>
</o:Message>
<o:Message Id="o147">
<a:ObjectID>1BDF6A64-2132-45C5-AF97-15BCAA1A2C33</a:ObjectID>
<a:Name>ok</a:Name>
<a:Code>ok</a:Code>
<a:CreationDate>1575972073</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972078</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o105"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o144">
<a:ObjectID>AE33744E-3AE4-49E7-9344-28DE437F08E8</a:ObjectID>
<a:Name>procéderAchat()</a:Name>
<a:Code>procederAchat__</a:Code>
<a:CreationDate>1575972277</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972487</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o142">
<a:ObjectID>28CCFBC2-DBFF-45C7-BCF9-3E6EFE8DB902</a:ObjectID>
<a:Name>calculerPrix()</a:Name>
<a:Code>calculerPrix__</a:Code>
<a:CreationDate>1575972307</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972386</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o195"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o140">
<a:ObjectID>33BB481D-1F5F-41E8-A5A5-60279F0F926D</a:ObjectID>
<a:Name>prix</a:Name>
<a:Code>prix</a:Code>
<a:CreationDate>1575972441</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972452</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o195"/>
</c:Object2>
</o:Message>
<o:Message Id="o137">
<a:ObjectID>DA3CFBA6-0F91-4454-A835-202D3FD03DB6</a:ObjectID>
<a:Name>prixTotal</a:Name>
<a:Code>prixTotal</a:Code>
<a:CreationDate>1575972469</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972493</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Actor Ref="o105"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o135">
<a:ObjectID>FAD949A7-A934-451F-B970-074F28C4987B</a:ObjectID>
<a:Name>paiement</a:Name>
<a:Code>paiement</a:Code>
<a:CreationDate>1575972504</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972523</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:Message>
<o:Message Id="o133">
<a:ObjectID>6E70A044-1713-47D1-B14B-097BC6D595FA</a:ObjectID>
<a:Name>validationPaiement()</a:Name>
<a:Code>validationPaiement__</a:Code>
<a:CreationDate>1575972621</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972645</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<a:Delay>1</a:Delay>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o130">
<a:ObjectID>6A7E974E-F167-4B3F-956F-8920944FCB31</a:ObjectID>
<a:Name>paiementValidé</a:Name>
<a:Code>paiementValide</a:Code>
<a:CreationDate>1575972665</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972697</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Actor Ref="o105"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o128">
<a:ObjectID>A9D41935-EFD4-486E-B1DF-AF5501C014C3</a:ObjectID>
<a:Name>exporterPDF()</a:Name>
<a:Code>exporterPDF__</a:Code>
<a:CreationDate>1575972766</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972809</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o195"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
<o:Message Id="o126">
<a:ObjectID>BC295B94-D929-4E71-A713-DD566C8A493B</a:ObjectID>
<a:Name>PDF</a:Name>
<a:Code>PDF</a:Code>
<a:CreationDate>1575972780</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972813</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o192"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o195"/>
</c:Object2>
</o:Message>
<o:Message Id="o123">
<a:ObjectID>1DB6C81C-50EE-4E9C-AA6A-5D958B38DC56</a:ObjectID>
<a:Name>PDF</a:Name>
<a:Code>PDF</a:Code>
<a:CreationDate>1575972822</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575972827</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o105"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o192"/>
</c:Object2>
</o:Message>
</c:Messages>
<c:UseCaseAssociations>
<o:UseCaseAssociation Id="o68">
<a:ObjectID>FA7E00BC-152D-4E6B-8E30-5390907E2C03</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1574934806</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934806</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o108"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o111"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o70">
<a:ObjectID>A8E9E958-6B45-421E-850D-84BE6E40CA83</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1574934808</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574934808</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o109"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o111"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o80">
<a:ObjectID>76C14B4F-A79A-4F5F-89DB-63210827F1BB</a:ObjectID>
<a:Name>Association_8</a:Name>
<a:Code>Association_8</a:Code>
<a:CreationDate>1574936998</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574936998</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o114"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o111"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o85">
<a:ObjectID>733C54E1-B695-4E9E-9A2E-B33B6711633A</a:ObjectID>
<a:Name>Association_9</a:Name>
<a:Code>Association_9</a:Code>
<a:CreationDate>1574938080</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574938080</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o115"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o111"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o92">
<a:ObjectID>36DB298C-0062-44DB-BCAA-F49724CD39D3</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1575297030</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297030</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o102"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o105"/>
</c:Object2>
</o:UseCaseAssociation>
</c:UseCaseAssociations>
<c:Flows>
<o:ActivityFlow Id="o238">
<a:ObjectID>E461CC71-25D5-4A22-8F97-0297D2CB14EB</a:ObjectID>
<a:CreationDate>1575295183</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295183</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o239"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o240"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o241">
<a:ObjectID>5B9A609B-9919-485D-9CAD-4CF97BC6AB5D</a:ObjectID>
<a:CreationDate>1575295185</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295185</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o242"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o239"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o243">
<a:ObjectID>79A8221C-ACAD-4699-9981-53DEB1720906</a:ObjectID>
<a:CreationDate>1575295189</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295189</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o244"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o245"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o246">
<a:ObjectID>B3BE866B-F91D-40FB-8CB7-41729307535F</a:ObjectID>
<a:CreationDate>1575295193</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295193</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o247"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o244"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o248">
<a:ObjectID>489B9595-C061-4963-9D74-A7FE765F19C6</a:ObjectID>
<a:CreationDate>1575295234</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295309</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>oui</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o242"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o249"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o250">
<a:ObjectID>0CE87A0D-0ED2-48BA-BD33-DD6DA02EDE44</a:ObjectID>
<a:CreationDate>1575295246</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295305</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>non</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o251"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o249"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o252">
<a:ObjectID>0168A4BD-36A2-47E7-B9A9-75E1501C5F61</a:ObjectID>
<a:CreationDate>1575295248</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295248</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o253"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o251"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o254">
<a:ObjectID>A3F77791-EF63-4F8D-91C9-E59891F4B949</a:ObjectID>
<a:CreationDate>1575295498</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295528</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>oui</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o245"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o255"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o256">
<a:ObjectID>D8A080B6-0C81-4F85-A5C7-628CC5DA0295</a:ObjectID>
<a:CreationDate>1575295589</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295589</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Decision Ref="o249"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o247"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o257">
<a:ObjectID>1DCEF91B-3CA2-462B-AFF2-023E61D8CEBC</a:ObjectID>
<a:CreationDate>1575295703</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295703</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o258"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o253"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o259">
<a:ObjectID>EC3C1136-2061-470C-B97B-024312A3874B</a:ObjectID>
<a:CreationDate>1575295780</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295780</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o260"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o258"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o261">
<a:ObjectID>24469B6C-A2D3-41EC-898F-C9286B1440E4</a:ObjectID>
<a:CreationDate>1575297388</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297388</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Decision Ref="o255"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o262"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o263">
<a:ObjectID>9131AB40-1AF3-415D-BDB7-AD5D98B7CD1C</a:ObjectID>
<a:CreationDate>1575298086</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298086</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o264"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o265"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o266">
<a:ObjectID>FC9C314F-B05B-4433-9B63-F36BA9573A7F</a:ObjectID>
<a:CreationDate>1575298092</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298092</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o267"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o268"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o269">
<a:ObjectID>17A9A85C-1398-453A-8B1A-7DDF4100BFC4</a:ObjectID>
<a:CreationDate>1575298096</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298096</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o270"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o267"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o271">
<a:ObjectID>3E9B38C2-5A9D-44EB-8BDB-C8F661E27BCB</a:ObjectID>
<a:CreationDate>1575357279</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575357279</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o272"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o270"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o273">
<a:ObjectID>35641695-0669-40F2-B70A-6949F2A71286</a:ObjectID>
<a:CreationDate>1575357458</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575357458</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o274"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o264"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o275">
<a:ObjectID>3939E248-6645-42C0-ACBE-4CA46A46815F</a:ObjectID>
<a:CreationDate>1575357465</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575357465</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o268"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o274"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o276">
<a:ObjectID>52C02501-A619-4869-8F7E-3477AAF8346D</a:ObjectID>
<a:CreationDate>1575360273</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360273</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o277"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o278"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o279">
<a:ObjectID>1F1E6737-08AB-4771-AA6D-45FEB968943F</a:ObjectID>
<a:CreationDate>1575360274</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360274</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o280"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o277"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o281">
<a:ObjectID>8835C64A-A639-4CAE-A7FA-8599BEB0418B</a:ObjectID>
<a:CreationDate>1575360275</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360275</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o282"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o280"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o283">
<a:ObjectID>C08763AD-B1EA-437D-8403-F49C0C5EF1BC</a:ObjectID>
<a:CreationDate>1575360277</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360277</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o270"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o282"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o284">
<a:ObjectID>EB5C22C4-8FDE-4F1D-AC42-A99A64FC3260</a:ObjectID>
<a:CreationDate>1575360291</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360291</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o285"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o270"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o286">
<a:ObjectID>48C88272-137F-4AD2-B700-38F618C7D683</a:ObjectID>
<a:CreationDate>1575360781</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360781</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o287"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o288"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o289">
<a:ObjectID>69D54F42-0A64-4D39-8F39-5456EB355898</a:ObjectID>
<a:CreationDate>1575360783</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360783</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o290"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o287"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o291">
<a:ObjectID>109652A5-7081-4CE4-9CDE-45E6C1482105</a:ObjectID>
<a:CreationDate>1575360784</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360784</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o292"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o290"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o293">
<a:ObjectID>91EB0D43-A68D-4959-A6D8-C3E5C1D4FAF2</a:ObjectID>
<a:CreationDate>1575360785</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360785</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o270"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o292"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o294">
<a:ObjectID>4A6F12F4-9C98-4246-BC5F-7406E97A65C2</a:ObjectID>
<a:CreationDate>1575360791</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360791</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o295"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o270"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o296">
<a:ObjectID>46C24E91-CF48-4829-B117-EA6488B476C7</a:ObjectID>
<a:CreationDate>1575970083</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970083</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o262"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o242"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o297">
<a:ObjectID>19C83460-A0FD-434A-8365-1A75172E11E5</a:ObjectID>
<a:CreationDate>1575970113</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>non</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o298"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o255"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o299">
<a:ObjectID>18EFD969-C58C-4B7F-86B9-87FA206AD0DE</a:ObjectID>
<a:CreationDate>1575970118</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970118</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o244"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o298"/>
</c:Object2>
</o:ActivityFlow>
</c:Flows>
<c:Activities>
<o:Activity Id="o242">
<a:ObjectID>88D5C940-0EC0-4AF2-9E25-4AD30B29E2CA</a:ObjectID>
<a:Name>Choisir journée</a:Name>
<a:Code>Choisir_journee</a:Code>
<a:CreationDate>1575294026</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295309</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o262">
<a:ObjectID>86582E4D-526B-4998-B2B4-306C2A26E719</a:ObjectID>
<a:Name>Choisir option Court Central</a:Name>
<a:Code>Choisir_option_Court_Central</a:Code>
<a:CreationDate>1575294295</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575294993</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o245">
<a:ObjectID>4E24D9ED-15B9-47AD-93BD-8351E28E09C0</a:ObjectID>
<a:Name>Choisir emplacement</a:Name>
<a:Code>Choisir_emplacement</a:Code>
<a:CreationDate>1575294343</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295528</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o239">
<a:ObjectID>AE2D94B8-DA5F-4DE0-9934-55EF9C440D51</a:ObjectID>
<a:Name>Verfifier disponibilité</a:Name>
<a:Code>Verfifier_disponibilite</a:Code>
<a:CreationDate>1575294426</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575294980</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o301"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o244">
<a:ObjectID>8A1B9FD8-2CB8-4BC5-9538-DE6445F507AF</a:ObjectID>
<a:Name>Verifier choix du client</a:Name>
<a:Code>Verifier_choix_du_client</a:Code>
<a:CreationDate>1575294442</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295543</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o247">
<a:ObjectID>C7E35BAB-1245-477A-A0BA-81A064B3CBA9</a:ObjectID>
<a:Name>Calculer prix</a:Name>
<a:Code>Calculer_prix</a:Code>
<a:CreationDate>1575294925</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295456</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o251">
<a:ObjectID>24619233-6D04-4F32-94B5-351C8C057037</a:ObjectID>
<a:Name>Comfirmer commande</a:Name>
<a:Code>Comfirmer_commande</a:Code>
<a:CreationDate>1575294957</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295305</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o253">
<a:ObjectID>87469F1C-2C43-41AF-A366-EEED8850C4A8</a:ObjectID>
<a:Name>Payer commande</a:Name>
<a:Code>Payer_commande</a:Code>
<a:CreationDate>1575295089</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295105</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o258">
<a:ObjectID>BBAF3CE4-DD27-4637-92D2-FA00FBB7E883</a:ObjectID>
<a:Name>Envoyer billet par mail</a:Name>
<a:Code>Envoyer_billet_par_mail</a:Code>
<a:CreationDate>1575295677</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o264">
<a:ObjectID>F7366256-5DF1-4603-A76A-40A4258C05C9</a:ObjectID>
<a:Name>Choisir categorie</a:Name>
<a:Code>Choisir_categorie</a:Code>
<a:CreationDate>1575297795</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297923</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o302"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o268">
<a:ObjectID>878B6999-1E2F-4F1D-9AA9-A2F533D8AADC</a:ObjectID>
<a:Name>Definir nb places</a:Name>
<a:Code>Definir_nb_places</a:Code>
<a:CreationDate>1575297800</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298037</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o302"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o270">
<a:ObjectID>B96524E0-6CBA-4C7C-9CA7-5BE3014426F6</a:ObjectID>
<a:Name>Confirmer</a:Name>
<a:Code>Confirmer</a:Code>
<a:CreationDate>1575297801</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298051</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o302"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o267">
<a:ObjectID>25BBF6AA-AEB3-4D4C-A2E5-6CC284628DD4</a:ObjectID>
<a:Name>Verifier coherence</a:Name>
<a:Code>Verifier_coherence</a:Code>
<a:CreationDate>1575298058</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575298072</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o303"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o274">
<a:ObjectID>D3D13AA4-E935-4DBE-A209-8B18E382540B</a:ObjectID>
<a:Name>Selectioner billets concernés</a:Name>
<a:Code>Selectioner_billets_concernes</a:Code>
<a:CreationDate>1575357418</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575357454</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o302"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o277">
<a:ObjectID>1CAFB3D4-05A5-4F1C-A27B-20C160A31866</a:ObjectID>
<a:Name>Choisir type de billet</a:Name>
<a:Code>Choisir_type_de_billet</a:Code>
<a:CreationDate>1575360005</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360039</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o280">
<a:ObjectID>6686056F-B514-47CC-B591-24E08FB2F371</a:ObjectID>
<a:Name>Definir nb billets</a:Name>
<a:Code>Definir_nb_billets</a:Code>
<a:CreationDate>1575360046</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360109</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o282">
<a:ObjectID>1989E0BB-95B8-463D-96DB-FD3BE27E6E6A</a:ObjectID>
<a:Name>Verifier coherence des données</a:Name>
<a:Code>Verifier_coherence_des_donnees</a:Code>
<a:CreationDate>1575360138</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360155</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o287">
<a:ObjectID>61AEDE7C-A542-4B28-AAC3-5800B04C953D</a:ObjectID>
<a:Name>Choisir billets GrandPublic ou Big Match</a:Name>
<a:Code>Choisir_billets_GrandPublic_ou_Big_Match</a:Code>
<a:CreationDate>1575360407</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360456</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o290">
<a:ObjectID>858358E6-3B2C-4BDB-A0B0-78A9774512AA</a:ObjectID>
<a:Name>Definir prix</a:Name>
<a:Code>Definir_prix</a:Code>
<a:CreationDate>1575360463</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360467</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o292">
<a:ObjectID>83BA0C91-0C66-4047-B20B-B51E16C949A4</a:ObjectID>
<a:Name>Verifier compatibilité des données</a:Name>
<a:Code>Verifier_compatibilite_des_donnees</a:Code>
<a:CreationDate>1575360667</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360692</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o298">
<a:ObjectID>1869D27C-354C-4D5F-BCAD-87D0EC70B13D</a:ObjectID>
<a:Name>Choisir nombre de personne</a:Name>
<a:Code>Choisir_nombre_de_personne</a:Code>
<a:CreationDate>1575970086</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o300"/>
</c:OrganizationUnit>
</o:Activity>
</c:Activities>
<c:Decisions>
<o:Decision Id="o249">
<a:ObjectID>D7B85C17-A53A-4DEE-ABAF-963BF3AFAC91</a:ObjectID>
<a:Name>Ajouter billet</a:Name>
<a:Code>Ajouter_billet</a:Code>
<a:CreationDate>1575295046</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295309</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Decision>
<o:Decision Id="o255">
<a:ObjectID>E99103C8-FD94-4BE4-8551-D405E71F56F9</a:ObjectID>
<a:Name>Court Central sélectionné</a:Name>
<a:Code>Court_Central_selectionne</a:Code>
<a:CreationDate>1575295412</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970143</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Decision>
</c:Decisions>
<c:ActivityDiagrams>
<o:ActivityDiagram Id="o304">
<a:ObjectID>43370109-4910-4570-B101-E8219BF8FD65</a:ObjectID>
<a:Name>4. Activité Achat de Billet - Billetterie</a:Name>
<a:Code>4__Activite_Achat_de_Billet___Billetterie</a:Code>
<a:CreationDate>1575293945</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576511475</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:SwimlaneGroupSymbol Id="o305">
<a:CreationDate>1575294389</a:CreationDate>
<a:ModificationDate>1575970311</a:ModificationDate>
<a:Rect>((16701,-53912), (32775,15667))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o306">
<a:CreationDate>1575294389</a:CreationDate>
<a:ModificationDate>1575970096</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((16701,-53912), (32775,15667))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:ActivitySymbol Id="o307">
<a:CreationDate>1575294426</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((20693,6258), (28809,8257))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o239"/>
</c:Object>
</o:ActivitySymbol>
<o:NoteSymbol Id="o308">
<a:Text>unitaire et total</a:Text>
<a:CreationDate>1575297455</a:CreationDate>
<a:ModificationDate>1575297469</a:ModificationDate>
<a:Rect>((24387,-28892), (29186,-25293))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:NoteSymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o301"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
<o:FlowSymbol Id="o309">
<a:CreationDate>1575970118</a:CreationDate>
<a:ModificationDate>1575970120</a:ModificationDate>
<a:Rect>((16178,-20079), (25169,-13809))</a:Rect>
<a:ListOfPoints>((16178,-13809),(25169,-13809),(25169,-20079))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o310"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o311"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o299"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o312">
<a:CreationDate>1575295183</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:Rect>((645,7183), (25050,11322))</a:Rect>
<a:ListOfPoints>((645,11322),(645,7183),(25050,7183))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o313"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o307"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o238"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o314">
<a:CreationDate>1575295185</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:Rect>((-224,1483), (25200,6358))</a:Rect>
<a:ListOfPoints>((25200,6358),(25200,1483),(-224,1483))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o307"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o315"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o241"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o316">
<a:CreationDate>1575295189</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((699,-20749), (23783,-17679))</a:Rect>
<a:ListOfPoints>((699,-17679),(699,-20749),(23783,-20749))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o317"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o311"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o243"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o318">
<a:CreationDate>1575295193</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((23508,-25256), (24508,-21306))</a:Rect>
<a:ListOfPoints>((24008,-21306),(24008,-25256))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o311"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o319"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o246"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o320">
<a:CreationDate>1575295589</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((309,-27996), (23581,-25277))</a:Rect>
<a:ListOfPoints>((23581,-25277),(309,-25277),(309,-27996))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o319"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseDecisionSymbol Ref="o321"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o256"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o322">
<a:CreationDate>1575295703</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((482,-44549), (23829,-41414))</a:Rect>
<a:ListOfPoints>((482,-41414),(482,-44549),(23829,-44549))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o323"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o324"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o257"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o325">
<a:CreationDate>1575295780</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((971,-51211), (24654,-44952))</a:Rect>
<a:ListOfPoints>((24654,-44952),(24654,-51211),(971,-51211))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o324"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o326"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o259"/>
</c:Object>
</o:FlowSymbol>
<o:ActivitySymbol Id="o311">
<a:CreationDate>1575294442</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((19425,-21432), (28142,-19433))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o244"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o319">
<a:CreationDate>1575294925</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((20783,-26012), (26782,-24013))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o247"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o324">
<a:CreationDate>1575295677</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((19763,-45300), (28554,-43301))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o258"/>
</c:Object>
</o:ActivitySymbol>
<o:SwimlaneGroupSymbol Id="o327">
<a:CreationDate>1575294085</a:CreationDate>
<a:ModificationDate>1575970311</a:ModificationDate>
<a:Rect>((-13449,-53912), (19165,15667))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o328">
<a:CreationDate>1575294085</a:CreationDate>
<a:ModificationDate>1575970311</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13449,-53912), (19165,15667))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:FlowSymbol Id="o329">
<a:CreationDate>1575970113</a:CreationDate>
<a:ModificationDate>1575970113</a:ModificationDate>
<a:Rect>((8098,-13974), (15105,-9540))</a:Rect>
<a:ListOfPoints>((8098,-10714),(15105,-10714),(15105,-13974))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o330"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o310"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o297"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o331">
<a:CreationDate>1575970083</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:Rect>((776,-5025), (1776,1050))</a:Rect>
<a:ListOfPoints>((1276,1050),(1276,-5025))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o315"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o332"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o296"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o333">
<a:CreationDate>1575297388</a:CreationDate>
<a:ModificationDate>1575297388</a:ModificationDate>
<a:Rect>((372,-8734), (1372,-4772))</a:Rect>
<a:ListOfPoints>((872,-4772),(872,-8734))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o332"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseDecisionSymbol Ref="o330"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o261"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o334">
<a:CreationDate>1575295498</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((498,-17243), (3172,-12694))</a:Rect>
<a:ListOfPoints>((998,-12694),(998,-17243))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o330"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o317"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o254"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o335">
<a:CreationDate>1575295248</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((109,-41663), (1109,-37463))</a:Rect>
<a:ListOfPoints>((609,-37463),(609,-41663))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o336"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o323"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o252"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o337">
<a:CreationDate>1575295246</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:Rect>((-154,-36713), (2746,-31956))</a:Rect>
<a:ListOfPoints>((346,-31956),(346,-36713))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o321"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o336"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o250"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o338">
<a:CreationDate>1575295234</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:Rect>((-10874,-31150), (-749,1766))</a:Rect>
<a:ListOfPoints>((-3532,-29976),(-10874,-29976),(-10874,1766),(-749,1766))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o321"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o315"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o248"/>
</c:Object>
</o:FlowSymbol>
<o:ActivitySymbol Id="o315">
<a:CreationDate>1575294026</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2295,688), (3796,2687))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o242"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o332">
<a:CreationDate>1575294295</a:CreationDate>
<a:ModificationDate>1575297375</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-4159,-5592), (6357,-3593))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o262"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o317">
<a:CreationDate>1575294343</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-4033,-18228), (4233,-16229))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o245"/>
</c:Object>
</o:ActivitySymbol>
<o:BaseDecisionSymbol Id="o321">
<a:CreationDate>1575295046</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3571,-31976), (4189,-27977))</a:Rect>
<a:LineColor>32896</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DEXP 0 Arial,8,N
DEXN 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Decision Ref="o249"/>
</c:Object>
</o:BaseDecisionSymbol>
<o:ActivitySymbol Id="o336">
<a:CreationDate>1575294957</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3787,-37802), (4854,-35803))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o251"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o323">
<a:CreationDate>1575295089</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2887,-42140), (4104,-40141))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o253"/>
</c:Object>
</o:ActivitySymbol>
<o:StartSymbol Id="o313">
<a:CreationDate>1575295127</a:CreationDate>
<a:ModificationDate>1575970296</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((46,10723), (1245,11922))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o240"/>
</c:Object>
</o:StartSymbol>
<o:EndSymbol Id="o326">
<a:CreationDate>1575295132</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((222,-51961), (1721,-50462))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o260"/>
</c:Object>
</o:EndSymbol>
<o:BaseDecisionSymbol Id="o330">
<a:CreationDate>1575295412</a:CreationDate>
<a:ModificationDate>1575297363</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-6115,-12714), (8169,-8715))</a:Rect>
<a:LineColor>32896</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DEXP 0 Arial,8,N
DEXN 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Decision Ref="o255"/>
</c:Object>
</o:BaseDecisionSymbol>
<o:NoteSymbol Id="o339">
<a:Text>Pour chaque personne
(categorie + place)</a:Text>
<a:CreationDate>1575297415</a:CreationDate>
<a:ModificationDate>1575297576</a:ModificationDate>
<a:Rect>((-9734,-20780), (-438,-18392))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:NoteSymbol>
<o:ActivitySymbol Id="o310">
<a:CreationDate>1575970086</a:CreationDate>
<a:ModificationDate>1575970104</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((7748,-15053), (18414,-13054))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o298"/>
</c:Object>
</o:ActivitySymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o300"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o340">
<a:ObjectID>B2C4B325-A39B-498A-93B6-CF391DDA7A15</a:ObjectID>
<a:Name>5. Activité Definir nombre de billets/categories - Billetterie</a:Name>
<a:Code>5__Activite_Definir_nombre_de_billets_categories___Billetterie</a:Code>
<a:CreationDate>1575295998</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576511378</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:FlowSymbol Id="o341">
<a:CreationDate>1575298092</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:Rect>((-7689,-750), (4650,3300))</a:Rect>
<a:ListOfPoints>((-7689,3300),(-7689,-750),(4650,-750))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o342"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o343"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o266"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o344">
<a:CreationDate>1575298096</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:Rect>((-6302,-6900), (3825,-1125))</a:Rect>
<a:ListOfPoints>((3825,-1125),(3825,-6900),(-6302,-6900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o343"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o345"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o269"/>
</c:Object>
</o:FlowSymbol>
<o:SwimlaneGroupSymbol Id="o346">
<a:CreationDate>1575297677</a:CreationDate>
<a:ModificationDate>1575357633</a:ModificationDate>
<a:Rect>((-3700,-17576), (10350,22424))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o347">
<a:CreationDate>1575297677</a:CreationDate>
<a:ModificationDate>1575357633</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3700,-17576), (10350,22424))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:ActivitySymbol Id="o343">
<a:CreationDate>1575298058</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((143,-1825), (7359,174))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o267"/>
</c:Object>
</o:ActivitySymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o303"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
<o:SwimlaneGroupSymbol Id="o348">
<a:CreationDate>1575297677</a:CreationDate>
<a:ModificationDate>1575357635</a:ModificationDate>
<a:Rect>((-17349,-17576), (-1652,22424))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o349">
<a:CreationDate>1575297677</a:CreationDate>
<a:ModificationDate>1575357635</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17349,-17576), (-1652,22424))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:FlowSymbol Id="o350">
<a:CreationDate>1575357465</a:CreationDate>
<a:ModificationDate>1575357465</a:ModificationDate>
<a:Rect>((-8549,3300), (-7549,9551))</a:Rect>
<a:ListOfPoints>((-8049,9551),(-8049,3300))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o351"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o342"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o275"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o352">
<a:CreationDate>1575357458</a:CreationDate>
<a:ModificationDate>1575357458</a:ModificationDate>
<a:Rect>((-8587,9096), (-7587,15150))</a:Rect>
<a:ListOfPoints>((-8087,15150),(-8087,9096))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o353"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o351"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o273"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o354">
<a:CreationDate>1575357279</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:Rect>((-8114,-15525), (-7114,-6975))</a:Rect>
<a:ListOfPoints>((-7614,-6975),(-7614,-15525))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o345"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o355"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o271"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o356">
<a:CreationDate>1575298086</a:CreationDate>
<a:ModificationDate>1575298086</a:ModificationDate>
<a:Rect>((-8827,15000), (-7827,19199))</a:Rect>
<a:ListOfPoints>((-8327,19199),(-8327,15000))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o357"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o353"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o263"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o357">
<a:CreationDate>1575297781</a:CreationDate>
<a:ModificationDate>1575297790</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8927,18600), (-7728,19799))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o265"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o353">
<a:CreationDate>1575297795</a:CreationDate>
<a:ModificationDate>1575297795</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-11899,14525), (-5209,16524))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o264"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o342">
<a:CreationDate>1575297800</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-11448,2675), (-4757,4674))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o268"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o345">
<a:CreationDate>1575297801</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10427,-7675), (-4428,-5676))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o270"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o355">
<a:CreationDate>1575357267</a:CreationDate>
<a:ModificationDate>1575357446</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8364,-16275), (-6865,-14776))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o272"/>
</c:Object>
</o:EndSymbol>
<o:ActivitySymbol Id="o351">
<a:CreationDate>1575357418</a:CreationDate>
<a:ModificationDate>1575357454</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13736,9050), (-2846,11049))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o274"/>
</c:Object>
</o:ActivitySymbol>
<o:NoteSymbol Id="o358">
<a:Text>Grand Public ou Big Match</a:Text>
<a:CreationDate>1575357491</a:CreationDate>
<a:ModificationDate>1575357630</a:ModificationDate>
<a:Rect>((-16599,6053), (-9485,8849))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:NoteSymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o302"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o359">
<a:ObjectID>EFB0ED0D-4FD1-453E-8E7B-76F367E0CFFB</a:ObjectID>
<a:Name>6. Activité Definir nombre billets/types - Billetterie</a:Name>
<a:Code>6__Activite_Definir_nombre_billets_types___Billetterie</a:Code>
<a:CreationDate>1575296056</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576511386</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o360">
<a:CreationDate>1575360314</a:CreationDate>
<a:ModificationDate>1575360314</a:ModificationDate>
<a:Rect>((-22423,14685), (-5758,-23595))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o361">
<a:CreationDate>1575360319</a:CreationDate>
<a:ModificationDate>1575360319</a:ModificationDate>
<a:Rect>((-22588,14850), (-5758,11138))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o362">
<a:CreationDate>1575360324</a:CreationDate>
<a:ModificationDate>1575360324</a:ModificationDate>
<a:Rect>((-4603,14933), (12392,-24585))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o363">
<a:CreationDate>1575360329</a:CreationDate>
<a:ModificationDate>1575360329</a:ModificationDate>
<a:Rect>((-4686,15015), (12475,11138))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:TextSymbol Id="o364">
<a:Text>Client</a:Text>
<a:CreationDate>1575360343</a:CreationDate>
<a:ModificationDate>1575360355</a:ModificationDate>
<a:Rect>((-16449,14832), (-11649,11234))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:TextSymbol Id="o365">
<a:Text>Systeme</a:Text>
<a:CreationDate>1575360345</a:CreationDate>
<a:ModificationDate>1575360363</a:ModificationDate>
<a:Rect>((1617,14626), (6417,11028))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:FlowSymbol Id="o366">
<a:CreationDate>1575360273</a:CreationDate>
<a:ModificationDate>1575360273</a:ModificationDate>
<a:Rect>((-16054,1815), (-15054,8332))</a:Rect>
<a:ListOfPoints>((-15575,8332),(-15534,1815))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o367"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o368"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o276"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o369">
<a:CreationDate>1575360274</a:CreationDate>
<a:ModificationDate>1575360274</a:ModificationDate>
<a:Rect>((-15581,-4538), (-14581,1980))</a:Rect>
<a:ListOfPoints>((-15081,1980),(-15081,-4538))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o368"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o370"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o279"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o371">
<a:CreationDate>1575360275</a:CreationDate>
<a:ModificationDate>1575360275</a:ModificationDate>
<a:Rect>((-14586,-11138), (1997,-5280))</a:Rect>
<a:ListOfPoints>((-14586,-5280),(-14586,-11138),(1997,-11138))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o370"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o372"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o281"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o373">
<a:CreationDate>1575360277</a:CreationDate>
<a:ModificationDate>1575360283</a:ModificationDate>
<a:Rect>((-14008,-16088), (3565,-10973))</a:Rect>
<a:ListOfPoints>((3565,-10973),(3565,-16088),(-14008,-16088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o372"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o374"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o283"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o375">
<a:CreationDate>1575360291</a:CreationDate>
<a:ModificationDate>1575360291</a:ModificationDate>
<a:Rect>((-15127,-22028), (-14127,-16583))</a:Rect>
<a:ListOfPoints>((-14668,-16583),(-14586,-22028))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o374"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o376"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o284"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o367">
<a:CreationDate>1575359990</a:CreationDate>
<a:ModificationDate>1575360269</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16175,7733), (-14976,8932))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o278"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o368">
<a:CreationDate>1575360005</a:CreationDate>
<a:ModificationDate>1575360005</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-19349,1228), (-11308,3227))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o277"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o370">
<a:CreationDate>1575360046</a:CreationDate>
<a:ModificationDate>1575360112</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-18434,-5949), (-11893,-3950))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o280"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o372">
<a:CreationDate>1575360138</a:CreationDate>
<a:ModificationDate>1575360138</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-1790,-11559), (10076,-9560))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o282"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o374">
<a:CreationDate>1575360262</a:CreationDate>
<a:ModificationDate>1575360267</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17832,-17005), (-11833,-15006))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o270"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o376">
<a:CreationDate>1575360287</a:CreationDate>
<a:ModificationDate>1575360287</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-15336,-22778), (-13837,-21279))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o285"/>
</c:Object>
</o:EndSymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o377">
<a:ObjectID>DC5273E9-8067-48F0-8733-C513F75B0D9C</a:ObjectID>
<a:Name>7. Activité Definir prix des billets - Billetterie</a:Name>
<a:Code>7__Activite_Definir_prix_des_billets___Billetterie</a:Code>
<a:CreationDate>1575296092</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510670</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o378">
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360851</a:ModificationDate>
<a:Rect>((-27344,17305), (-10679,-20975))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o379">
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360851</a:ModificationDate>
<a:Rect>((-27509,17470), (-10679,13758))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o380">
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360844</a:ModificationDate>
<a:Rect>((-3949,17966), (13046,-21552))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o381">
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360844</a:ModificationDate>
<a:Rect>((-4032,18048), (13129,14171))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:TextSymbol Id="o382">
<a:Text>Client</a:Text>
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360824</a:ModificationDate>
<a:Rect>((-22195,17865), (-17395,14267))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:TextSymbol Id="o383">
<a:Text>Systeme</a:Text>
<a:CreationDate>1575360821</a:CreationDate>
<a:ModificationDate>1575360844</a:ModificationDate>
<a:Rect>((2271,17659), (7071,14061))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:FlowSymbol Id="o384">
<a:CreationDate>1575360781</a:CreationDate>
<a:ModificationDate>1575360781</a:ModificationDate>
<a:Rect>((-20487,6038), (-19487,12037))</a:Rect>
<a:ListOfPoints>((-20062,12037),(-19912,6038))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o385"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o386"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o286"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o387">
<a:CreationDate>1575360783</a:CreationDate>
<a:ModificationDate>1575360783</a:ModificationDate>
<a:Rect>((-20149,-1762), (-19149,6413))</a:Rect>
<a:ListOfPoints>((-19649,6413),(-19649,-1762))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o386"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o388"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o289"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o389">
<a:CreationDate>1575360784</a:CreationDate>
<a:ModificationDate>1575360784</a:ModificationDate>
<a:Rect>((-19537,-6862), (788,-1762))</a:Rect>
<a:ListOfPoints>((-19537,-1762),(-19537,-6862),(788,-6862))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o388"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o390"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o291"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o391">
<a:CreationDate>1575360785</a:CreationDate>
<a:ModificationDate>1575360785</a:ModificationDate>
<a:Rect>((-18562,-11962), (638,-7462))</a:Rect>
<a:ListOfPoints>((638,-7462),(638,-11962),(-18562,-11962))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o390"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o392"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o293"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o393">
<a:CreationDate>1575360791</a:CreationDate>
<a:ModificationDate>1575360791</a:ModificationDate>
<a:Rect>((-19568,-16912), (-18568,-12112))</a:Rect>
<a:ListOfPoints>((-19049,-12112),(-19087,-16912))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o392"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o394"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o294"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o385">
<a:CreationDate>1575360379</a:CreationDate>
<a:ModificationDate>1575360392</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-20662,11438), (-19463,12637))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o288"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o386">
<a:CreationDate>1575360407</a:CreationDate>
<a:ModificationDate>1575360460</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-26708,5488), (-11621,7487))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o287"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o388">
<a:CreationDate>1575360463</a:CreationDate>
<a:ModificationDate>1575360463</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-22012,-2387), (-16013,-388))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o290"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o390">
<a:CreationDate>1575360667</a:CreationDate>
<a:ModificationDate>1575360667</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2480,-8311), (10358,-6312))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o292"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o392">
<a:CreationDate>1575360715</a:CreationDate>
<a:ModificationDate>1575360715</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-22011,-12811), (-16012,-10812))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o270"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o394">
<a:CreationDate>1575360788</a:CreationDate>
<a:ModificationDate>1575360788</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-19837,-17662), (-18338,-16163))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o295"/>
</c:Object>
</o:EndSymbol>
</c:Symbols>
</o:ActivityDiagram>
</c:ActivityDiagrams>
<c:Starts>
<o:Start Id="o240">
<a:ObjectID>CFEE3D33-913B-4DB9-9BE8-DD1B7B639B1C</a:ObjectID>
<a:Name>Debut_1</a:Name>
<a:Code>Debut_1</a:Code>
<a:CreationDate>1575295127</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295127</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Start>
<o:Start Id="o265">
<a:ObjectID>60FBC2F6-76FF-4705-BE6E-FD0DD2AD18F8</a:ObjectID>
<a:Name>Debut_2</a:Name>
<a:Code>Debut_2</a:Code>
<a:CreationDate>1575297781</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297781</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Start>
<o:Start Id="o278">
<a:ObjectID>9E0B66E1-C034-4242-905A-39CE9AF7438A</a:ObjectID>
<a:Name>Debut_3</a:Name>
<a:Code>Debut_3</a:Code>
<a:CreationDate>1575359990</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575359990</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Start>
<o:Start Id="o288">
<a:ObjectID>0A39D4BE-2C44-42A3-A525-104B323278ED</a:ObjectID>
<a:Name>Debut_4</a:Name>
<a:Code>Debut_4</a:Code>
<a:CreationDate>1575360379</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360379</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Start>
</c:Starts>
<c:Ends>
<o:End Id="o260">
<a:ObjectID>D5D900C3-6869-437C-9A5E-7C190C471B01</a:ObjectID>
<a:Name>Fin_1</a:Name>
<a:Code>Fin_1</a:Code>
<a:CreationDate>1575295132</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575295132</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:End>
<o:End Id="o272">
<a:ObjectID>0D7E4D8D-FFF1-4852-8BBC-06859BBE9572</a:ObjectID>
<a:Name>Fin_2</a:Name>
<a:Code>Fin_2</a:Code>
<a:CreationDate>1575357267</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575357267</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:End>
<o:End Id="o285">
<a:ObjectID>A21E77F0-3985-4132-9D23-FD817FEE2ECE</a:ObjectID>
<a:Name>Fin_3</a:Name>
<a:Code>Fin_3</a:Code>
<a:CreationDate>1575360287</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360287</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:End>
<o:End Id="o295">
<a:ObjectID>F831B2C6-0D15-4EFC-8B23-CE97A209BAB4</a:ObjectID>
<a:Name>Fin_4</a:Name>
<a:Code>Fin_4</a:Code>
<a:CreationDate>1575360788</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575360788</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:End>
</c:Ends>
</o:Package>
<o:Package Id="o395">
<a:ObjectID>BFEC5C98-43E9-4C5E-A0E0-A6DF1EC3C338</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>planning</a:Code>
<a:CreationDate>1574700347</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962825</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:ClassDiagrams>
<o:ClassDiagram Id="o396">
<a:ObjectID>44D18D7F-B500-49D3-80F5-78636C0AC4EA</a:ObjectID>
<a:Name>2. Classes - Planning</a:Name>
<a:Code>2__Classes___Planning</a:Code>
<a:CreationDate>1575368194</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510840</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=Yes
Generalization.DisplayName=No
Generalization.DisplayedRules=Yes
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Realization.DisplayedStereotype=Yes
Realization.DisplayName=No
Realization.DisplayedRules=Yes
Realization_SymbolLayout=
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=Yes
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Class.Stereotype=Yes
Class.Constraint=Yes
Class.Attributes=Yes
Class.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Class.Attributes._Limit=-3
Class.Operations=Yes
Class.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Class.Operations._Limit=-3
Class.InnerClassifiers=Yes
Class.Comment=No
Class.IconPicture=No
Class.TextStyle=No
Class_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de classe&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Interface.Stereotype=Yes
Interface.Constraint=Yes
Interface.Attributes=Yes
Interface.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Interface.Attributes._Limit=-3
Interface.Operations=Yes
Interface.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Interface.Operations._Limit=-3
Interface.InnerClassifiers=Yes
Interface.Comment=No
Interface.IconPicture=No
Interface.TextStyle=No
Interface_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom d&amp;#39;interface&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Port.IconPicture=No
Port.TextStyle=No
Port_SymbolLayout=
Association.RoleAMultiplicity=Yes
Association.RoleAName=Yes
Association.RoleAOrdering=Yes
Association.DisplayedStereotype=No
Association.DisplayName=No
Association.DisplayedRules=Yes
Association.RoleBMultiplicity=Yes
Association.RoleBName=Yes
Association.RoleBOrdering=Yes
Association.RoleMultiplicitySymbol=No
Association_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité A&quot; Attribute=&quot;RoleAMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle A&quot; Attribute=&quot;RoleAName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre A&quot; Attribute=&quot;RoleAOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité B&quot; Attribute=&quot;RoleBMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle B&quot; Attribute=&quot;RoleBName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre B&quot; Attribute=&quot;RoleBOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
RequireLink.DisplayedStereotype=Yes
RequireLink.DisplayName=No
RequireLink.DisplayedRules=Yes
RequireLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
PortShowName=Yes
PortShowType=No
PortShowMult=No

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:GeneralizationSymbol Id="o397">
<a:CreationDate>1575368256</a:CreationDate>
<a:ModificationDate>1575369142</a:ModificationDate>
<a:Rect>((-25025,6391), (-12149,7391))</a:Rect>
<a:ListOfPoints>((-25025,6900),(-19650,6900),(-19650,6881),(-12149,6881))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o398"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o399"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o400"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o401">
<a:CreationDate>1575368257</a:CreationDate>
<a:ModificationDate>1575369142</a:ModificationDate>
<a:Rect>((-24599,892), (-10164,1892))</a:Rect>
<a:ListOfPoints>((-24599,1392),(-10164,1392))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o402"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o399"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o403"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o404">
<a:CreationDate>1575368259</a:CreationDate>
<a:ModificationDate>1575909002</a:ModificationDate>
<a:Rect>((-48178,-14925), (-37578,-6150))</a:Rect>
<a:ListOfPoints>((-48178,-14925),(-37578,-14925),(-37578,-6150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o405"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o406"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o407"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o408">
<a:CreationDate>1575368260</a:CreationDate>
<a:ModificationDate>1575909003</a:ModificationDate>
<a:Rect>((-33754,-15319), (-21674,-8900))</a:Rect>
<a:ListOfPoints>((-21674,-15319),(-33754,-15319),(-33754,-8900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o409"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o406"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o410"/>
</c:Object>
</o:GeneralizationSymbol>
<o:AssociationSymbol Id="o411">
<a:CreationDate>1575368344</a:CreationDate>
<a:ModificationDate>1575909007</a:ModificationDate>
<a:Rect>((-47924,-22038), (-38302,-15176))</a:Rect>
<a:ListOfPoints>((-47924,-16350),(-38302,-16350),(-39238,-22038))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o405"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o412"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o413"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o414">
<a:CreationDate>1575368436</a:CreationDate>
<a:ModificationDate>1575369444</a:ModificationDate>
<a:Rect>((-18171,-7002), (-4575,-4654))</a:Rect>
<a:ListOfPoints>((-18171,-5828),(-4575,-5828))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>3592</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o415"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o416"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o417"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o418">
<a:CreationDate>1575368457</a:CreationDate>
<a:ModificationDate>1575907407</a:ModificationDate>
<a:Rect>((-40138,-7488), (-27674,6450))</a:Rect>
<a:ListOfPoints>((-38188,-7488),(-38188,6450),(-27674,6450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o406"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o398"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o419"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o420">
<a:CreationDate>1575368459</a:CreationDate>
<a:ModificationDate>1575907407</a:ModificationDate>
<a:Rect>((-35572,-6150), (-27749,1561))</a:Rect>
<a:ListOfPoints>((-34364,-6150),(-34669,375),(-27749,375))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o406"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o402"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o421"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o422">
<a:CreationDate>1575368461</a:CreationDate>
<a:ModificationDate>1575907407</a:ModificationDate>
<a:Rect>((-33908,-7303), (-23096,-4955))</a:Rect>
<a:ListOfPoints>((-33908,-6129),(-23096,-6129))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o406"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o415"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o423"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o424">
<a:CreationDate>1575368597</a:CreationDate>
<a:ModificationDate>1575907407</a:ModificationDate>
<a:Rect>((-49243,-7991), (-35283,-5643))</a:Rect>
<a:ListOfPoints>((-35283,-6817),(-49243,-6817))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o406"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o425"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o426"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o427">
<a:CreationDate>1575370024</a:CreationDate>
<a:ModificationDate>1575909007</a:ModificationDate>
<a:Rect>((-51495,-24481), (-36115,-22133))</a:Rect>
<a:ListOfPoints>((-36115,-23307),(-51495,-23307))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o412"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o428"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o429"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o430">
<a:CreationDate>1575370028</a:CreationDate>
<a:ModificationDate>1575905103</a:ModificationDate>
<a:Rect>((-58724,-24140), (-51896,-6961))</a:Rect>
<a:ListOfPoints>((-55209,-22966),(-58724,-22966),(-58724,-6961),(-51896,-6961))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o428"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o425"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o431"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o432">
<a:CreationDate>1575905244</a:CreationDate>
<a:ModificationDate>1575909009</a:ModificationDate>
<a:Rect>((-31323,-26017), (-26321,-23669))</a:Rect>
<a:ListOfPoints>((-26321,-24843),(-31323,-24843))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o433"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o412"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o434"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o435">
<a:CreationDate>1575905293</a:CreationDate>
<a:ModificationDate>1575909012</a:ModificationDate>
<a:SourceTextOffset>(1537, 688)</a:SourceTextOffset>
<a:Rect>((-24954,-22853), (-21393,-17603))</a:Rect>
<a:ListOfPoints>((-23904,-17603),(-23904,-22853))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o409"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o433"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o436"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o437">
<a:CreationDate>1575907402</a:CreationDate>
<a:ModificationDate>1575908964</a:ModificationDate>
<a:Rect>((-62189,-2883), (-39437,-375))</a:Rect>
<a:ListOfPoints>((-62189,-1549),(-50936,-1549),(-50936,-1709),(-39437,-1709))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o438"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o406"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o439"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o406">
<a:CreationDate>1575368244</a:CreationDate>
<a:ModificationDate>1575907407</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-41092,-11545), (-31308,-756))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o440"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o405">
<a:CreationDate>1575368246</a:CreationDate>
<a:ModificationDate>1575909002</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-51299,-18186), (-45751,-14365))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o441"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o409">
<a:CreationDate>1575368248</a:CreationDate>
<a:ModificationDate>1575909003</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-25274,-18561), (-19726,-14740))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o442"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o412">
<a:CreationDate>1575368250</a:CreationDate>
<a:ModificationDate>1575909007</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-42649,-27109), (-29801,-19392))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o443"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o399">
<a:CreationDate>1575368251</a:CreationDate>
<a:ModificationDate>1575369142</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16125,-671), (-7269,8170))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o444"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o402">
<a:CreationDate>1575368252</a:CreationDate>
<a:ModificationDate>1575369142</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-28760,-1161), (-23587,2660))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o445"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o398">
<a:CreationDate>1575368253</a:CreationDate>
<a:ModificationDate>1575369142</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-28724,4839), (-23176,8660))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o446"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o415">
<a:CreationDate>1575368413</a:CreationDate>
<a:ModificationDate>1575369441</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-27146,-7986), (-16204,-4165))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o447"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o416">
<a:CreationDate>1575368416</a:CreationDate>
<a:ModificationDate>1575369444</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-9201,-9410), (351,-3641))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o448"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o425">
<a:CreationDate>1575368587</a:CreationDate>
<a:ModificationDate>1575369112</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-53290,-9092), (-44356,-4297))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o449"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o428">
<a:CreationDate>1575370003</a:CreationDate>
<a:ModificationDate>1575370371</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-56176,-26303), (-47474,-20534))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o450"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o433">
<a:CreationDate>1575905207</a:CreationDate>
<a:ModificationDate>1575909004</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-26545,-26114), (-21746,-22293))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o451"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o438">
<a:CreationDate>1575907220</a:CreationDate>
<a:ModificationDate>1575908964</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-71434,-5009), (-59360,1734))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o452"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o453"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o453">
<a:ObjectID>684E4FF0-FFE0-47B8-90BD-1B7325280FB9</a:ObjectID>
<a:Name>1. Cas d&#39;Utilisation - Planning</a:Name>
<a:Code>1__Cas_d_Utilisation___Planning</a:Code>
<a:CreationDate>1574700347</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510784</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o454">
<a:CreationDate>1575362979</a:CreationDate>
<a:ModificationDate>1575362979</a:ModificationDate>
<a:Rect>((-11943,41361), (40198,28079))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o455">
<a:CreationDate>1575362981</a:CreationDate>
<a:ModificationDate>1575367420</a:ModificationDate>
<a:Rect>((-11628,20602), (41750,-36674))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o456">
<a:Text>Reservation entrainements</a:Text>
<a:CreationDate>1575362991</a:CreationDate>
<a:ModificationDate>1575363017</a:ModificationDate>
<a:Rect>((-10672,38036), (513,41634))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:TextSymbol Id="o457">
<a:Text>Assistance planning</a:Text>
<a:CreationDate>1575363021</a:CreationDate>
<a:ModificationDate>1575366021</a:ModificationDate>
<a:Rect>((-12569,21057), (-1136,17459))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:UseCaseAssociationSymbol Id="o458">
<a:CreationDate>1575362949</a:CreationDate>
<a:ModificationDate>1575362949</a:ModificationDate>
<a:Rect>((4517,33870), (31082,33970))</a:Rect>
<a:ListOfPoints>((31082,33870),(4517,33870))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o459"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o460"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o461"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o462">
<a:CreationDate>1575362959</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-125,2801), (31660,12439))</a:Rect>
<a:ListOfPoints>((31660,2801),(31660,12439),(-125,12439))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o464"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o465"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o466">
<a:CreationDate>1575362961</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((3010,4204), (31742,6251))</a:Rect>
<a:ListOfPoints>((31742,4204),(31742,6251),(3010,6251))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o467"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o468"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o469">
<a:CreationDate>1575362962</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((887,-746), (32072,4039))</a:Rect>
<a:ListOfPoints>((32072,4039),(32072,-746),(887,-746))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o470"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o471"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o472">
<a:CreationDate>1575362965</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((1960,-5862), (31990,4121))</a:Rect>
<a:ListOfPoints>((31990,4121),(31990,-5862),(1960,-5862))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o473"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o474"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o475">
<a:CreationDate>1575362966</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((1877,-13782), (31495,3544))</a:Rect>
<a:ListOfPoints>((31495,3544),(31495,-13782),(1877,-13782))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o476"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o477"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o478">
<a:CreationDate>1575367406</a:CreationDate>
<a:ModificationDate>1575367406</a:ModificationDate>
<a:Rect>((4351,-20550), (31651,2400))</a:Rect>
<a:ListOfPoints>((31651,2400),(31651,-20550),(4351,-20550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o463"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o479"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o480"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o481">
<a:CreationDate>1575367410</a:CreationDate>
<a:ModificationDate>1575367435</a:ModificationDate>
<a:Rect>((-4761,-30450), (263,-21975))</a:Rect>
<a:ListOfPoints>((-599,-21975),(-599,-25762),(-3899,-25762),(-3899,-30450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o479"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o482"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o483"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseSymbol Id="o460">
<a:CreationDate>1575361003</a:CreationDate>
<a:ModificationDate>1575362919</a:ModificationDate>
<a:Rect>((-7940,30510), (8455,35909))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o484"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o464">
<a:CreationDate>1575361004</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-5234,9750), (7262,15149))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o485"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o470">
<a:CreationDate>1575361004</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-2411,-2625), (5486,2774))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o486"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o473">
<a:CreationDate>1575361005</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-1486,-9224), (7411,-3825))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o487"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o467">
<a:CreationDate>1575361005</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-1987,3450), (5212,8849))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o488"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o476">
<a:CreationDate>1575362808</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:Rect>((-911,-16296), (7586,-10897))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o489"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o459">
<a:CreationDate>1575362938</a:CreationDate>
<a:ModificationDate>1575362938</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((28847,33004), (33646,36603))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Shortcut Ref="o490"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o463">
<a:CreationDate>1575362943</a:CreationDate>
<a:ModificationDate>1575366055</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((29425,1482), (34224,5081))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Shortcut Ref="o491"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o479">
<a:CreationDate>1575367336</a:CreationDate>
<a:ModificationDate>1575367370</a:ModificationDate>
<a:Rect>((-4122,-24073), (7274,-18674))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o492"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o482">
<a:CreationDate>1575367380</a:CreationDate>
<a:ModificationDate>1575367415</a:ModificationDate>
<a:Rect>((-9299,-35325), (-2100,-29926))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o493"/>
</c:Object>
</o:UseCaseSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:Classes>
<o:Class Id="o440">
<a:ObjectID>3C38294D-4BBA-442E-A9F0-7F5B5EA664AD</a:ObjectID>
<a:Name>Match</a:Name>
<a:Code>Match</a:Code>
<a:CreationDate>1575368244</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575908998</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o494">
<a:ObjectID>94D4EAC6-3452-43DE-BDBE-1665F050C46A</a:ObjectID>
<a:Name>idMatch</a:Name>
<a:Code>idMatch</a:Code>
<a:CreationDate>1575369222</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369237</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o495">
<a:ObjectID>5EA96C18-7D8F-487D-8A1D-5ED94F4AFF2A</a:ObjectID>
<a:Name>date</a:Name>
<a:Code>date</a:Code>
<a:CreationDate>1575369274</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369334</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o496">
<a:ObjectID>74B10D01-1E37-41ED-A49F-C74EA07267FC</a:ObjectID>
<a:Name>horaire</a:Name>
<a:Code>horaire</a:Code>
<a:CreationDate>1575369274</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369334</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o497">
<a:ObjectID>20971DFF-06FF-4AE9-B4B0-4441AAA583CB</a:ObjectID>
<a:Name>score</a:Name>
<a:Code>score</a:Code>
<a:CreationDate>1575905632</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575905675</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int[]</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>*</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o498">
<a:ObjectID>2849A47C-5060-495B-ABA0-6364F73172F6</a:ObjectID>
<a:Name>indexGagnant</a:Name>
<a:Code>indexGagnant</a:Code>
<a:CreationDate>1575908814</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575908834</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o499">
<a:ObjectID>82D2D4AD-EEEC-4AE6-9F98-4D2EEFEDE7A8</a:ObjectID>
<a:Name>SetScore</a:Name>
<a:Code>setScore</a:Code>
<a:CreationDate>1575906016</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575906408</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o500">
<a:ObjectID>A1EE337C-7983-4D51-B04F-1C0AFF701690</a:ObjectID>
<a:Name>deplacer</a:Name>
<a:Code>deplacer</a:Code>
<a:CreationDate>1575908979</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575908998</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o501">
<a:ObjectID>AB34D354-3374-42E8-A79B-B6D0A921E1D5</a:ObjectID>
<a:Name>modifier</a:Name>
<a:Code>modifier</a:Code>
<a:CreationDate>1575908979</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575908998</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o441">
<a:ObjectID>FA3CB00F-7D2F-4764-80A8-09C75BF6CD5B</a:ObjectID>
<a:Name>Match Simple</a:Name>
<a:Code>MatchSimple</a:Code>
<a:CreationDate>1575368246</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575906742</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o442">
<a:ObjectID>F7BB1605-6259-4E3F-B2DF-2667796EFDE6</a:ObjectID>
<a:Name>Match Double</a:Name>
<a:Code>MatchDouble</a:Code>
<a:CreationDate>1575368248</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575906748</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o443">
<a:ObjectID>C2B8F3C6-2CA0-4CAC-89A0-6CE5FE0D6273</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1575368250</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909092</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o502">
<a:ObjectID>BEA35800-2F85-4773-AAE7-371CE7A1C147</a:ObjectID>
<a:Name>idJoueur</a:Name>
<a:Code>idJoueur</a:Code>
<a:CreationDate>1575369470</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369477</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o503">
<a:ObjectID>5365F4A0-8701-461A-953E-D04778E4862B</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1575369479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369594</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o504">
<a:ObjectID>53E657D5-A08A-4382-9161-222D24F99B40</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1575369479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369594</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o505">
<a:ObjectID>758A60CD-7E1E-4A87-B066-7A5AC51433EA</a:ObjectID>
<a:Name>ATP</a:Name>
<a:Code>atp</a:Code>
<a:CreationDate>1575369479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369594</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o506">
<a:ObjectID>E20FC741-A0E0-426B-858B-ACD8A094AB96</a:ObjectID>
<a:Name>nationalite</a:Name>
<a:Code>nationalite</a:Code>
<a:CreationDate>1575369479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369594</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o507">
<a:ObjectID>2254C8C9-D4B3-471E-929C-20FF2BA5D2CD</a:ObjectID>
<a:Name>demanderEntrainement</a:Name>
<a:Code>demanderEntrainement</a:Code>
<a:CreationDate>1575909075</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909092</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o444">
<a:ObjectID>EFF7D607-4FB2-40C6-A3AB-CAADCD5E962D</a:ObjectID>
<a:Name>Arbitre</a:Name>
<a:Code>Arbitre</a:Code>
<a:CreationDate>1575368251</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368782</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o508">
<a:ObjectID>78EC6E6B-63B0-4E7B-B267-5C7AF275FA2C</a:ObjectID>
<a:Name>idArbitre</a:Name>
<a:Code>idArbitre</a:Code>
<a:CreationDate>1575368643</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368732</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o509">
<a:ObjectID>6CAF6F5C-D347-4B58-B5B0-00C8E768EC36</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1575368643</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368782</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o510">
<a:ObjectID>474CD38C-99FC-4AEA-8432-48942A43A506</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1575368643</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369436</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o511">
<a:ObjectID>0BA1B9E3-EEED-4001-95CA-1B1EB3580576</a:ObjectID>
<a:Name>nationalité</a:Name>
<a:Code>nationalite</a:Code>
<a:CreationDate>1575368643</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368782</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o512">
<a:ObjectID>141A8652-6D38-42D3-A5A3-71044AD442AF</a:ObjectID>
<a:Name>catégorie</a:Name>
<a:Code>categorie</a:Code>
<a:CreationDate>1575368643</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368782</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o445">
<a:ObjectID>A58F7127-4D15-49E4-A496-863318448A27</a:ObjectID>
<a:Name>Arbitre Ligne</a:Name>
<a:Code>ArbitreLigne</a:Code>
<a:CreationDate>1575368252</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368889</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o446">
<a:ObjectID>0DB7503A-B877-4A96-81C6-7822D6C088EF</a:ObjectID>
<a:Name>Arbitre Chaise</a:Name>
<a:Code>ArbitreChaise</a:Code>
<a:CreationDate>1575368253</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368876</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o447">
<a:ObjectID>BE35FF5A-0864-4A13-B97C-4F75EF2818E0</a:ObjectID>
<a:Name>Equipe Ramasseurs</a:Name>
<a:Code>EquipeRamasseurs</a:Code>
<a:CreationDate>1575368413</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369378</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o513">
<a:ObjectID>79FA3526-48B4-4691-B82D-7BA01125303C</a:ObjectID>
<a:Name>idEquipeRamasseur</a:Name>
<a:Code>idEquipeRamasseur</a:Code>
<a:CreationDate>1575369367</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369378</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o448">
<a:ObjectID>2C3C7175-878B-4084-82B7-8CC70139D96D</a:ObjectID>
<a:Name>Ramasseur</a:Name>
<a:Code>Ramasseur</a:Code>
<a:CreationDate>1575368416</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369421</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o514">
<a:ObjectID>84E0A021-EF36-4E7C-96C5-0055F999398F</a:ObjectID>
<a:Name>idRamasseur</a:Name>
<a:Code>idRamasseur</a:Code>
<a:CreationDate>1575369383</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369401</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o515">
<a:ObjectID>1CD9D8DB-EFFF-417D-A1B9-51DA62B5C150</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1575369404</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369421</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o516">
<a:ObjectID>3C46E87A-B182-4BB1-81DB-82B4F55A813D</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1575369404</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369421</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o449">
<a:ObjectID>927C51A2-E870-49AF-A656-9C4CEC44C35A</a:ObjectID>
<a:Name>Court</a:Name>
<a:Code>Court</a:Code>
<a:CreationDate>1575368587</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370159</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o517">
<a:ObjectID>1F28A0E4-7CC5-4604-876A-7D38304D2121</a:ObjectID>
<a:Name>idCourt</a:Name>
<a:Code>idCourt</a:Code>
<a:CreationDate>1575369179</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369217</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o518">
<a:ObjectID>8AA9F72D-80B9-4841-A8EB-D3258F45F3FF</a:ObjectID>
<a:Name>principal</a:Name>
<a:Code>principal</a:Code>
<a:CreationDate>1575369179</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369217</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>boolean</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o450">
<a:ObjectID>80C3ABDD-39F6-4460-BDAF-424D2F9281FA</a:ObjectID>
<a:Name>Entrainement</a:Name>
<a:Code>Entrainement</a:Code>
<a:CreationDate>1575370003</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370159</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o519">
<a:ObjectID>321EC2E5-3BE5-4FEB-B04C-452D06FAD65A</a:ObjectID>
<a:Name>date</a:Name>
<a:Code>date</a:Code>
<a:CreationDate>1575370046</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370077</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o520">
<a:ObjectID>AFD57AAA-2E4E-4976-AEB6-E75F6534B967</a:ObjectID>
<a:Name>heureDebut</a:Name>
<a:Code>heureDebut</a:Code>
<a:CreationDate>1575370046</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370065</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o521">
<a:ObjectID>1485ADCC-2DAA-493C-935C-E4022D8AC2DA</a:ObjectID>
<a:Name>heureFin</a:Name>
<a:Code>heureFin</a:Code>
<a:CreationDate>1575370067</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370077</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o451">
<a:ObjectID>80B3E032-066F-4250-923B-2B7349BD2732</a:ObjectID>
<a:Name>DuoJoueur</a:Name>
<a:Code>DuoJoueur</a:Code>
<a:CreationDate>1575905207</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907036</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o452">
<a:ObjectID>67A5D4D1-AC45-4D3A-B7F7-0CABB7277124</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>Planning</a:Code>
<a:CreationDate>1575907220</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909954</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o522">
<a:ObjectID>E8AE4353-8F93-4BF5-8AC7-E67FA3806C0E</a:ObjectID>
<a:Name>IdPlanning</a:Name>
<a:Code>idPlanning</a:Code>
<a:CreationDate>1575907254</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907261</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o523">
<a:ObjectID>6C7AFE51-93B1-4421-B599-BC17C486D1D3</a:ObjectID>
<a:Name>dateDebut</a:Name>
<a:Code>dateDebut</a:Code>
<a:CreationDate>1575907263</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907397</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o524">
<a:ObjectID>EE60769C-E02A-49DA-B0F2-01009C2E9601</a:ObjectID>
<a:Name>dateFin</a:Name>
<a:Code>dateFin</a:Code>
<a:CreationDate>1575907263</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907397</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o525">
<a:ObjectID>A0E6176F-4453-4BCE-AD1A-1BC1608334EC</a:ObjectID>
<a:Name>Libelle</a:Name>
<a:Code>libelle</a:Code>
<a:CreationDate>1575907631</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907665</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o526">
<a:ObjectID>E176B569-B73C-4CE2-82ED-14F8C55B9C20</a:ObjectID>
<a:Name>genererPlanning</a:Name>
<a:Code>genererPlanning</a:Code>
<a:CreationDate>1575909945</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909977</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReturnType>Match[]</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o413">
<a:ObjectID>52F6D3FC-3019-4693-B3B5-392A1EC157ED</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>association1</a:Code>
<a:CreationDate>1575368344</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369097</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>0..5</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o443"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o441"/>
</c:Object2>
</o:Association>
<o:Association Id="o417">
<a:ObjectID>2B04FC53-99EC-4D84-8795-21EF52E65BE7</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>association3</a:Code>
<a:CreationDate>1575368436</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368992</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAIndicator>C</a:RoleAIndicator>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>6</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o448"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o447"/>
</c:Object2>
</o:Association>
<o:Association Id="o419">
<a:ObjectID>0AECB810-2BF5-4D57-8952-CDE538F8A705</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>association4</a:Code>
<a:CreationDate>1575368457</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368876</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>0..4</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o446"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o440"/>
</c:Object2>
</o:Association>
<o:Association Id="o421">
<a:ObjectID>AB0D7C82-DF98-4B66-8533-F604606ACEF7</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>association5</a:Code>
<a:CreationDate>1575368459</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368889</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>8</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o445"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o440"/>
</c:Object2>
</o:Association>
<o:Association Id="o423">
<a:ObjectID>F642972E-D8CF-4CE3-85DF-5E81221B5E92</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>association6</a:Code>
<a:CreationDate>1575368461</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368920</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o447"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o440"/>
</c:Object2>
</o:Association>
<o:Association Id="o426">
<a:ObjectID>0DDDC26A-CE5C-4FFD-8721-56B7ED4271CB</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>association7</a:Code>
<a:CreationDate>1575368597</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575369015</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o449"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o440"/>
</c:Object2>
</o:Association>
<o:Association Id="o429">
<a:ObjectID>6113622F-DA0E-407A-A9D7-EC265FBB34F2</a:ObjectID>
<a:Name>Association_8</a:Name>
<a:Code>association8</a:Code>
<a:CreationDate>1575370024</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370146</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>*</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o450"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o443"/>
</c:Object2>
</o:Association>
<o:Association Id="o431">
<a:ObjectID>5523D464-8B75-4ED2-A51D-E5C95698A127</a:ObjectID>
<a:Name>Association_9</a:Name>
<a:Code>association9</a:Code>
<a:CreationDate>1575370028</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575370159</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o449"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o450"/>
</c:Object2>
</o:Association>
<o:Association Id="o434">
<a:ObjectID>56EE0955-DAAE-468B-94B8-390CA26FA1B4</a:ObjectID>
<a:Name>Association_10</a:Name>
<a:Code>association10</a:Code>
<a:CreationDate>1575905244</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575905281</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o443"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o451"/>
</c:Object2>
</o:Association>
<o:Association Id="o436">
<a:ObjectID>361D830A-AE60-4570-A9C0-87CD2365FAF5</a:ObjectID>
<a:Name>Association_11</a:Name>
<a:Code>association11</a:Code>
<a:CreationDate>1575905293</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575905368</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>0..4</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o451"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o442"/>
</c:Object2>
</o:Association>
<o:Association Id="o439">
<a:ObjectID>90556AAD-4933-4541-8661-43D3BC8CBB45</a:ObjectID>
<a:Name>Association_14</a:Name>
<a:Code>association14</a:Code>
<a:CreationDate>1575907402</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575907439</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o440"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o452"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:Generalizations>
<o:Generalization Id="o400">
<a:ObjectID>96F3CF05-8DC4-482D-A460-A1C6BBE8689D</a:ObjectID>
<a:Name>Generalisation_1</a:Name>
<a:Code>Generalisation_1</a:Code>
<a:CreationDate>1575368256</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368256</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o444"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o446"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o403">
<a:ObjectID>30678D06-644C-4CCF-ACC2-EFCFE766B0DB</a:ObjectID>
<a:Name>Generalisation_2</a:Name>
<a:Code>Generalisation_2</a:Code>
<a:CreationDate>1575368257</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368257</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o444"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o445"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o407">
<a:ObjectID>8553E124-8BA1-4DAF-B401-C7314CC8146B</a:ObjectID>
<a:Name>Generalisation_3</a:Name>
<a:Code>Generalisation_3</a:Code>
<a:CreationDate>1575368259</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368259</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o440"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o441"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o410">
<a:ObjectID>66582669-18B8-4958-B29B-DCE735733053</a:ObjectID>
<a:Name>Generalisation_4</a:Name>
<a:Code>Generalisation_4</a:Code>
<a:CreationDate>1575368260</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575368260</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Class Ref="o440"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o442"/>
</c:Object2>
</o:Generalization>
</c:Generalizations>
<c:Dependencies>
<o:Dependency Id="o483">
<a:ObjectID>9D964956-3D08-425B-B904-20A9383FE96A</a:ObjectID>
<a:Name>Dependance_1</a:Name>
<a:Code>Dependance_1</a:Code>
<a:CreationDate>1575367410</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575367432</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o493"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o492"/>
</c:Object2>
</o:Dependency>
</c:Dependencies>
<c:Actors>
<o:Shortcut Id="o490">
<a:ObjectID>7E626418-75E1-4134-ADE7-6574A096C196</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1575362938</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362938</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>F0FC516A-20FD-478F-9E16-F9A0F3FA3839</a:TargetID>
<a:TargetClassID>18112101-1A4B-11D1-83D9-444553540000</a:TargetClassID>
<a:TargetPackagePath>&lt;Modèle&gt;</a:TargetPackagePath>
</o:Shortcut>
<o:Shortcut Id="o491">
<a:ObjectID>984DD277-0979-4831-B0BD-B3091722FF4D</a:ObjectID>
<a:Name>Responcable</a:Name>
<a:Code>Responcable</a:Code>
<a:CreationDate>1575362943</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362943</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>F947A99C-4325-4786-B62D-E3D3D73FBAE4</a:TargetID>
<a:TargetClassID>18112101-1A4B-11D1-83D9-444553540000</a:TargetClassID>
<a:TargetPackagePath>&lt;Modèle&gt;</a:TargetPackagePath>
</o:Shortcut>
</c:Actors>
<c:UseCases>
<o:UseCase Id="o484">
<a:ObjectID>7D70D3E5-9AC3-46F2-9AF7-C83A20A08350</a:ObjectID>
<a:Name>Reserver un cours d&#39;entrainement</a:Name>
<a:Code>Reserver_un_cours_d_entrainement</a:Code>
<a:CreationDate>1575361003</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362740</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o485">
<a:ObjectID>92E0A2F6-603C-4366-ABF5-7A7C99C60DC7</a:ObjectID>
<a:Name>Creer planning de match</a:Name>
<a:Code>Creer_planning_de_match</a:Code>
<a:CreationDate>1575361004</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362767</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o486">
<a:ObjectID>CAF4705C-810B-4FDB-9BB5-915EE738E991</a:ObjectID>
<a:Name>Modifier match</a:Name>
<a:Code>Modifier_match</a:Code>
<a:CreationDate>1575361004</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362796</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o487">
<a:ObjectID>46158188-9A64-40D7-BD59-3F6F35C529F5</a:ObjectID>
<a:Name>Supprimer match</a:Name>
<a:Code>Supprimer_match</a:Code>
<a:CreationDate>1575361005</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362806</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o488">
<a:ObjectID>BC68BEAD-278A-429E-B453-EE4C92BB9C48</a:ObjectID>
<a:Name>Inserer match</a:Name>
<a:Code>Inserer_match</a:Code>
<a:CreationDate>1575361005</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362774</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o489">
<a:ObjectID>8D7C01EC-DA74-4035-AFFD-012CDB078B2F</a:ObjectID>
<a:Name>Déplacrer match</a:Name>
<a:Code>Deplacrer_match</a:Code>
<a:CreationDate>1575362808</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362820</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o492">
<a:ObjectID>CD43E7BB-B314-4476-BA16-BDF715404127</a:ObjectID>
<a:Name>Saisir feuille de match</a:Name>
<a:Code>Saisir_feuille_de_match</a:Code>
<a:CreationDate>1575367336</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575367432</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
<o:UseCase Id="o493">
<a:ObjectID>8CBB6D44-C8A2-4727-BF7C-C76ACC5FA562</a:ObjectID>
<a:Name>Rentrer score</a:Name>
<a:Code>Rentrer_score</a:Code>
<a:CreationDate>1575367380</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575367432</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:UseCase>
</c:UseCases>
<c:UseCaseAssociations>
<o:UseCaseAssociation Id="o461">
<a:ObjectID>149AF171-A199-4CF1-8148-4991B8CF9964</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>Association_1</a:Code>
<a:CreationDate>1575362949</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362949</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o484"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o490"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o465">
<a:ObjectID>6193907A-1C74-4BA3-A587-0F9F5F6F97CC</a:ObjectID>
<a:Name>Association_2</a:Name>
<a:Code>Association_2</a:Code>
<a:CreationDate>1575362959</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362959</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o485"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o468">
<a:ObjectID>6B1EF4E7-A898-4884-8A28-07F6A14D7639</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>Association_3</a:Code>
<a:CreationDate>1575362961</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362961</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o488"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o471">
<a:ObjectID>E4EF1E9B-DB74-4834-B4E2-0DEB4D84112D</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>Association_4</a:Code>
<a:CreationDate>1575362962</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362962</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o486"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o474">
<a:ObjectID>B9C77AFA-D76B-47B0-B244-8DE9756682C6</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1575362965</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362965</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o487"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o477">
<a:ObjectID>567F2486-2216-4CA4-AE9F-A5634E15FF22</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1575362966</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575362966</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o489"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o480">
<a:ObjectID>CA4BCDB6-8149-40A1-A7B6-1FE27D187CE3</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1575367406</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575367406</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:UseCase Ref="o492"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o491"/>
</c:Object2>
</o:UseCaseAssociation>
</c:UseCaseAssociations>
<c:Flows>
<o:ActivityFlow Id="o527">
<a:ObjectID>EC50204E-64AE-45C2-B295-CD78086FAFC3</a:ObjectID>
<a:CreationDate>1575910476</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910476</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o528"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o529"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o530">
<a:ObjectID>23014A71-B8D8-483A-B584-399C97B87FED</a:ObjectID>
<a:CreationDate>1575961924</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961924</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o531"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o532"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o533">
<a:ObjectID>D785A22E-9B60-47C3-993C-FD27EDA1D467</a:ObjectID>
<a:CreationDate>1575961942</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961942</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o534"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o532"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o535">
<a:ObjectID>FF3967D1-95D6-4D79-81E1-9BF2A34DD4E5</a:ObjectID>
<a:CreationDate>1575961977</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961977</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o536"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o531"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o537">
<a:ObjectID>C0CF2F9B-C746-4157-9567-D0A5DDD245AA</a:ObjectID>
<a:CreationDate>1575961979</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961979</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o536"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o534"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o538">
<a:ObjectID>40D76E90-77AD-4839-A047-D6ADBF5C3D9F</a:ObjectID>
<a:CreationDate>1575962094</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962094</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o539"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o540"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o541">
<a:ObjectID>5A768E8F-EC59-48CB-9F80-112306FB0A27</a:ObjectID>
<a:CreationDate>1575962096</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962096</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o542"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o540"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o543">
<a:ObjectID>C1A01C6A-68C8-40DF-B3AB-F0030D7178DB</a:ObjectID>
<a:CreationDate>1575962109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962109</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o544"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o528"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o545">
<a:ObjectID>44463AD2-B01B-463B-B473-2586E943E1A8</a:ObjectID>
<a:CreationDate>1575962118</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962118</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o532"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o544"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o546">
<a:ObjectID>717FA1FA-EF72-47CE-8944-7706C824DD5D</a:ObjectID>
<a:CreationDate>1575962133</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962133</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o547"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o536"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o548">
<a:ObjectID>8BEA767F-193E-4553-A198-D6F4731D67AC</a:ObjectID>
<a:CreationDate>1575962145</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962145</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o549"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o547"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o550">
<a:ObjectID>BD52A6FD-B67A-4D71-B1D9-6C82604C32C6</a:ObjectID>
<a:CreationDate>1575962171</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962171</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o551"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o549"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o552">
<a:ObjectID>9A9ABC97-AF25-4FE7-AC6E-341FC92813D2</a:ObjectID>
<a:CreationDate>1575962178</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962178</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o540"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o551"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o553">
<a:ObjectID>C06D6EA7-490B-4AEF-90A5-D4B1AECF6B36</a:ObjectID>
<a:CreationDate>1575962318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o554"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o539"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o555">
<a:ObjectID>E4C8D396-ADDD-4A85-BBA4-179076EA631E</a:ObjectID>
<a:CreationDate>1575962320</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962320</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o554"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o542"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o556">
<a:ObjectID>495A26DE-C8BB-435F-8FF0-10736FAAAD3A</a:ObjectID>
<a:CreationDate>1575962816</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962816</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o557"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o554"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o558">
<a:ObjectID>507C5BC9-1FC2-4191-AB38-1138CA1CA2F9</a:ObjectID>
<a:CreationDate>1575962825</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962825</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o559"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o557"/>
</c:Object2>
</o:ActivityFlow>
</c:Flows>
<c:Activities>
<o:Activity Id="o531">
<a:ObjectID>C03D0802-555F-4ACC-B9A4-281553EBC84E</a:ObjectID>
<a:Name>Choisir Horaire</a:Name>
<a:Code>Choisir_Horaire</a:Code>
<a:CreationDate>1575909370</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909893</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o528">
<a:ObjectID>2EA8B997-1A2E-49F4-A1FF-D257EFDA7F71</a:ObjectID>
<a:Name>Choisir Jour</a:Name>
<a:Code>Choisir_Jour</a:Code>
<a:CreationDate>1575909895</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909902</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o549">
<a:ObjectID>4EB2F7CD-DF58-44F5-A64C-95F55E084560</a:ObjectID>
<a:Name>Selectionner les joueurs</a:Name>
<a:Code>Selectionner_les_joueurs</a:Code>
<a:CreationDate>1575909895</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909940</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o534">
<a:ObjectID>C9735D5E-18E1-4343-B8AB-D73DE15EB843</a:ObjectID>
<a:Name>Choisir cour</a:Name>
<a:Code>Choisir_cour</a:Code>
<a:CreationDate>1575909895</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910310</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o539">
<a:ObjectID>0FC8831C-60E9-45A9-9FF4-7E8AF40F273D</a:ObjectID>
<a:Name>Selectioner arbitres</a:Name>
<a:Code>Selectioner_arbitres</a:Code>
<a:CreationDate>1575910362</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910371</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o542">
<a:ObjectID>BF19976F-FC58-4398-A72B-9B0053E6F4D2</a:ObjectID>
<a:Name>Selectoionner rammasseur</a:Name>
<a:Code>Selectoionner_rammasseur</a:Code>
<a:CreationDate>1575910395</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910406</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o544">
<a:ObjectID>E3CE2007-4FEC-4492-A158-BD9733DC23DF</a:ObjectID>
<a:Name>Recuperer disponibilité</a:Name>
<a:Code>Recuperer_disponibilite</a:Code>
<a:CreationDate>1575910479</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910544</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o547">
<a:ObjectID>92867553-0B78-4F3C-BBDF-B0AF6A8BFF59</a:ObjectID>
<a:Name>Recupérer disponibilité joueur</a:Name>
<a:Code>Recuperer_disponibilite_joueur</a:Code>
<a:CreationDate>1575910589</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910622</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o551">
<a:ObjectID>92AEC026-DCE5-4D7C-A648-3FA255EEE2AD</a:ObjectID>
<a:Name>Récuperer arbitres possibles</a:Name>
<a:Code>Recuperer_arbitres_possibles</a:Code>
<a:CreationDate>1575910641</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910766</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o557">
<a:ObjectID>05E2531D-A472-464D-8B34-2DF37F282C68</a:ObjectID>
<a:Name>Confirmer Match</a:Name>
<a:Code>Confirmer_Match</a:Code>
<a:CreationDate>1575962588</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962604</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
</c:Activities>
<c:Synchronizations>
<o:Synchronization Id="o532">
<a:ObjectID>70F044BA-D5D1-474F-A2C4-FAC0ACFF8A72</a:ObjectID>
<a:Name>Synchronisation_1</a:Name>
<a:Code>Synchronisation_1</a:Code>
<a:CreationDate>1575961918</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961918</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Synchronization>
<o:Synchronization Id="o536">
<a:ObjectID>C076DC7C-AE6C-4E90-BF02-A36F71914575</a:ObjectID>
<a:Name>Synchronisation_2</a:Name>
<a:Code>Synchronisation_2</a:Code>
<a:CreationDate>1575961966</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575961966</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Synchronization>
<o:Synchronization Id="o540">
<a:ObjectID>D42C5591-948D-4EA7-ACCB-7E92B07F4ADC</a:ObjectID>
<a:Name>Synchronisation_3</a:Name>
<a:Code>Synchronisation_3</a:Code>
<a:CreationDate>1575962078</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962078</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Synchronization>
<o:Synchronization Id="o554">
<a:ObjectID>58184E6E-B6A8-46C2-A1A6-44638A7DD38D</a:ObjectID>
<a:Name>Synchronisation_4</a:Name>
<a:Code>Synchronisation_4</a:Code>
<a:CreationDate>1575962080</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575962080</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Synchronization>
</c:Synchronizations>
<c:ActivityDiagrams>
<o:ActivityDiagram Id="o560">
<a:ObjectID>15B0B3A9-9C56-4689-88C3-BA308D05E3D9</a:ObjectID>
<a:Name>3. Activité Inserer un Match - Planning</a:Name>
<a:Code>3__Activite_Inserer_un_Match___Planning</a:Code>
<a:CreationDate>1575909312</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510876</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o561">
<a:CreationDate>1575962837</a:CreationDate>
<a:ModificationDate>1575962837</a:ModificationDate>
<a:Rect>((-20399,27843), (10746,-39539))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o562">
<a:CreationDate>1575962843</a:CreationDate>
<a:ModificationDate>1575962843</a:ModificationDate>
<a:Rect>((10646,27843), (34604,-39448))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o563">
<a:CreationDate>1575962875</a:CreationDate>
<a:ModificationDate>1575962875</a:ModificationDate>
<a:Rect>((-20499,27834), (10746,24140))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o564">
<a:CreationDate>1575962878</a:CreationDate>
<a:ModificationDate>1575962882</a:ModificationDate>
<a:Rect>((10646,27835), (34604,24040))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o565">
<a:Text>Responsable</a:Text>
<a:CreationDate>1575962887</a:CreationDate>
<a:ModificationDate>1575962965</a:ModificationDate>
<a:Rect>((-8590,24179), (-194,27777))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,14,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:TextSymbol Id="o566">
<a:Text>Systeme</a:Text>
<a:CreationDate>1575962889</a:CreationDate>
<a:ModificationDate>1575962997</a:ModificationDate>
<a:Rect>((19456,24191), (26480,27790))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,14,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:FlowSymbol Id="o567">
<a:CreationDate>1575910476</a:CreationDate>
<a:ModificationDate>1575910476</a:ModificationDate>
<a:Rect>((-6968,16050), (-5968,21899))</a:Rect>
<a:ListOfPoints>((-6562,21899),(-6374,16050))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o568"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o569"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o527"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o570">
<a:CreationDate>1575961924</a:CreationDate>
<a:ModificationDate>1575962247</a:ModificationDate>
<a:Rect>((-12278,4763), (-11278,9750))</a:Rect>
<a:ListOfPoints>((-11699,9750),(-11699,7231),(-11858,7231),(-11858,4763))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o571"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o572"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o530"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o573">
<a:CreationDate>1575961942</a:CreationDate>
<a:ModificationDate>1575962230</a:ModificationDate>
<a:Rect>((289,5550), (1289,9600))</a:Rect>
<a:ListOfPoints>((751,9600),(751,7231),(826,7231),(826,5550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o571"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o574"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o533"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o575">
<a:CreationDate>1575961977</a:CreationDate>
<a:ModificationDate>1575961977</a:ModificationDate>
<a:Rect>((-12686,-412), (-11686,4013))</a:Rect>
<a:ListOfPoints>((-12186,4013),(-12186,-412))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o572"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o576"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o535"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o577">
<a:CreationDate>1575961979</a:CreationDate>
<a:ModificationDate>1575961979</a:ModificationDate>
<a:Rect>((776,-562), (1776,4763))</a:Rect>
<a:ListOfPoints>((1276,4763),(1276,-562))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o574"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o576"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o537"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o578">
<a:CreationDate>1575962094</a:CreationDate>
<a:ModificationDate>1575962163</a:ModificationDate>
<a:Rect>((-14336,-24100), (-13336,-20275))</a:Rect>
<a:ListOfPoints>((-13836,-20275),(-13836,-24100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o579"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o580"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o538"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o581">
<a:CreationDate>1575962096</a:CreationDate>
<a:ModificationDate>1575962163</a:ModificationDate>
<a:Rect>((2388,-24100), (3388,-19900))</a:Rect>
<a:ListOfPoints>((2888,-19900),(2888,-24100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o579"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o582"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o541"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o583">
<a:CreationDate>1575962109</a:CreationDate>
<a:ModificationDate>1575962112</a:ModificationDate>
<a:Rect>((-3374,12450), (25351,16200))</a:Rect>
<a:ListOfPoints>((-3374,16200),(25351,16200),(25351,12450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o569"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o584"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o543"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o585">
<a:CreationDate>1575962118</a:CreationDate>
<a:ModificationDate>1575962225</a:ModificationDate>
<a:Rect>((-5028,9150), (18676,11775))</a:Rect>
<a:ListOfPoints>((18676,11775),(-5028,11775),(-5028,9150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o584"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o571"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o545"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o586">
<a:CreationDate>1575962133</a:CreationDate>
<a:ModificationDate>1575962154</a:ModificationDate>
<a:Rect>((-5999,-2775), (17551,75))</a:Rect>
<a:ListOfPoints>((-5999,75),(-5999,-2775),(17551,-2775))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o576"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o587"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o546"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o588">
<a:CreationDate>1575962145</a:CreationDate>
<a:ModificationDate>1575962145</a:ModificationDate>
<a:Rect>((-6299,-9150), (19201,-3000))</a:Rect>
<a:ListOfPoints>((19201,-3000),(19201,-5700),(-6299,-5700),(-6299,-9150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o587"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o589"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o548"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o590">
<a:CreationDate>1575962171</a:CreationDate>
<a:ModificationDate>1575962171</a:ModificationDate>
<a:Rect>((-6524,-13800), (15601,-9600))</a:Rect>
<a:ListOfPoints>((-6524,-9600),(-6524,-13800),(15601,-13800))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o589"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o591"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o550"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o592">
<a:CreationDate>1575962178</a:CreationDate>
<a:ModificationDate>1575962178</a:ModificationDate>
<a:Rect>((-5699,-20400), (20101,-14625))</a:Rect>
<a:ListOfPoints>((20101,-14625),(20101,-17325),(-5699,-17325),(-5699,-20400))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o591"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o579"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o552"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o593">
<a:CreationDate>1575962318</a:CreationDate>
<a:ModificationDate>1575962318</a:ModificationDate>
<a:Rect>((-13811,-27300), (-12811,-24000))</a:Rect>
<a:ListOfPoints>((-13311,-24000),(-13311,-27300))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o580"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o594"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o553"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o595">
<a:CreationDate>1575962320</a:CreationDate>
<a:ModificationDate>1575962320</a:ModificationDate>
<a:Rect>((2726,-27000), (3726,-24525))</a:Rect>
<a:ListOfPoints>((3226,-24525),(3226,-27000))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o582"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o594"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o555"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o596">
<a:CreationDate>1575962816</a:CreationDate>
<a:ModificationDate>1575962816</a:ModificationDate>
<a:Rect>((-5411,-32100), (-4411,-27225))</a:Rect>
<a:ListOfPoints>((-4911,-27225),(-4911,-32100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o594"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o597"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o556"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o598">
<a:CreationDate>1575962825</a:CreationDate>
<a:ModificationDate>1575962825</a:ModificationDate>
<a:Rect>((-5140,-37582), (-4140,-32025))</a:Rect>
<a:ListOfPoints>((-4668,-32025),(-4612,-37582))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o597"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o599"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o558"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o568">
<a:CreationDate>1575909366</a:CreationDate>
<a:ModificationDate>1575909366</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-7162,21300), (-5963,22499))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o529"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o572">
<a:CreationDate>1575909370</a:CreationDate>
<a:ModificationDate>1575910791</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-15562,3875), (-9563,5874))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o531"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o569">
<a:CreationDate>1575909895</a:CreationDate>
<a:ModificationDate>1575910388</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-9037,15200), (-3038,17199))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o528"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o589">
<a:CreationDate>1575909895</a:CreationDate>
<a:ModificationDate>1575962139</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10809,-10350), (-1569,-8351))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o549"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o574">
<a:CreationDate>1575909895</a:CreationDate>
<a:ModificationDate>1575961933</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-1987,3875), (4012,5874))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o534"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o580">
<a:CreationDate>1575910362</a:CreationDate>
<a:ModificationDate>1575962163</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17183,-24800), (-9592,-22801))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o539"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o582">
<a:CreationDate>1575910395</a:CreationDate>
<a:ModificationDate>1575962163</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2256,-24950), (7884,-22951))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o542"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o584">
<a:CreationDate>1575910479</a:CreationDate>
<a:ModificationDate>1575910592</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((18292,11075), (27233,13074))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o544"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o587">
<a:CreationDate>1575910589</a:CreationDate>
<a:ModificationDate>1575962135</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((15481,-3548), (26897,-1549))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o547"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o591">
<a:CreationDate>1575910641</a:CreationDate>
<a:ModificationDate>1575962165</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((15106,-15149), (25622,-13150))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o551"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o599">
<a:CreationDate>1575910777</a:CreationDate>
<a:ModificationDate>1575962585</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-5362,-38332), (-3863,-36833))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o559"/>
</c:Object>
</o:EndSymbol>
<o:BaseSynchronizationSymbol Id="o571">
<a:CreationDate>1575961918</a:CreationDate>
<a:ModificationDate>1575962225</a:ModificationDate>
<a:Rect>((-14623,8588), (4051,10087))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o532"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o576">
<a:CreationDate>1575961966</a:CreationDate>
<a:ModificationDate>1575961972</a:ModificationDate>
<a:Rect>((-14475,-712), (3901,787))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o536"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o579">
<a:CreationDate>1575962078</a:CreationDate>
<a:ModificationDate>1575962163</a:ModificationDate>
<a:Rect>((-16704,-20800), (8102,-19301))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o540"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o594">
<a:CreationDate>1575962080</a:CreationDate>
<a:ModificationDate>1575962216</a:ModificationDate>
<a:Rect>((-17175,-27776), (8306,-26277))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o554"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:ActivitySymbol Id="o597">
<a:CreationDate>1575962588</a:CreationDate>
<a:ModificationDate>1575962588</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8484,-32724), (-1868,-30725))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o557"/>
</c:Object>
</o:ActivitySymbol>
</c:Symbols>
</o:ActivityDiagram>
</c:ActivityDiagrams>
<c:Starts>
<o:Start Id="o529">
<a:ObjectID>853762F5-C5EA-491A-8AD4-2B83DC64C78C</a:ObjectID>
<a:Name>Debut_1</a:Name>
<a:Code>Debut_1</a:Code>
<a:CreationDate>1575909366</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575909366</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Start>
</c:Starts>
<c:Ends>
<o:End Id="o559">
<a:ObjectID>3CAC68B4-BA82-44A3-A413-D427D78D8BD7</a:ObjectID>
<a:Name>Fin_1</a:Name>
<a:Code>Fin_1</a:Code>
<a:CreationDate>1575910777</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575910777</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:End>
</c:Ends>
</o:Package>
</c:Packages>
<c:Reports>
<o:IntraModelReport Id="o600">
<a:ObjectID>1CF3A992-0224-4643-A57F-89C7BAAAAE95</a:ObjectID>
<a:Name>EC-Tennis</a:Name>
<a:Code>EC_Tennis</a:Code>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportFirstPageTitle>Rapport</a:ReportFirstPageTitle>
<a:ReportFirstPageAuthor>p1800666</a:ReportFirstPageAuthor>
<a:ReportFirstPageDate>%DATE%</a:ReportFirstPageDate>
<a:HtmlStylesheetFile>Theme_BleuClair.css</a:HtmlStylesheetFile>
<a:HtmlHeaderFile>Entete_BleuClair.html</a:HtmlHeaderFile>
<a:HtmlFooterFile>PiedPage_BleuClair.html</a:HtmlFooterFile>
<a:HtmlHeaderSize>54</a:HtmlHeaderSize>
<a:HtmlFooterSize>18</a:HtmlFooterSize>
<a:HtmlTOCLevel>4</a:HtmlTOCLevel>
<a:HtmlHomePageFile>Accueil_BleuClair.html</a:HtmlHomePageFile>
<a:HtmlTemplate>Bleu_Clair</a:HtmlTemplate>
<a:RtfTemplate>Professionnel</a:RtfTemplate>
<a:RtfUseSectionHeadFoot>1</a:RtfUseSectionHeadFoot>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o601">
<a:ObjectID>B1F3D7E1-AE21-4BF8-9A08-DA0091C4B1EB</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>1</a:FontStyle>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:SpaceBefore>423</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:BorderType>15</a:BorderType>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o602">
<a:ObjectID>BDCC81D2-4313-44AA-9A6D-4C7E1157B7C6</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:Box>0</a:Box>
</o:ReportParagraph>
</c:Paragraphs>
<c:Sections>
<o:ReportSection Id="o603">
<a:ObjectID>4D3A3431-0846-4006-90A9-F3930A39E8CF</a:ObjectID>
<a:Name>Section_1</a:Name>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ModelID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:ModelID>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TEMPLATE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:ReportTemplate.Header>%MODULE%	%MODELNAME%</a:ReportTemplate.Header>
<a:ReportTemplate.Footer>%APPNAME%	%DATE%	Page %PAGE%</a:ReportTemplate.Footer>
<a:Margin>((800,900), (1100,900))</a:Margin>
<a:ReportTemplate.PaperSize>(21000, 29700)</a:ReportTemplate.PaperSize>
<c:Items>
<o:TableOfContentsReportItem Id="o604">
<a:ObjectID>E546FD55-6E69-4DCC-B853-93803E507499</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TBLCTS</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:TableOfContentsTitle>Sommaire</a:TableOfContentsTitle>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o605">
<a:ObjectID>6963BD06-BE45-4706-BD8E-D01EF2EBEE44</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o606">
<a:ObjectID>8CE54705-92AC-4F2C-8031-CE6230E5572D</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o607">
<a:ObjectID>DFAE2E8C-AD14-49EB-8CC2-044D50EF9FCE</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o608">
<a:ObjectID>F3AA7B9D-1133-49B6-B68A-6BD05C7F600F</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>350</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o609">
<a:ObjectID>7DC74636-409A-4A47-8067-91422561EE58</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o610">
<a:ObjectID>88CE11DF-6FD7-4D4D-9D81-14798A6549A1</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>700</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o611">
<a:ObjectID>CE78D5A6-6C37-45F5-9B22-B0D0931F39BD</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o612">
<a:ObjectID>5799130D-1A9C-4635-958A-2F1C178CC270</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>1050</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o613">
<a:ObjectID>5BBC29C3-9B8C-43E9-BCAE-F4ABF89C1CE3</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o614">
<a:ObjectID>C5C980CF-E685-464D-8C79-FB2F1B78BEF1</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>1400</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o615">
<a:ObjectID>21D10315-FA9C-433C-8652-E05B973B40EC</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o616">
<a:ObjectID>AFBB67FA-3297-4E30-B137-CDA182C5975C</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>1750</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o617">
<a:ObjectID>3658C051-ACB4-4CF6-B906-E5A0F0B5F7E7</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o618">
<a:ObjectID>E01AD6C2-4C77-400C-9C95-B11F50A2C8E0</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>2100</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o619">
<a:ObjectID>9D262354-4F94-4C63-8CF0-7137AF845969</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o620">
<a:ObjectID>4216585E-11A3-4673-9602-D4410BCB74B1</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>2450</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o621">
<a:ObjectID>4BB69D4A-C5DC-45E6-84C8-035364CC8C05</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
<o:ReportParagraph Id="o622">
<a:ObjectID>1DBA4C5F-65AE-41FF-951F-00AED7E39410</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Left>2800</a:Left>
<a:FillTheTabs>1</a:FillTheTabs>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o623">
<a:ObjectID>F75368F7-EDEC-4396-B805-0BA0715042F5</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportParagraph>
</c:Paragraphs>
</o:TableOfContentsReportItem>
<o:TitleReportItem Id="o624">
<a:ObjectID>DC601013-9046-4B9C-A86D-D54AA6182A47</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:TitleReportItem.Text>Introduction</a:TitleReportItem.Text>
<c:Items>
<o:TitleReportItem Id="o625">
<a:ObjectID>53D16BEE-9239-466B-942F-1223A9416B25</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:TitleReportItem.Text>Description</a:TitleReportItem.Text>
<a:TitleReportItem.ShowSubItemsInHTMLToc>0</a:TitleReportItem.ShowSubItemsInHTMLToc>
<c:Items>
<o:FreeTextReportItem Id="o626">
<a:ObjectID>FEB9DF09-71DB-469C-81BA-2D6638B0DF04</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TEXT</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<c:Paragraphs>
<o:ReportBaseParagraph Id="o627">
<a:ObjectID>481C1BE2-3B1B-439C-A07D-EEB08F65BDD7</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Box>0</a:Box>
</o:ReportBaseParagraph>
</c:Paragraphs>
</o:FreeTextReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportBaseParagraph Id="o628">
<a:ObjectID>5571E122-0852-4C8D-B4D5-F881AF5443D5</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>423</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportBaseParagraph>
</c:Paragraphs>
</o:TitleReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportBaseParagraph Id="o629">
<a:ObjectID>953451B8-9967-4AF2-B6C7-43B494EF1A8C</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>423</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportBaseParagraph>
</c:Paragraphs>
</o:TitleReportItem>
<o:PageBreakReportItem Id="o630">
<a:ObjectID>5DC65C35-4055-43C1-924A-2BEF14374152</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>PAGEBREAK</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
</o:PageBreakReportItem>
<o:TitleReportItem Id="o631">
<a:ObjectID>177DE193-BA65-4E12-B46D-D265E641D682</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:TitleReportItem.Text>Description succincte du modèle</a:TitleReportItem.Text>
<c:Items>
<o:TitleReportItem Id="o632">
<a:ObjectID>670099BE-5842-46A6-A3DF-25D95D770E58</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:TitleReportItem.Text>Objets de niveau modèle</a:TitleReportItem.Text>
<c:Items>
<o:ListReportItem Id="o633">
<a:ObjectID>5071310C-DE38-4C09-A7E1-3F7C5C84D0D0</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_MODEL_DIAG_LIST</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Liste des diagrammes</a:DefaultTitle>
<a:DefaultTitleFromResource>Liste des diagrammes</a:DefaultTitleFromResource>
<a:Title>Liste des diagrammes</a:Title>
<a:Sorted>1</a:Sorted>
<a:ListReportItem.Layout>-50 Name
-50 Code
</a:ListReportItem.Layout>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o634">
<a:ObjectID>0E03895B-469B-4B57-91B8-4A0882A6A187</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportTitleParagraph Id="o635">
<a:ObjectID>A748FA45-8646-454F-9FB5-5F9686E96914</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>2</a:FontStyle>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o636">
<a:ObjectID>D66EAE29-7407-4BC3-927D-5F91FB23EE26</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportParagraph>
</c:Paragraphs>
<c:Rows>
<o:ListRowDefinition Id="o637">
<a:ObjectID>5862BF07-7B56-466B-87F6-11CD36BA552D</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:ListRowDefinition>
</c:Rows>
</o:ListReportItem>
<o:BookTitleReportItem Id="o638">
<a:ObjectID>43D8BA87-B176-4DEA-A62A-8CB231DCB7C7</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Diagramme %ITEM%</a:Title>
<a:CollectionName>AllDiagrams</a:CollectionName>
<c:Items>
<o:UserGraphicReportItem Id="o639">
<a:ObjectID>E51AAF72-FAEC-40A6-8D08-E8374BC2402A</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_GRAPHIC</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:GraphicsFormat>1</a:GraphicsFormat>
<a:OnePageType>1</a:OnePageType>
<a:ZoomFactor>84</a:ZoomFactor>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o640">
<a:ObjectID>1FCF73C4-A5EE-42C8-84C3-9839CDF8D2EE</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:UserGraphicReportItem>
<o:AttributeTextReportItem Id="o641">
<a:ObjectID>0CD610DA-27F9-4DFF-8DDF-367E15EB1CAF</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_DESC</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Description du diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Description du diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Description du diagramme %ITEM%</a:Title>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o642">
<a:ObjectID>77770E96-F320-4A7D-BDBB-3EE14D00DF0C</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o643">
<a:ObjectID>5437F568-FFB4-4E0C-B632-69D5EC95A834</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Box>0</a:Box>
</o:ReportParagraph>
</c:Paragraphs>
</o:AttributeTextReportItem>
<o:AttributeTextReportItem Id="o644">
<a:ObjectID>3B480D56-82BC-44D9-BCEF-A3CFFF09126A</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_NOTE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Annotation du diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Annotation du diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Annotation du diagramme %ITEM%</a:Title>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o645">
<a:ObjectID>F83ACD10-3CA4-41DF-BCE0-220161D032DE</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o646">
<a:ObjectID>D9200201-529A-43A5-9989-E182A6ACA543</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Box>0</a:Box>
</o:ReportParagraph>
</c:Paragraphs>
</o:AttributeTextReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o647">
<a:ObjectID>EF3C4D9E-1E29-4E08-A3FF-AC5AF144CDD0</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:BookTitleReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportBaseParagraph Id="o648">
<a:ObjectID>D4A82BD1-E323-429D-9CED-5164E14A656F</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>423</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportBaseParagraph>
</c:Paragraphs>
</o:TitleReportItem>
<o:PackageBookReportItem Id="o649">
<a:ObjectID>13AAB086-21FC-45CE-BA6B-EE2513A348B5</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_PCKG_TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Package %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Package %ITEM%</a:DefaultTitleFromResource>
<a:Title>Package %ITEM%</a:Title>
<a:CollectionName>Packages</a:CollectionName>
<c:Items>
<o:ListReportItem Id="o650">
<a:ObjectID>EE2F8796-F736-4FAF-BF8F-B4D2CF27D00C</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_MODEL_DIAG_LIST</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Liste des diagrammes</a:DefaultTitle>
<a:DefaultTitleFromResource>Liste des diagrammes</a:DefaultTitleFromResource>
<a:Title>Liste des diagrammes</a:Title>
<a:Sorted>1</a:Sorted>
<a:ListReportItem.Layout>-50 Name
-50 Code
</a:ListReportItem.Layout>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o651">
<a:ObjectID>69937BD7-D5DB-45BE-9145-784456E7400E</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportTitleParagraph Id="o652">
<a:ObjectID>E0E4515F-1131-4D90-AFCC-1EDD465592A1</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>2</a:FontStyle>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o653">
<a:ObjectID>811236F0-164B-4644-883D-239C64B5212A</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportParagraph>
</c:Paragraphs>
<c:Rows>
<o:ListRowDefinition Id="o654">
<a:ObjectID>A46DF23E-7E8C-4DAD-A456-75471D48F6BD</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:ListRowDefinition>
</c:Rows>
</o:ListReportItem>
<o:BookTitleReportItem Id="o655">
<a:ObjectID>0D75707F-B98C-4261-AA9A-635E0C342566</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Diagramme %ITEM%</a:Title>
<a:CollectionName>AllDiagrams</a:CollectionName>
<c:Items>
<o:UserGraphicReportItem Id="o656">
<a:ObjectID>8D62D515-57E2-4817-8883-3D1B5940C3EA</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_GRAPHIC</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:GraphicsFormat>1</a:GraphicsFormat>
<a:OnePageType>1</a:OnePageType>
<a:ZoomFactor>84</a:ZoomFactor>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o657">
<a:ObjectID>389CA380-C724-4F91-B442-B9B2C46488ED</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:UserGraphicReportItem>
<o:AttributeTextReportItem Id="o658">
<a:ObjectID>FB8B1E1D-42EF-411C-AD8C-6A88ADA986BE</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_DESC</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Description du diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Description du diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Description du diagramme %ITEM%</a:Title>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o659">
<a:ObjectID>5325C07A-0E3F-4A30-864D-653EB50369E6</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o660">
<a:ObjectID>8E7BF1F6-6926-46C1-A82E-BA7AAE832AF2</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Box>0</a:Box>
</o:ReportParagraph>
</c:Paragraphs>
</o:AttributeTextReportItem>
<o:AttributeTextReportItem Id="o661">
<a:ObjectID>3A60D0B6-DE95-498C-814F-2C429DE4AD42</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_NOTE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Annotation du diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Annotation du diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Annotation du diagramme %ITEM%</a:Title>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o662">
<a:ObjectID>E255DD4B-6527-4B7E-9563-397F3F6DADCC</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o663">
<a:ObjectID>9F12829C-DA60-45AC-A3BD-7015786B2727</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:Box>0</a:Box>
</o:ReportParagraph>
</c:Paragraphs>
</o:AttributeTextReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o664">
<a:ObjectID>AB134847-60E8-44A8-B1C2-0F9A3E5466A6</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:BookTitleReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o665">
<a:ObjectID>7B191062-15C5-43E3-B459-082CF608303A</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:PackageBookReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportBaseParagraph Id="o666">
<a:ObjectID>B5549DD7-88C0-43A4-A287-0E83D244E047</a:ObjectID>
<a:CreationDate>1575975332</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575975332</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>423</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportBaseParagraph>
</c:Paragraphs>
</o:TitleReportItem>
<o:BookTitleReportItem Id="o667">
<a:ObjectID>2CB68DA1-4815-4D13-ADEF-B10BA81712A3</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_TITLE</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Diagramme %ITEM%</a:DefaultTitle>
<a:DefaultTitleFromResource>Diagramme %ITEM%</a:DefaultTitleFromResource>
<a:Title>Diagramme %ITEM%</a:Title>
<a:CollectionName>AllDiagrams</a:CollectionName>
<c:Items>
<o:ListReportItem Id="o668">
<a:ObjectID>FB930E13-7ECC-4469-925B-557C3FD0C349</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ItemTOCEntry>1</a:ItemTOCEntry>
<a:ItemInTemplate>1</a:ItemInTemplate>
<a:ItemRegistrationID>OOM_DIAG_DIAG_LIST</a:ItemRegistrationID>
<a:ItemModuleID>CLD</a:ItemModuleID>
<a:DefaultTitle>Liste des diagrammes du diagramme</a:DefaultTitle>
<a:DefaultTitleFromResource>Liste des diagrammes du diagramme</a:DefaultTitleFromResource>
<a:Title>Liste des diagrammes du diagramme</a:Title>
<a:Sorted>1</a:Sorted>
<a:ListReportItem.Layout>-50 Name
-50 Code
</a:ListReportItem.Layout>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o669">
<a:ObjectID>A50B1761-7400-4FDB-AE32-22BD7CABA63B</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
<o:ReportTitleParagraph Id="o670">
<a:ObjectID>523B02D1-432B-46D5-AA90-5749A8DC7ED8</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>2</a:FontStyle>
<a:ReportBaseParagraph.Alignment>4</a:ReportBaseParagraph.Alignment>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportTitleParagraph>
<o:ReportParagraph Id="o671">
<a:ObjectID>16EC8F2F-0571-4C87-9833-DF526234719D</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:BorderType>1</a:BorderType>
<a:ParagraphInTable>1</a:ParagraphInTable>
</o:ReportParagraph>
</c:Paragraphs>
<c:Rows>
<o:ListRowDefinition Id="o672">
<a:ObjectID>2E0560DC-791A-469A-B9A0-F2C8ED60901D</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:ListRowDefinition>
</c:Rows>
</o:ListReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportTitleParagraph Id="o673">
<a:ObjectID>D3E80520-B81B-488F-91ED-943CA0EBEC95</a:ObjectID>
<a:CreationDate>1576510318</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576510318</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Arial</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:FontStyle>5</a:FontStyle>
<a:SpaceBefore>353</a:SpaceBefore>
<a:SpaceAfter>106</a:SpaceAfter>
<a:Box>0</a:Box>
</o:ReportTitleParagraph>
</c:Paragraphs>
</o:BookTitleReportItem>
</c:Items>
<c:Paragraphs>
<o:ReportHeaderParagraph Id="o674">
<a:ObjectID>86749C34-DDCB-4547-B22D-2A765E4DF7C3</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:BorderType>8</a:BorderType>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o675">
<a:ObjectID>3B0391F3-5C4A-4BC1-8083-42919766F52E</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportHeaderParagraph>
<o:ReportFooterParagraph Id="o676">
<a:ObjectID>F7BCCCD8-B8C9-447D-B851-F7BF4A691E7F</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ReportBaseParagraph.FontName>Times New Roman</a:ReportBaseParagraph.FontName>
<a:FontHeight>10</a:FontHeight>
<a:BorderType>2</a:BorderType>
<a:Box>0</a:Box>
<c:Tabulations>
<o:ReportTabulation Id="o677">
<a:ObjectID>E473C6C7-4D95-4DAA-B8C1-07C06DDFC218</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>9550</a:Position>
<a:Alignment>2</a:Alignment>
</o:ReportTabulation>
<o:ReportTabulation Id="o678">
<a:ObjectID>FB4649EE-61AC-4D42-AC0C-474C4C55073D</a:ObjectID>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Position>19100</a:Position>
<a:Alignment>3</a:Alignment>
</o:ReportTabulation>
</c:Tabulations>
</o:ReportFooterParagraph>
</c:Paragraphs>
<c:TargetModel>
<o:TargetModel Ref="o679"/>
</c:TargetModel>
</o:ReportSection>
</c:Sections>
<c:ReportLanguages>
<o:Shortcut Id="o680">
<a:ObjectID>1697ED91-12BB-4ED6-B1F5-F6758875E3B7</a:ObjectID>
<a:Name>Français</a:Name>
<a:Code>French</a:Code>
<a:CreationDate>1575974911</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>9870ECCD-6599-11D5-A481-0008C75A860B</a:TargetID>
<a:TargetClassID>585CBB37-C940-11D1-BD0D-00A02478ECC9</a:TargetClassID>
</o:Shortcut>
</c:ReportLanguages>
<c:TargetModels>
<o:TargetModel Id="o679">
<a:ObjectID>CB523D02-7CA8-46B3-B17C-AB99E7940CD8</a:ObjectID>
<a:Name>OpenTenis</a:Name>
<a:Code>OpenTenis</a:Code>
<a:CreationDate>1575974908</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///.</a:TargetModelURL>
<a:TargetModelID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:TargetModelID>
<a:TargetModelClassID>18112060-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
</o:TargetModel>
</c:TargetModels>
</o:IntraModelReport>
</c:Reports>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o681"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o681">
<a:ObjectID>58DDB428-B582-4D52-A5BB-4C8D74662863</a:ObjectID>
<a:Name>Packages</a:Name>
<a:Code>Packages</a:Code>
<a:CreationDate>1574700250</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576511354</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=Yes
Grid size=0
Graphic unit=2
Window color=255, 255, 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255, 255, 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LCNMFont=Arial,8,N
LCNMFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:ExtendedDependencySymbol Id="o682">
<a:CreationDate>1574700707</a:CreationDate>
<a:ModificationDate>1574700735</a:ModificationDate>
<a:Rect>((-8175,13088), (3975,16238))</a:Rect>
<a:ListOfPoints>((-8175,16238),(-1350,16238),(-1350,13088),(3975,13088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o683"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o684"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o685"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o686">
<a:CreationDate>1574700709</a:CreationDate>
<a:ModificationDate>1574700725</a:ModificationDate>
<a:Rect>((-8175,7988), (3675,13088))</a:Rect>
<a:ListOfPoints>((-8175,7988),(3675,7988),(3675,13088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o687"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o684"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o688"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o689">
<a:CreationDate>1574700711</a:CreationDate>
<a:ModificationDate>1574700711</a:ModificationDate>
<a:Rect>((-8475,-1649), (4200,-1049))</a:Rect>
<a:ListOfPoints>((-8475,-1349),(4200,-1349))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o690"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o691"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o692"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o693">
<a:CreationDate>1574700712</a:CreationDate>
<a:ModificationDate>1574700731</a:ModificationDate>
<a:Rect>((-7275,-7762), (4950,-1837))</a:Rect>
<a:ListOfPoints>((-7275,-7762),(4950,-7762),(4950,-1837))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o694"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o691"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o695"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:PackageSymbol Id="o684">
<a:CreationDate>1574700346</a:CreationDate>
<a:ModificationDate>1576511331</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((2250,12338), (7049,15937))</a:Rect>
<a:LineColor>11711154</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Package Ref="o5"/>
</c:Object>
</o:PackageSymbol>
<o:PackageSymbol Id="o691">
<a:CreationDate>1574700347</a:CreationDate>
<a:ModificationDate>1574700458</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((2250,-2737), (7049,862))</a:Rect>
<a:LineColor>11711154</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Package Ref="o395"/>
</c:Object>
</o:PackageSymbol>
<o:ActorSymbol Id="o683">
<a:CreationDate>1574700462</a:CreationDate>
<a:ModificationDate>1574700735</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10575,13388), (-5776,16987))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o696"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o690">
<a:CreationDate>1574700465</a:CreationDate>
<a:ModificationDate>1574700678</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10500,-3262), (-5701,337))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o697"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o694">
<a:CreationDate>1574700467</a:CreationDate>
<a:ModificationDate>1574700689</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10200,-10312), (-5401,-6713))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o698"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o687">
<a:CreationDate>1574700581</a:CreationDate>
<a:ModificationDate>1574700717</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10350,7463), (-5551,11062))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o699"/>
</c:Object>
</o:ActorSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:Actors>
<o:Actor Id="o696">
<a:ObjectID>18AC2CF6-6A46-43AA-BB91-5D4B2C703E3D</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1574700462</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700577</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
<o:Actor Id="o697">
<a:ObjectID>F0FC516A-20FD-478F-9E16-F9A0F3FA3839</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1574700465</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700678</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
<o:Actor Id="o698">
<a:ObjectID>F947A99C-4325-4786-B62D-E3D3D73FBAE4</a:ObjectID>
<a:Name>Responcable</a:Name>
<a:Code>Responcable</a:Code>
<a:CreationDate>1574700467</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700689</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
<o:Actor Id="o699">
<a:ObjectID>4FDC965D-B5AD-48B9-AD13-7D737886A82C</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1574700581</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700585</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:Actor>
</c:Actors>
<c:OrganizationUnits>
<o:OrganizationUnit Id="o300">
<a:ObjectID>A4913730-4591-4EF4-AE92-BCEC83EB4BDC</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1575294085</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575294110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Rôle</a:Stereotype>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o301">
<a:ObjectID>557228AD-E7E1-41A2-87CB-12112C36A9B6</a:ObjectID>
<a:Name>Systeme</a:Name>
<a:Code>Systeme</a:Code>
<a:CreationDate>1575294389</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575294416</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o302">
<a:ObjectID>D5BCF03C-5093-43E9-9B02-D4C775F6C859</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1575297677</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297712</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o303">
<a:ObjectID>3A6778F1-612D-4F79-859F-E3687DF07D9E</a:ObjectID>
<a:Name>System</a:Name>
<a:Code>System</a:Code>
<a:CreationDate>1575297677</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575297716</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
</o:OrganizationUnit>
</c:OrganizationUnits>
<c:ChildExtendedDependencies>
<o:ExtendedDependency Id="o685">
<a:ObjectID>C0CC3ED5-7797-41AC-BD3E-2530642C8BBD</a:ObjectID>
<a:CreationDate>1574700707</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700707</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o5"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o696"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o688">
<a:ObjectID>91E55828-02A2-451E-85EB-C045BD7A3A32</a:ObjectID>
<a:CreationDate>1574700709</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700709</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o5"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o699"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o692">
<a:ObjectID>A8BCCEC0-5C4B-4033-8E78-DCB6596B838F</a:ObjectID>
<a:CreationDate>1574700711</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700711</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o395"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o697"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o695">
<a:ObjectID>ED4BBA00-467D-483D-98B1-0E77622DFC67</a:ObjectID>
<a:CreationDate>1574700712</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700712</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o395"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o698"/>
</c:Object2>
</o:ExtendedDependency>
</c:ChildExtendedDependencies>
<c:TargetModels>
<o:TargetModel Id="o700">
<a:ObjectID>3DE57010-05D5-424A-A07F-2DC713C53B93</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1574700249</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700249</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///%_OBJLANG%/java5-j2ee14.xol</a:TargetModelURL>
<a:TargetModelID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o701">
<a:ObjectID>328F74E9-59C7-489E-AD14-21CD20EA88D3</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1574700250</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1574700250</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///%_XEM%/WSDLJ2EE.xem</a:TargetModelURL>
<a:TargetModelID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetModelID>
<a:TargetModelClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o702">
<a:ObjectID>62306FAC-4E1D-4A13-B90D-2B5EA2606ABE</a:ObjectID>
<a:Name>OpenTenis</a:Name>
<a:Code>OpenTenis</a:Code>
<a:CreationDate>1575362938</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575970086</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///.</a:TargetModelURL>
<a:TargetModelID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:TargetModelID>
<a:TargetModelClassID>18112060-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o491"/>
<o:Shortcut Ref="o490"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o703">
<a:ObjectID>DC20DDAA-D510-4C38-8448-375E0B6F5722</a:ObjectID>
<a:Name>Français</a:Name>
<a:Code>French</a:Code>
<a:CreationDate>1575974911</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1575974911</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///%_RTPLANG%/Francais.xrl</a:TargetModelURL>
<a:TargetModelID>9870ECCD-6599-11D5-A481-0008C75A860B</a:TargetModelID>
<a:TargetModelClassID>585CBB37-C940-11D1-BD0D-00A02478ECC9</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o680"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>