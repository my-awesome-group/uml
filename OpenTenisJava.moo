<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{C3A78955-4511-433C-97FE-B21679B46701}" Label="" LastModificationDate="1576515358" Name="OpenTenisJava" Objects="728" Symbols="313" Target="Java" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.3.0.3248"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>C3A78955-4511-433C-97FE-B21679B46701</a:ObjectID>
<a:Name>OpenTenisJava</a:Name>
<a:Code>OpenTenisJava</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513351</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CFC1D5AD-E0DF-4218-BC3F-18980C26122F}
DAT 1576512834
ATT NAME
ATT DISPNAME
ATT CODE
ATT MOPT</a:History>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=

[FolderOptions\CheckModel]

[FolderOptions\CheckModel\Package]

[FolderOptions\CheckModel\Package\Circular inheritance]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\Circular dependency]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\ShortcutUniqCode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Package\ChildShortcut]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe]

[FolderOptions\CheckModel\Classe\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Persistent class]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Association Identifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Constructor return type]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Constructor modifier]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Method implementation]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Role name assignment]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Role name unicity]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanInfo]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\JavaBean]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Inheritance on Enum Type (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassBusinessImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassHomeImpl]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassEjbCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassEjbPostCreate]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassEjbFind]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassEjbHome]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\BeanClassEjbSelect]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\PKClassDefinition]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\PKClassAttributes]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\PKClassExistence]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Mapping]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\MappingSFMap]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\ClssInvalidGenMode]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe\Java_Class_Enum_Enum_ration_abstraite]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Java_Class_Enum_Enum_ration_finale]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe\Java_Class_EJB3BeanClass_Composant_EJB_manquant]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Interface]

[FolderOptions\CheckModel\Interface\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Empty classifier]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Visibility]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Interface constructor]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\Association navigability]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeCreateMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeFindMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\HomeMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\ObjectBusinessMethods]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface\CsfrWrongBound]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Attribut]

[FolderOptions\CheckModel\Classe.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Attribut\CheckDttpIncompatibleFormat]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Identifiant]

[FolderOptions\CheckModel\Classe.Identifiant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\EmptyColl - ATTR]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Identifiant\CheckIncludeColl - Clss]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut]

[FolderOptions\CheckModel\Interface.Attribut\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Datatype assignment]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Extend final class]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\DomainDivergence]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\Event parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Attribut\CheckDttpIncompatibleFormat]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Opération]

[FolderOptions\CheckModel\Classe.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Abstract operation&#39;s body]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Abstract operation]
CheckSeverity=No
FixRequested=Yes
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Operation signature]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Overriding operation]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Enum Abstract Methods]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Opération\Java_Operation_Param_tre_d_argument_variable]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Port]

[FolderOptions\CheckModel\Classe.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie]

[FolderOptions\CheckModel\Classe.Partie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Partie\PartLink]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Partie\PartComposition]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Classe.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Interface.Opération]

[FolderOptions\CheckModel\Interface.Opération\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Return type assignment]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Parameter datatype]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Interface.Opération\Java_Operation_Param_tre_d_argument_variable]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Association]

[FolderOptions\CheckModel\Association\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Association\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation]

[FolderOptions\CheckModel\Généralisation\Redundant Generalizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation\Multiple inheritance (Java)]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation\Final datatype with initial value]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation\Non-Persistent Specifying Attribute]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Généralisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Réalisation]

[FolderOptions\CheckModel\Réalisation\Redundant Realizations]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Generic links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réalisation\Bound links]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine]

[FolderOptions\CheckModel\Domaine\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\CheckNumParam]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Domaine\CheckDttpIncompatibleFormat]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Acteur]

[FolderOptions\CheckModel\Acteur\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Acteur\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Acteur\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Cas d&#39;utilisation]

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Cas d&#39;utilisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Cas d&#39;utilisation\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Objet]

[FolderOptions\CheckModel\Objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Objet\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Lien entre objets]

[FolderOptions\CheckModel\Lien entre objets\Redundant Instance links]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction]

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefDiagram]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefLifelines]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefInpMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Référence d&#39;interaction\IRefOutMsg]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Message]

[FolderOptions\CheckModel\Message\MessageNoNumber]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Message\MessageManyLinks]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Message\Actor-Message]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Activité]

[FolderOptions\CheckModel\Activité\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Activité\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Activité\CheckActvTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Activité\CheckNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Activité\CheckActvReuse]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Noeud d&#39;objet]

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud d&#39;objet\CheckObndDttp]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Décision]

[FolderOptions\CheckModel\Décision\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Décision\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Décision\CheckDcsnCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Synchronisation]

[FolderOptions\CheckModel\Synchronisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Synchronisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Synchronisation\CheckSyncCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Unité d&#39;organisation]

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Unité d&#39;organisation\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Unité d&#39;organisation\CheckPrntOrgnLoop]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Début]

[FolderOptions\CheckModel\Début\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Début\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Début\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Fin]

[FolderOptions\CheckModel\Fin\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Fin\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Fin\CheckStrtTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Transition]

[FolderOptions\CheckModel\Transition\CheckTrnsSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\CheckTrnsCond]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Transition\TrnsDuplSTAT]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Flux]

[FolderOptions\CheckModel\Flux\CheckFlowSrc]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Flux\CheckFlowNoCond]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Flux\CheckFlowCond]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Flux\FlowDuplOOMACTV]
CheckSeverity=No
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Evénement]

[FolderOptions\CheckModel\Evénement\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Evénement\EvntUnused]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat]

[FolderOptions\CheckModel\Etat\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatTrns]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\StatNoStart]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat\ActnOrder]
CheckSeverity=Yes
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action]

[FolderOptions\CheckModel\Etat.Action\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnEvent]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Etat.Action\ActnDupl]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction]

[FolderOptions\CheckModel\Point de jonction\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Point de jonction\JnPtCompl]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant]

[FolderOptions\CheckModel\Composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Single]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\EJBClassifiers]
CheckSeverity=No
FixRequested=Yes
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\Method Soap Message redefinition]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant\WSDLJava_Component_Service_Web_vide]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port]

[FolderOptions\CheckModel\Composant.Port\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Port\PortIsolated]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage]

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnNull]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Composant.Connecteur d&#39;assemblage\AscnIntf]
CheckSeverity=Yes
FixRequested=No
CheckRequested=No

[FolderOptions\CheckModel\Noeud]

[FolderOptions\CheckModel\Noeud\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Noeud\Empty Node]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant]

[FolderOptions\CheckModel\Instance de composant\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Component Instance with null Component]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Duplicate Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Instance de composant\Isolated Component Instance]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données]

[FolderOptions\CheckModel\Source de données\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\EmptyColl - MODLSRC]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Source de données\Data Source Target Consistency]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée]

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre d&#39;entrée\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie]

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Activité.Paramètre de sortie\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Réplication]

[FolderOptions\CheckModel\Réplication\PartialReplication]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion]

[FolderOptions\CheckModel\Règle de gestion\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Règle de gestion\EmptyColl - OBJCOL]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu]

[FolderOptions\CheckModel\Objet étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Objet étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu]

[FolderOptions\CheckModel\Lien étendu\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Lien étendu\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier]

[FolderOptions\CheckModel\Fichier\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Fichier\CheckPathExists]
CheckSeverity=Yes
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Format de données]

[FolderOptions\CheckModel\Format de données\UniqueNom]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Format de données\UniqueCode]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes

[FolderOptions\CheckModel\Format de données\CheckDataFormatNullExpression]
CheckSeverity=No
FixRequested=No
CheckRequested=Yes</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=Yes
DisplayName=Yes
EnableTrans=Yes
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=int
DeftParm=int
DeftCont=java.util.Collection
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; &#39;(.)+=*/&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%.3:PARENT%_%COLUMN%
ColnFKNameUse=No
PreserveMode=Yes

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No

[ModelOptions\Generate\Oom]
PreserveMode=Yes
CheckModel=Yes
SaveLinks=Yes
NameToCode=Yes
EnableTransformations=No</a:ModelOptionsText>
<c:GeneratedModels>
<o:Shortcut Id="o3">
<a:ObjectID>D7BAF9FC-7BCF-4147-A82F-CE7EF0D447F5</a:ObjectID>
<a:Name>OpenTenisJava</a:Name>
<a:Code>OPENTENISJAVA</a:Code>
<a:CreationDate>1576513352</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513352</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>ED9C452C-1B2E-4CAF-90B6-7C5F7AB6F5E9</a:TargetID>
<a:TargetClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetClassID>
</o:Shortcut>
</c:GeneratedModels>
<c:GenerationOrigins>
<o:Shortcut Id="o4">
<a:ObjectID>EFA980BB-A277-4AB3-A74F-745265565AF9</a:ObjectID>
<a:Name>OpenTenis</a:Name>
<a:Code>OpenTenis</a:Code>
<a:CreationDate>1576512834</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512834</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:TargetID>
<a:TargetClassID>18112060-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:GenerationOrigins>
<c:ObjectLanguage>
<o:Shortcut Id="o5">
<a:ObjectID>2706A906-CC4B-4D57-92D9-7C234BBC2F92</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1576512831</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512831</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ExtendedModelDefinitions>
<o:Shortcut Id="o6">
<a:ObjectID>2854F49E-5C2C-4679-82E7-36547804A715</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetID>
<a:TargetClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetClassID>
</o:Shortcut>
</c:ExtendedModelDefinitions>
<c:Packages>
<o:Package Id="o7">
<a:ObjectID>115AFFC8-0AF8-41F0-9A90-BD908EEE2E40</a:ObjectID>
<a:Name>Billetterie</a:Name>
<a:Code>billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {DAE55521-F3CE-48A2-B83E-708BBB48130E}
DAT 1576512834</a:History>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=</a:PackageOptionsText>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:ClassDiagrams>
<o:ClassDiagram Id="o8">
<a:ObjectID>CAB43A21-3129-4903-A267-53C36ABCAE41</a:ObjectID>
<a:Name>2. Classes - Billetterie</a:Name>
<a:Code>2__Classes___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F221E44B-5722-4BF1-AA67-3C7D28D9BBD9}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=Yes
Generalization.DisplayName=No
Generalization.DisplayedRules=Yes
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Realization.DisplayedStereotype=Yes
Realization.DisplayName=No
Realization.DisplayedRules=Yes
Realization_SymbolLayout=
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=Yes
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Class.Stereotype=Yes
Class.Constraint=Yes
Class.Attributes=Yes
Class.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Class.Attributes._Limit=-3
Class.Operations=Yes
Class.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Class.Operations._Limit=-3
Class.InnerClassifiers=Yes
Class.Comment=No
Class.IconPicture=No
Class.TextStyle=No
Class_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de classe&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Interface.Stereotype=Yes
Interface.Constraint=Yes
Interface.Attributes=Yes
Interface.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Interface.Attributes._Limit=-3
Interface.Operations=Yes
Interface.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Interface.Operations._Limit=-3
Interface.InnerClassifiers=Yes
Interface.Comment=No
Interface.IconPicture=No
Interface.TextStyle=No
Interface_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom d&amp;#39;interface&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Port.IconPicture=No
Port.TextStyle=No
Port_SymbolLayout=
Association.RoleAMultiplicity=Yes
Association.RoleAName=Yes
Association.RoleAOrdering=Yes
Association.DisplayedStereotype=No
Association.DisplayName=No
Association.DisplayedRules=Yes
Association.RoleBMultiplicity=Yes
Association.RoleBName=Yes
Association.RoleBOrdering=Yes
Association.RoleMultiplicitySymbol=No
Association_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité A&quot; Attribute=&quot;RoleAMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle A&quot; Attribute=&quot;RoleAName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre A&quot; Attribute=&quot;RoleAOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité B&quot; Attribute=&quot;RoleBMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle B&quot; Attribute=&quot;RoleBName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre B&quot; Attribute=&quot;RoleBOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
RequireLink.DisplayedStereotype=Yes
RequireLink.DisplayName=No
RequireLink.DisplayedRules=Yes
RequireLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
PortShowName=Yes
PortShowType=No
PortShowMult=No

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:AssociationSymbol Id="o9">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512956</a:ModificationDate>
<a:Rect>((-3261,16125), (12312,19327))</a:Rect>
<a:ListOfPoints>((-3261,16125),(2327,16125),(2327,19327),(12312,19327))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o10"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o11"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o12"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o13">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-42308,19106), (-27517,20679))</a:Rect>
<a:ListOfPoints>((-27517,20661),(-37313,20661),(-37313,19106),(-42308,19106))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o15"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o16"/>
</c:Object>
</o:AssociationSymbol>
<o:GeneralizationSymbol Id="o17">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-34730,9657), (-32274,23238))</a:Rect>
<a:ListOfPoints>((-34730,9657),(-34730,12537),(-32274,12537),(-32274,23238))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o18"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o19"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o20">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-31414,5689), (-28189,19576))</a:Rect>
<a:ListOfPoints>((-31414,5689),(-31414,10274),(-28189,10274),(-28189,19576))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o22"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o23">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-24546,-261), (-23546,23464))</a:Rect>
<a:ListOfPoints>((-24046,-261),(-24046,23464))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o24"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o25"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o26">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-19744,5722), (-16749,19577))</a:Rect>
<a:ListOfPoints>((-16749,5722),(-19744,5722),(-19744,19577))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o27"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o28"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o29">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-15942,10089), (-14585,20179))</a:Rect>
<a:ListOfPoints>((-14585,10089),(-14585,12566),(-15942,12566),(-15942,20179))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o30"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o31"/>
</c:Object>
</o:GeneralizationSymbol>
<o:AssociationSymbol Id="o32">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-33061,-7202), (-23868,-1190))</a:Rect>
<a:ListOfPoints>((-23868,-1190),(-31974,-1190),(-31974,-7202))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o24"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o33"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o34"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o35">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-36167,-8503), (-34143,4203))</a:Rect>
<a:ListOfPoints>((-35080,4203),(-35080,-8503))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o33"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o36"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o37">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:Rect>((-10664,15749), (-4087,21585))</a:Rect>
<a:ListOfPoints>((-10664,21585),(-7866,21585),(-7866,16336),(-4087,16336))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o10"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o38"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o14">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512957</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-36148,13395), (-8778,28080))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o39"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o11">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512956</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((9022,15491), (19552,22234))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o40"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o21">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-37044,2960), (-27904,6781))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o41"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o27">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17201,955), (-6285,6724))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o42"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o15">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512954</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-47837,15297), (-40061,23014))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o43"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o18">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-36384,7485), (-29486,11306))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o44"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o24">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-27690,-3259), (-18550,562))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o45"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o30">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17498,7544), (-11650,11365))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o46"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o33">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-40086,-11017), (-29248,-5248))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o47"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o10">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512955</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-6955,14215), (433,18035))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o48"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o49"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o49">
<a:ObjectID>77A36889-0566-436E-ABA9-3926DEFB0833</a:ObjectID>
<a:Name>1. Cas d&#39;Utilisation - Billetterie</a:Name>
<a:Code>1__Cas_d_Utilisation___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C563506B-865C-4E35-A466-C3D937EEF81D}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o50">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((3449,14709), (59249,-30165))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o51">
<a:Text>Systeme Achat billets</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((3744,14371), (11695,10773))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:RectangleSymbol Id="o52">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((3362,-32786), (59930,-61124))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o53">
<a:Text>Systeme Gestion Billet (back-office)</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((3456,-32795), (17343,-36395))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:DependencySymbol Id="o54">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((23417,-17790), (28517,-3090))</a:Rect>
<a:ListOfPoints>((25967,-3090),(25967,-17790))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o55"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o56"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o57"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o58">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((12825,-4214), (26529,-2668))</a:Rect>
<a:ListOfPoints>((26529,-3914),(12825,-3914))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o55"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o59"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o60"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o61">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((15512,-44855), (43412,-38735))</a:Rect>
<a:ListOfPoints>((43412,-39981),(18062,-39981),(18061,-44855))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o62"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o63"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o64"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o65">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:CenterTextOffset>(8588, 75)</a:CenterTextOffset>
<a:Rect>((18662,-52177), (42962,-46280))</a:Rect>
<a:ListOfPoints>((42962,-51006),(18662,-51006),(18861,-46280))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o66"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o63"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o67"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o68">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((45737,-47781), (70387,-39306))</a:Rect>
<a:ListOfPoints>((70387,-47781),(70387,-39306),(45737,-39306))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o69"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o62"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o70"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o71">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((47912,-53181), (70387,-47706))</a:Rect>
<a:ListOfPoints>((70387,-47706),(70387,-53181),(47912,-53181))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o69"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o66"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o72"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o73">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((12675,-21111), (17775,-15111))</a:Rect>
<a:ListOfPoints>((15375,-15111),(15375,-17920),(15075,-17920),(15075,-21111))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o74"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o75"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o76"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o77">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((17393,-2167), (24968,5304))</a:Rect>
<a:ListOfPoints>((24968,-921),(19943,-921),(19943,5304))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o55"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o78"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o79"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o80">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((47868,-47808), (70043,-45108))</a:Rect>
<a:ListOfPoints>((70043,-47808),(70043,-45108),(47868,-45108))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o69"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o81"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o82"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o83">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((19593,-45520), (36393,-43974))</a:Rect>
<a:ListOfPoints>((36393,-45220),(19593,-45220))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o81"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o63"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o84"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o85">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((36948,-57913), (70503,-47682))</a:Rect>
<a:ListOfPoints>((70503,-47682),(70503,-57913),(36948,-57913))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o69"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o86"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o87"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o88">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((14757,-58337), (39487,-45516))</a:Rect>
<a:ListOfPoints>((39487,-57091),(17307,-57091),(17307,-45516))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o86"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o63"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o89"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o90">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((12525,-12974), (17625,-5474))</a:Rect>
<a:ListOfPoints>((14250,-5474),(14250,-8901),(15900,-8901),(15900,-12974))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o59"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o74"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o91"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseAssociationSymbol Id="o92">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((26925,-2774), (64275,6676))</a:Rect>
<a:ListOfPoints>((64275,6676),(26925,6676),(26925,-2774))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o93"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o55"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o94"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o95">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((26700,-9074), (40050,-1903))</a:Rect>
<a:ListOfPoints>((26700,-3149),(37500,-3149),(37500,-9074))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o55"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o96"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o97"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o98">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((35587,-22799), (40461,-12224))</a:Rect>
<a:ListOfPoints>((38024,-22799),(38024,-12224))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o99"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o96"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o100"/>
</c:Object>
</o:DependencySymbol>
<o:DependencySymbol Id="o101">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((39825,-17549), (52162,-11337))</a:Rect>
<a:ListOfPoints>((49725,-17549),(49725,-11924),(39825,-11924))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o102"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o96"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o103"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseSymbol Id="o55">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((22300,-5340), (29499,59))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o104"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o102">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((45764,-21301), (55261,-15902))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o105"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o99">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((32777,-25893), (43973,-20494))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o106"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o93">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((61585,5402), (66384,9001))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o107"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o56">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((22404,-20940), (29603,-15541))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o108"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o74">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((11005,-16229), (19702,-10830))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o109"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o62">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((31994,-41854), (48990,-36455))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o110"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o66">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((36694,-54904), (51790,-49505))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o111"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o63">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((13267,-48530), (22864,-43131))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o112"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o69">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((68137,-49356), (72936,-45757))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o113"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o75">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((11812,-25161), (19011,-19762))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o114"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o78">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((13119,3207), (23316,8606))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o115"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o81">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((35172,-47732), (49766,-42333))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o116"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o86">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((33121,-60313), (42118,-54914))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o117"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o59">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((8876,-6973), (16773,-1574))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o118"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o96">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((32026,-14098), (42823,-8699))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o119"/>
</c:Object>
</o:UseCaseSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:SequenceDiagrams>
<o:SequenceDiagram Id="o120">
<a:ObjectID>95913845-A1FA-43AD-8AAC-9A9BDDE6C8AC</a:ObjectID>
<a:Name>3. Sequence Achat de Billet - Billetterie</a:Name>
<a:Code>3__Sequence_Achat_de_Billet___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0F586500-E818-4672-9E78-EEC53024CBB5}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\SQD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
InteractionSymbol.IconPicture=No
InteractionSymbol.TextStyle=No
InteractionSymbol_SymbolLayout=
UMLObject.Stereotype=Yes
UMLObject.HeaderAlwaysVisible=Yes
UMLObject.IconPicture=No
UMLObject.TextStyle=No
UMLObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivationSymbol.IconPicture=No
ActivationSymbol.TextStyle=No
ActivationSymbol_SymbolLayout=
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
InteractionReference.IconPicture=No
InteractionReference.TextStyle=No
InteractionReference_SymbolLayout=
InteractionFragment.IconPicture=No
InteractionFragment.TextStyle=No
InteractionFragment_SymbolLayout=
Message.BeginTime=Yes
Message.Stereotype=Yes
Message.NameOrCode=No
Message.NameOrOper=No
Message.OperAndArgs=No
Message.CondAndOper=No
Message.CondOperAndSign=Yes
Message.EndTime=Yes
Message.ActivationAttachment=No
Message_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Heure de début&quot; Attribute=&quot;BeginTime&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;NameOrCode&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;Nom&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;No&quot; Display=&quot;VerticalRadios&quot; &gt;[CRLF]   &lt;StandardAttribute Name=&quot;Nom d&amp;#39;opération&quot; Attribute=&quot;NameOrOper&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Nom d&amp;#39;opération avec signature&quot; Attribute=&quot;OperAndArgs&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Expression de séquence et nom d&amp;#39;opération&quot; Attribute=&quot;CondAndOper&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]   &lt;StandardAttribute Name=&quot;Expression de séquence et nom d&amp;#39;opération avec signature&quot; Attribute=&quot;CondOperAndSign&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;/ExclusiveChoice&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Heure de fin&quot; Attribute=&quot;EndTime&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
IRefShowStrn=Yes
FragShowLife=Yes
ShowIntrSym=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SINT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=22000
Height=28800
Brush color=255 255 255
Fill Color=No
Brush style=4
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 208 208 232
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SQDOBJT]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,U
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACTVSYM]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=900
Height=2400
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\IREF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=1031
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 208 208 232
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\IFRG]
KWRDFont=Arial,8,N
KWRDFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 255
Fill Color=Yes
Brush style=4
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=208 208 232
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\SQDMSSG]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:InteractionSymbol Id="o121">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-21550,-52351), (43752,14392))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:BaseSymbol.Flags>4</a:BaseSymbol.Flags>
<a:LineColor>15257808</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:SequenceDiagram Ref="o120"/>
</c:Object>
</o:InteractionSymbol>
<o:MessageSymbol Id="o122">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,5607), (-946,7078))</a:Rect>
<a:ListOfPoints>((-13848,5832),(-946,5832))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o124"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o125"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o126">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-842,4444), (9132,5915))</a:Rect>
<a:ListOfPoints>((-842,4669),(9132,4669))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o124"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o127"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o128"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o129">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-617,2119), (8908,3665))</a:Rect>
<a:ListOfPoints>((8908,2419),(-617,2419))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o127"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o124"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o130"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o131">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,524), (-869,2070))</a:Rect>
<a:ListOfPoints>((-869,824),(-13848,824))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o124"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o132"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o133">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-2480), (-946,-1009))</a:Rect>
<a:ListOfPoints>((-13848,-2255),(-946,-2255))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o135"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o136">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-900,-5615), (20315,-4144))</a:Rect>
<a:ListOfPoints>((-900,-5390),(20315,-5390))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o137"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o138"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o139">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((20325,-6600), (30106,-5129))</a:Rect>
<a:ListOfPoints>((20325,-6375),(30106,-6375))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o137"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o140"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o141"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o142">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((20400,-8850), (30000,-7304))</a:Rect>
<a:ListOfPoints>((30000,-8550),(20400,-8550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o140"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o137"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o143"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o144">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-525,-10200), (20100,-8654))</a:Rect>
<a:ListOfPoints>((20100,-9900),(-525,-9900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o137"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o145"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o146">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-900,-17596), (30106,-16125))</a:Rect>
<a:ListOfPoints>((-900,-17371),(30106,-17371))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o147"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o148"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o149">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-975,-19276), (30450,-17730))</a:Rect>
<a:ListOfPoints>((30450,-18976),(-975,-18976))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o147"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o150"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o151">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-12150), (-900,-10604))</a:Rect>
<a:ListOfPoints>((-13848,-11850),(-900,-11850))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o152"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o153">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-975,-12675), (30106,-11204))</a:Rect>
<a:ListOfPoints>((-975,-12450),(30106,-12450))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o154"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o155"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o156">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1350,-14025), (30150,-12479))</a:Rect>
<a:ListOfPoints>((30150,-13725),(-1350,-13725))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o154"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o157"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o158">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-20250), (-675,-18704))</a:Rect>
<a:ListOfPoints>((-13848,-19950),(-675,-19950))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o159"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o160">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-900,-20775), (30106,-19304))</a:Rect>
<a:ListOfPoints>((-900,-20550),(30106,-20550))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o161"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o162"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o163">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-975,-22725), (30000,-21179))</a:Rect>
<a:ListOfPoints>((30000,-22425),(-975,-22425))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o161"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o164"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o165">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-25875), (-750,-24329))</a:Rect>
<a:ListOfPoints>((-750,-25575),(-13848,-25575))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o134"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o166"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o167">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-28236), (-946,-26765))</a:Rect>
<a:ListOfPoints>((-13848,-28011),(-946,-28011))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o169"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o170">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1125,-31200), (38391,-29729))</a:Rect>
<a:ListOfPoints>((-1125,-30975),(38391,-30975))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o171"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o172"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o173">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-750,-32775), (38250,-31229))</a:Rect>
<a:ListOfPoints>((38250,-32475),(-750,-32475))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o171"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o174"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o175">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-35175), (-900,-33629))</a:Rect>
<a:ListOfPoints>((-900,-34875),(-13848,-34875))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o176"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o177">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-36900), (-825,-35354))</a:Rect>
<a:ListOfPoints>((-13848,-36600),(-825,-36600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o178"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o179">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1358,-38875), (6666,-36829))</a:Rect>
<a:ListOfPoints>((-946,-37275),(2654,-37275),(2654,-38875),(-946,-38875))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o180"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o181"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o182">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-41025), (-750,-39479))</a:Rect>
<a:ListOfPoints>((-750,-40725),(-13848,-40725))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o183"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o184">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-825,-43800), (38391,-42329))</a:Rect>
<a:ListOfPoints>((-825,-43575),(38391,-43575))</a:ListOfPoints>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o185"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o186"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o187">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1350,-45900), (38625,-44354))</a:Rect>
<a:ListOfPoints>((38625,-45600),(-1350,-45600))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o185"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o188"/>
</c:Object>
</o:MessageSymbol>
<o:MessageSymbol Id="o189">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-48375), (-975,-46829))</a:Rect>
<a:ListOfPoints>((-975,-48075),(-13848,-48075))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivationSymbol Ref="o168"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActorSequenceSymbol Ref="o123"/>
</c:DestinationSymbol>
<c:Object>
<o:Message Ref="o190"/>
</c:Object>
</o:MessageSymbol>
<o:ActorSequenceSymbol Id="o123">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16248,7942), (-11449,11541))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o191">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13848,-50700), (-13748,7942))</a:Rect>
<a:ListOfPoints>((-13848,7942),(-13848,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:Actor Ref="o107"/>
</c:Object>
</o:ActorSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o192">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3346,7942), (1453,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o193">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-946,-50700), (-846,7942))</a:Rect>
<a:ListOfPoints>((-946,7942),(-946,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o124">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1396,824), (-496,5842))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o134">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1396,-25575), (-496,-2245))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o168">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1396,-48075), (-496,-28001))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o180">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1096,-40425), (-196,-38865))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o194"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o195">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((35992,7942), (40791,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o196">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((38391,-50700), (38491,7942))</a:Rect>
<a:ListOfPoints>((38391,7942),(38391,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o171">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((37941,-32475), (38841,-30965))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o185">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((37941,-45600), (38841,-43565))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o197"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o198">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((17916,7942), (22715,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o199">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((20315,-50700), (20415,7942))</a:Rect>
<a:ListOfPoints>((20315,7942),(20315,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o137">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((19865,-9900), (20765,-5380))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o200"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o201">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((6733,7942), (11532,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o202">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((9132,-50700), (9232,7942))</a:Rect>
<a:ListOfPoints>((9132,7942),(9132,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o127">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((8682,2419), (9582,4679))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o203"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:UMLObjectSequenceSymbol Id="o204">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((27707,7942), (32506,11541))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJSTRN 0 Arial,8,N
DISPNAME 0 Arial,8,U</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:SlaveSubSymbols>
<o:LifelineSymbol Id="o205">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((30106,-50700), (30206,7942))</a:Rect>
<a:ListOfPoints>((30106,7942),(30106,-50700))</a:ListOfPoints>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>33023</a:LineColor>
<a:DashStyle>3</a:DashStyle>
<a:ShadowColor>8421504</a:ShadowColor>
</o:LifelineSymbol>
<o:ActivationSymbol Id="o140">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((29656,-8550), (30556,-6365))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o147">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((29656,-18976), (30556,-17361))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o154">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((29656,-13725), (30556,-12440))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
<o:ActivationSymbol Id="o161">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((29656,-22425), (30556,-20540))</a:Rect>
<a:LineColor>8388608</a:LineColor>
<a:FillColor>13890042</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
</o:ActivationSymbol>
</c:SlaveSubSymbols>
<c:Object>
<o:UMLObject Ref="o206"/>
</c:Object>
</o:UMLObjectSequenceSymbol>
<o:InteractionFragmentSymbol Id="o207">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20325,-23402), (42751,-3902))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:AttachedLifelines>
<o:UMLObjectSequenceSymbol Ref="o192"/>
<o:UMLObjectSequenceSymbol Ref="o195"/>
<o:UMLObjectSequenceSymbol Ref="o198"/>
<o:UMLObjectSequenceSymbol Ref="o201"/>
<o:UMLObjectSequenceSymbol Ref="o204"/>
<o:ActorSequenceSymbol Ref="o123"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o208"/>
</c:Object>
</o:InteractionFragmentSymbol>
<o:InteractionFragmentSymbol Id="o209">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-19875,-33451), (42075,-29400))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:AttachedLifelines>
<o:ActorSequenceSymbol Ref="o123"/>
<o:UMLObjectSequenceSymbol Ref="o192"/>
<o:UMLObjectSequenceSymbol Ref="o195"/>
<o:UMLObjectSequenceSymbol Ref="o198"/>
<o:UMLObjectSequenceSymbol Ref="o201"/>
<o:UMLObjectSequenceSymbol Ref="o204"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o210"/>
</c:Object>
</o:InteractionFragmentSymbol>
<o:InteractionFragmentSymbol Id="o211">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20025,-46350), (41400,-42150))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>KWRD 0 Arial,8,N</a:FontList>
<a:BrushStyle>4</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>15257808</a:GradientEndColor>
<c:AttachedLifelines>
<o:ActorSequenceSymbol Ref="o123"/>
<o:UMLObjectSequenceSymbol Ref="o192"/>
<o:UMLObjectSequenceSymbol Ref="o195"/>
<o:UMLObjectSequenceSymbol Ref="o198"/>
<o:UMLObjectSequenceSymbol Ref="o201"/>
<o:UMLObjectSequenceSymbol Ref="o204"/>
</c:AttachedLifelines>
<c:Object>
<o:InteractionFragment Ref="o212"/>
</c:Object>
</o:InteractionFragmentSymbol>
</c:Symbols>
</o:SequenceDiagram>
</c:SequenceDiagrams>
<c:InteractionFragments>
<o:InteractionFragment Id="o208">
<a:ObjectID>35B09555-CC7E-4122-A78F-F72E3CB5A3CB</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>1000</a:Size>
<a:FragmentType>alt</a:FragmentType>
<c:Regions>
<o:InteractionFragment Id="o213">
<a:ObjectID>A2094D3A-785E-4EA4-B6CE-A3F097D7F43D</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>11851</a:Size>
<a:FragmentType>opt</a:FragmentType>
<a:Condition>Cours Central = oui</a:Condition>
</o:InteractionFragment>
<o:InteractionFragment Id="o214">
<a:ObjectID>5CEFC542-9AF2-459B-890B-651C3444DD8B</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>7650</a:Size>
<a:FragmentType>opt</a:FragmentType>
<a:Condition>Cours Central = non</a:Condition>
</o:InteractionFragment>
</c:Regions>
</o:InteractionFragment>
<o:InteractionFragment Id="o210">
<a:ObjectID>8CA3B104-BCD0-426D-B45C-A4ADE0920C4B</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>4051</a:Size>
<a:FragmentType>loop</a:FragmentType>
<a:Condition>foreach billet</a:Condition>
</o:InteractionFragment>
<o:InteractionFragment Id="o212">
<a:ObjectID>B0CC229F-58D5-4C5F-8B36-34B903F3B077</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Size>1000</a:Size>
<a:FragmentType>loop</a:FragmentType>
<a:Condition>foreach bilet</a:Condition>
</o:InteractionFragment>
</c:InteractionFragments>
<c:Classes>
<o:Class Id="o39">
<a:ObjectID>A556E69E-E3A6-4008-8090-806CAE17CC32</a:ObjectID>
<a:Name>Billet</a:Name>
<a:Code>Billet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {4EEE8D4C-8BE3-4BBA-A210-59505407B15A}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o215">
<a:ObjectID>44225CDF-C925-441F-BE3F-9DDDD532D3FE</a:ObjectID>
<a:Name>numBillet</a:Name>
<a:Code>numBillet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512906</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E4B5198F-16A2-4CD1-ACB2-38B861DAB6E1}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o216">
<a:ObjectID>59FEA728-4726-4821-B5DC-FBC2FF757AB5</a:ObjectID>
<a:Name>dateValidite</a:Name>
<a:Code>dateValidite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E2C74504-5268-47F2-B610-C998A2F6D451}
DAT 1576512834</a:History>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o217">
<a:ObjectID>51755578-06D6-42C0-AC1D-2381B342C95B</a:ObjectID>
<a:Name>terrain</a:Name>
<a:Code>terrain</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E05497DA-781A-4D1B-B0B1-43C306F48F13}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o218">
<a:ObjectID>F436A742-099B-45A5-AEEE-5C7EE1E1F205</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6508E540-AC00-4BCC-B379-3D697A8C4A32}
DAT 1576512834</a:History>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o219">
<a:ObjectID>AD2D9F52-C284-4DC1-AB7E-A7180E1BB204</a:ObjectID>
<a:Name>getNumBillet</a:Name>
<a:Code>getNumBillet</a:Code>
<a:CreationDate>1576512903</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512906</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>int</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o215"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o220">
<a:ObjectID>D8D376E9-7A04-43AC-82DD-780456FD20D5</a:ObjectID>
<a:Name>setNumBillet</a:Name>
<a:Code>setNumBillet</a:Code>
<a:CreationDate>1576512903</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512906</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o221">
<a:ObjectID>3100BADE-7E7E-48FA-B4AD-327833926322</a:ObjectID>
<a:Name>newNumBillet</a:Name>
<a:Code>newNumBillet</a:Code>
<a:CreationDate>1576512903</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512906</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>int</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o215"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o222">
<a:ObjectID>08070E9C-1B84-42AC-8CC5-4235C53376A8</a:ObjectID>
<a:Name>Billet</a:Name>
<a:Code>Billet</a:Code>
<a:CreationDate>1576512931</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512948</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Constructor</a:Stereotype>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Class Ref="o39"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o223">
<a:ObjectID>A361DCF4-3133-48CF-B183-C177BF6AFA85</a:ObjectID>
<a:Name>Billet</a:Name>
<a:Code>Billet</a:Code>
<a:CreationDate>1576512939</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512948</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Copy constructor</a:Stereotype>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o224">
<a:ObjectID>3876AB33-D91B-4034-884C-7A1CAD215030</a:ObjectID>
<a:Name>oldBillet</a:Name>
<a:Code>oldBillet</a:Code>
<a:CreationDate>1576512939</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512948</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>Billet</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
<c:ObjectDataType>
<o:Class Ref="o39"/>
</c:ObjectDataType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Class Ref="o39"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o225">
<a:ObjectID>D68FBB15-530D-4B8C-94FF-B5A8D112CB6C</a:ObjectID>
<a:Name>getDateValidite</a:Name>
<a:Code>getDateValidite</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>Date</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o216"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o226">
<a:ObjectID>989BEBD9-7BBF-48A7-AE68-50CFF28FBBBD</a:ObjectID>
<a:Name>setDateValidite</a:Name>
<a:Code>setDateValidite</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o227">
<a:ObjectID>51C690E0-E2F3-457D-BDC0-686237F37FFE</a:ObjectID>
<a:Name>newDateValidite</a:Name>
<a:Code>newDateValidite</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>Date</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o216"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o228">
<a:ObjectID>C1F4580D-19D0-46BA-9D8A-7314B29E8B8E</a:ObjectID>
<a:Name>getTerrain</a:Name>
<a:Code>getTerrain</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>String</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o217"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o229">
<a:ObjectID>16FC39E2-BD03-4A99-B538-34550F248257</a:ObjectID>
<a:Name>setTerrain</a:Name>
<a:Code>setTerrain</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o230">
<a:ObjectID>EA139E72-E722-42E1-A91B-DD661BFF6A35</a:ObjectID>
<a:Name>newTerrain</a:Name>
<a:Code>newTerrain</a:Code>
<a:CreationDate>1576513195</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513195</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>String</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o217"/>
</c:InfluentObject>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o40">
<a:ObjectID>84531042-2031-4DA9-8166-A93FEA9F4F04</a:ObjectID>
<a:Name>Categorie</a:Name>
<a:Code>Categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F66AA8FA-5184-42EE-8363-2D6E59CB2EC4}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o231">
<a:ObjectID>85475A91-CC0C-4A5E-BF87-4F8602046B6D</a:ObjectID>
<a:Name>numCat</a:Name>
<a:Code>numCat</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C7554FE8-17C9-4D17-8890-0C311D8668E1}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o232">
<a:ObjectID>88D1907B-E25A-4F5B-B1AA-B18995EFE5BF</a:ObjectID>
<a:Name>prix</a:Name>
<a:Code>prix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {296A9ABD-4E96-40C1-A93C-2D49E2813BE5}
DAT 1576512834</a:History>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o233">
<a:ObjectID>81B9B6AC-FE60-4EA0-81B6-546F8726CDB9</a:ObjectID>
<a:Name>nbPlaces</a:Name>
<a:Code>nbPlaces</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {25803A13-D6C8-4F92-B55E-306E7A1E1A9D}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o234">
<a:ObjectID>A4123AC1-A48D-4594-8D9F-69108B56AEC2</a:ObjectID>
<a:Name>definirPrixCat</a:Name>
<a:Code>definirPrixCat</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {7A041DB0-0A6F-4FC2-ACE6-9DF3CB116170}
DAT 1576512834</a:History>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o235">
<a:ObjectID>D6714342-C33D-471D-82ED-09BF7785EC21</a:ObjectID>
<a:Name>definirNbPlaces</a:Name>
<a:Code>definirNbPlaces</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {ECBCBD6B-93DB-4C70-95DA-118C53BF2E3E}
DAT 1576512834</a:History>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o41">
<a:ObjectID>B9799BC8-3E05-47A9-B579-04222745E537</a:ObjectID>
<a:Name>BilletSolidarite</a:Name>
<a:Code>BilletSolidarite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {76961808-EC73-4EC6-AB16-677FC30C7CB9}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Operations>
<o:Operation Id="o236">
<a:ObjectID>DEF3A9FF-E3B5-40F2-97F8-7EDEAE425823</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5D880996-D1E0-4007-80CE-3F6E1923AACE}
DAT 1576512834</a:History>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o42">
<a:ObjectID>AE51E463-453E-4FDF-A46A-1A79CC2CC4A6</a:ObjectID>
<a:Name>BilletLicencie</a:Name>
<a:Code>BilletLicencie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {403336A3-FC17-4F52-9A62-04289117AF7F}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o237">
<a:ObjectID>F0A0E217-77F8-421E-A12B-19544504CDF0</a:ObjectID>
<a:Name>numLicence</a:Name>
<a:Code>numLicence</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {EFF751C0-AC0C-4665-90FA-BF685FF18188}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o238">
<a:ObjectID>A0D556F4-DD1A-41DA-BEFA-354794201067</a:ObjectID>
<a:Name>reduction</a:Name>
<a:Code>reduction</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BDF3A9A6-0BF4-4004-8798-6ADD044B3D6D}
DAT 1576512834</a:History>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Static>1</a:Static>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o239">
<a:ObjectID>DF4AED39-90C2-416A-B552-A9BB3B298B69</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8BFA8366-DE3F-48D3-9987-DB0E75BE7B2C}
DAT 1576512834</a:History>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o240">
<a:ObjectID>BE7F0ADA-A356-4EC2-A55D-ADB17A2E9F7C</a:ObjectID>
<a:Name>definirReduction</a:Name>
<a:Code>definirReduction</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1399EE53-6D1D-4165-AD65-1B687C19826A}
DAT 1576512834</a:History>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o43">
<a:ObjectID>D5052D37-DB4D-4C95-AD9B-0E31F4729359</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E84BE3D0-65DB-4B82-9AA7-049862E6C6A2}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o241">
<a:ObjectID>DCB0BB42-4AEC-4771-9248-FE9DF7022D8D</a:ObjectID>
<a:Name>idClient</a:Name>
<a:Code>idClient</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {FB2CC89A-27D6-4E26-9172-8CEE599275C6}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o242">
<a:ObjectID>A63874E5-4820-45D8-9C99-8234080B2841</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {EC964C60-F0CE-403E-BEA9-A056822016BD}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o243">
<a:ObjectID>7F57C92B-A2F2-4C21-9286-B0AC6EB0A10E</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {7981E074-7A55-457B-9B0F-826BFBF9C1E8}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o244">
<a:ObjectID>E8776142-579D-4416-A103-E47409481A7E</a:ObjectID>
<a:Name>mail</a:Name>
<a:Code>mail</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BF285AA0-2BAE-4ADA-95F4-535CBC24A522}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o245">
<a:ObjectID>0E01D0BD-9741-4B3D-81C3-F4291B458906</a:ObjectID>
<a:Name>adresse</a:Name>
<a:Code>adresse</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E23ABA8A-BF5C-49AA-8F87-B5D6CD41D382}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o44">
<a:ObjectID>B05C3645-3C66-46CF-8714-720F00E4F2A7</a:ObjectID>
<a:Name>BilletGrandPublic</a:Name>
<a:Code>BilletGrandPublic</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E0610823-0959-42A5-A75B-2A56533CA1C9}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o45">
<a:ObjectID>B1849777-36C8-45D2-82C3-2779A45E1D1F</a:ObjectID>
<a:Name>BilletPromo</a:Name>
<a:Code>BilletPromo</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {7BEEA596-AC35-460D-81E9-4F291027E2A9}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Operations>
<o:Operation Id="o246">
<a:ObjectID>5F586B95-5C9D-49CE-A662-DDFD9F1E6FB0</a:ObjectID>
<a:Name>calculerPrix</a:Name>
<a:Code>calculerPrix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9E384C10-586E-40C6-9279-262D18086A64}
DAT 1576512834</a:History>
<a:ReturnType>float</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o46">
<a:ObjectID>6BBA083F-6DC4-4F8B-A8EE-9F009BE6BFD1</a:ObjectID>
<a:Name>BilletBigMatch</a:Name>
<a:Code>BilletBigMatch</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {926FCF32-8673-4EF1-ACCC-6E5C08F33460}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o47">
<a:ObjectID>119E902A-F639-497C-A0D9-98BB911E9076</a:ObjectID>
<a:Name>CodePromo</a:Name>
<a:Code>CodePromo</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {395E8D46-63B0-4922-AA9A-625225065AD7}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o247">
<a:ObjectID>5737EF8B-1315-41F0-8084-22EBC09A9E76</a:ObjectID>
<a:Name>nbUtilisation</a:Name>
<a:Code>nbUtilisation</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6BF40EE0-DF17-4A0D-B33A-18BE212FCA8E}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o248">
<a:ObjectID>1E88BC5F-BB94-48CB-8D85-030717637641</a:ObjectID>
<a:Name>code</a:Name>
<a:Code>code</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {40A18EA0-5E43-4848-930B-C4EDA21C755B}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o249">
<a:ObjectID>90E15577-3318-4C9E-97BF-30BB603A81E7</a:ObjectID>
<a:Name>reduction</a:Name>
<a:Code>reduction</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1A8F5E0B-53E1-4F85-A847-7366DAAAFC1C}
DAT 1576512834</a:History>
<a:DataType>float</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o250">
<a:ObjectID>998399E0-72C6-4D78-9D42-2199F4D9659B</a:ObjectID>
<a:Name>definirReduction</a:Name>
<a:Code>definirReduction</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D80FE90C-93DB-4FC2-9C6C-4AD516FA5657}
DAT 1576512834</a:History>
<a:ReturnType>void</a:ReturnType>
<a:Operation.Static>1</a:Operation.Static>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o48">
<a:ObjectID>A394D07A-3992-429D-AF54-580DA49949C9</a:ObjectID>
<a:Name>Place</a:Name>
<a:Code>Place</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {13AB075E-A6D6-4D49-A1AF-A5CFCD205DD9}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o251">
<a:ObjectID>4C08B1C8-45C2-40F9-807C-785A51BD0BC2</a:ObjectID>
<a:Name>numPlace</a:Name>
<a:Code>numPlace</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5932AF78-58DC-4B65-8EAB-1916F977FEBB}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o12">
<a:ObjectID>9D7EB121-455E-4435-ACDA-32F66FDB924B</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>association1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CF63A57A-40EB-4328-B41B-49EEBC58553D}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o40"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o48"/>
</c:Object2>
</o:Association>
<o:Association Id="o16">
<a:ObjectID>96A63C59-E641-4870-BC36-B960A544705E</a:ObjectID>
<a:Name>Association_2</a:Name>
<a:Code>association2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {DCDCA8A7-C57A-4C71-9AD7-EB1F78892F96}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>0..3</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o43"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o39"/>
</c:Object2>
</o:Association>
<o:Association Id="o34">
<a:ObjectID>0E708C0C-A3E7-4D75-A644-C45856F18027</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>association3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A4F8A05A-9BAE-4BC5-9989-97E0CBB0F1D1}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o47"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o45"/>
</c:Object2>
</o:Association>
<o:Association Id="o36">
<a:ObjectID>128BDD9C-060B-499D-82C6-CB67C85DDEF8</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>association4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A52A5512-6A6F-44C5-9640-CCEE912F7DA5}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o47"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o41"/>
</c:Object2>
</o:Association>
<o:Association Id="o38">
<a:ObjectID>C7E69173-C83C-4794-8B60-69807D235708</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>association5</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A0A1DB54-88AB-439A-A71D-CD038F45F1A1}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o48"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o39"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:Generalizations>
<o:Generalization Id="o19">
<a:ObjectID>985F2994-6200-47F0-823B-A5C0E0C80EB2</a:ObjectID>
<a:Name>Generalisation_6</a:Name>
<a:Code>Generalisation_6</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {31955DAD-5C3E-41DD-BB86-FAEC660E8701}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o39"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o44"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o22">
<a:ObjectID>E973C44F-F9C1-4969-9B4E-40F77C22A7FE</a:ObjectID>
<a:Name>Generalisation_7</a:Name>
<a:Code>Generalisation_7</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {86034A41-DAB8-4F8D-9D65-68922284C136}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o39"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o41"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o25">
<a:ObjectID>920AF358-3FAA-4C43-968F-B3E28622F5DA</a:ObjectID>
<a:Name>Generalisation_8</a:Name>
<a:Code>Generalisation_8</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A5B0EE3F-1C38-4321-9A0E-17F80924AA4F}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o39"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o45"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o28">
<a:ObjectID>73E14A57-8041-4D48-A4CF-162C73CF4972</a:ObjectID>
<a:Name>Generalisation_9</a:Name>
<a:Code>Generalisation_9</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2105DC48-5D2C-44AB-8806-9FF15D9A36A0}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o39"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o42"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o31">
<a:ObjectID>234E7B6F-2B74-4925-8BFA-FBE9851ECB0D</a:ObjectID>
<a:Name>Generalisation_10</a:Name>
<a:Code>Generalisation_10</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {04256870-0970-4F54-BA96-5F52B420D7D9}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o39"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o46"/>
</c:Object2>
</o:Generalization>
</c:Generalizations>
<c:Dependencies>
<o:Dependency Id="o57">
<a:ObjectID>46C9F25E-6AAF-401D-A49B-DE7E9A23E453</a:ObjectID>
<a:Name>Dependance_4</a:Name>
<a:Code>Dependance_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6A649A07-C0E6-4A84-B5F1-CA6F4606BCF8}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o108"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o104"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o60">
<a:ObjectID>E60295E2-CDFF-4599-9265-9607E3323B41</a:ObjectID>
<a:Name>Dependance_5</a:Name>
<a:Code>Dependance_5</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8DF18C34-8B0F-4192-858A-DFC7703A54B5}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o118"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o104"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o64">
<a:ObjectID>899EDB03-A352-43DD-8939-C5177E630F9A</a:ObjectID>
<a:Name>Dependance_9</a:Name>
<a:Code>Dependance_9</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5034D996-8996-4E9D-A3B1-1D307961D791}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o112"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o110"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o67">
<a:ObjectID>8C5194F1-4AC9-42D4-B0C2-A124F37798EC</a:ObjectID>
<a:Name>Dependance_10</a:Name>
<a:Code>Dependance_10</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2B36EBF1-4018-40ED-8CDC-672B161185E6}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o112"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o111"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o76">
<a:ObjectID>84C803B4-D153-4993-8A5A-755943D538E8</a:ObjectID>
<a:Name>Dependance_11</a:Name>
<a:Code>Dependance_11</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8EE6F839-163D-4635-9922-3BAF70838E9C}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o114"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o109"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o79">
<a:ObjectID>4825C171-67B2-47F6-8F2A-C00372762F7B</a:ObjectID>
<a:Name>Dependance_12</a:Name>
<a:Code>Dependance_12</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F0324474-3C4B-4486-A0A0-E033430977A4}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o115"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o104"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o84">
<a:ObjectID>FC50611D-A7E9-476E-B4CB-23CC2E694A14</a:ObjectID>
<a:Name>Dependance_13</a:Name>
<a:Code>Dependance_13</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0F16B282-A016-4764-A05D-851B61F42DD4}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o112"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o116"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o89">
<a:ObjectID>B4F94FF2-4451-4252-9A5C-784DC81ADB04</a:ObjectID>
<a:Name>Dependance_14</a:Name>
<a:Code>Dependance_14</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {37A7DD45-7249-469E-8694-F18F71651A34}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o112"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o117"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o91">
<a:ObjectID>903E5BF7-00AC-4A73-B908-6DAA35CDB62F</a:ObjectID>
<a:Name>Dependance_15</a:Name>
<a:Code>Dependance_15</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A847684D-F5B5-4CF0-9939-584AF8C62EB2}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o109"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o118"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o97">
<a:ObjectID>4859606F-D033-4A87-A12B-D4A1310DCA5D</a:ObjectID>
<a:Name>Dependance_16</a:Name>
<a:Code>Dependance_16</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6D580827-A152-4F8C-9C1D-812FB8E7CC01}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o119"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o104"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o100">
<a:ObjectID>74CD8BB0-D1D0-4582-86DC-A77107C480E8</a:ObjectID>
<a:Name>Dependance_17</a:Name>
<a:Code>Dependance_17</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F5DF42C4-C623-42D6-8394-E63F71E16B61}
DAT 1576512834</a:History>
<a:Stereotype>extend</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o119"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o106"/>
</c:Object2>
</o:Dependency>
<o:Dependency Id="o103">
<a:ObjectID>3EB2CA7E-CBB3-4D0C-B190-3B7FD45B9577</a:ObjectID>
<a:Name>Dependance_18</a:Name>
<a:Code>Dependance_18</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {37790465-A0C5-4DD7-BD3B-21081876720F}
DAT 1576512834</a:History>
<a:Stereotype>extend</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o119"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o105"/>
</c:Object2>
</o:Dependency>
</c:Dependencies>
<c:Actors>
<o:Actor Id="o107">
<a:ObjectID>821114CC-F67E-4E31-BF85-D4AB6FF1B96E</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5DC3DC66-82F9-47D7-9DAF-5197E9DFC96C}
DAT 1576512834</a:History>
</o:Actor>
<o:Actor Id="o113">
<a:ObjectID>3D2562BD-BDBB-4962-BBF5-6E116687CDA9</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F543F67C-015E-4FA6-8E3C-0EE1D7846EC4}
DAT 1576512834</a:History>
</o:Actor>
</c:Actors>
<c:UseCases>
<o:UseCase Id="o104">
<a:ObjectID>2B08F9C6-C343-4DF0-8EF6-E288F8B0A5FE</a:ObjectID>
<a:Name>Acheter billet</a:Name>
<a:Code>Acheter_billet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {58DC3C76-609A-4EE2-B3BD-B400CCD4ECCF}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o105">
<a:ObjectID>38D56A57-233F-4B47-BFF1-0C6041BE1086</a:ObjectID>
<a:Name>Entrer code promo</a:Name>
<a:Code>Entrer_code_promo</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E7EB89CC-C44D-4348-8BD5-0DE6825979CC}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o106">
<a:ObjectID>17340F93-14CB-461D-BD70-F92DD89520CB</a:ObjectID>
<a:Name>Entrer numero licence</a:Name>
<a:Code>Entrer_numero_licence</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {00B3F42A-1E9E-4C5D-8705-F09FC11F26D0}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o108">
<a:ObjectID>CADB2326-2E92-461C-B039-99CE1D6A6E45</a:ObjectID>
<a:Name>Payer</a:Name>
<a:Code>Payer</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F81EFE1A-F6ED-4138-8112-9BC073C53DC9}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o109">
<a:ObjectID>DDBA0ECD-795E-40C4-B838-4F9876F55D3D</a:ObjectID>
<a:Name>Choisir categorie</a:Name>
<a:Code>Choisir_categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9F72E104-7AB8-40C3-A567-895831B62734}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o110">
<a:ObjectID>0D4429CC-1E37-4FA9-A3CC-2D50638A9633</a:ObjectID>
<a:Name>Definir nombre billet par categorie</a:Name>
<a:Code>Definir_nombre_billet_par_categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2634929D-9012-4021-B8FF-2E1926B7D3A9}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o111">
<a:ObjectID>B92FBB3E-6EFD-4340-9338-5080D5DB3437</a:ObjectID>
<a:Name>Definir prix billet par categorie</a:Name>
<a:Code>Definir_prix_billet_par_categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5F14806E-F601-49B7-85B7-BB750EB8553F}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o112">
<a:ObjectID>AF9BA303-BDDD-4550-BF8F-BA32F1E210C0</a:ObjectID>
<a:Name>S&#39;authentifier Staff</a:Name>
<a:Code>S_authentifier_Staff</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {104972A4-0F37-4814-89E2-E4BB9A21DCAC}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o114">
<a:ObjectID>BB027596-7840-470F-9663-C10D760E4602</a:ObjectID>
<a:Name>Choisir place</a:Name>
<a:Code>Choisir_place</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E0FED134-3711-47D6-8111-CF8347DF7E67}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o115">
<a:ObjectID>5EC5AF8B-7C2E-4FC0-9A08-993D9AF5C93A</a:ObjectID>
<a:Name>S&#39;authentifier Client</a:Name>
<a:Code>S_authentifier_Client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {69B258EE-EE01-4B3C-BF86-1758DE9833FF}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o116">
<a:ObjectID>F4329B4D-D02F-4289-BED6-1FE07B0ADD3A</a:ObjectID>
<a:Name>Definir nombre billet par type</a:Name>
<a:Code>Definir_nombre_billet_par_type</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6262727E-F3D2-4D89-B8D8-35F16CDB30B5}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o117">
<a:ObjectID>195A3D07-9BDC-4EA0-A407-F31BCE9F3A7C</a:ObjectID>
<a:Name>Definir reductions</a:Name>
<a:Code>Definir_reductions</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0CD6CD2F-88DA-442F-9026-01D66EDF59A7}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o118">
<a:ObjectID>46D3CDC3-B3F3-4F54-89B7-785FB7A5F70A</a:ObjectID>
<a:Name>Choisir journée</a:Name>
<a:Code>Choisir_journee</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0BF2D07C-B751-4D79-80CE-294D70A51DB2}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o119">
<a:ObjectID>FBA12DC0-FB33-4E85-9EFC-CE758630E56E</a:ObjectID>
<a:Name>Choisir type de billlet</a:Name>
<a:Code>Choisir_type_de_billlet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CD22E091-566A-4E65-BAD9-2E1D44ACEAF5}
DAT 1576512834</a:History>
</o:UseCase>
</c:UseCases>
<c:Package.Objects>
<o:UMLObject Id="o194">
<a:ObjectID>0421A589-4565-45CF-AE9D-C4BEA3EA9C4F</a:ObjectID>
<a:Name>Application</a:Name>
<a:Code>Application</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {7DE9DCE3-3D0F-4638-8D8B-663586B2D335}
DAT 1576512834</a:History>
</o:UMLObject>
<o:UMLObject Id="o197">
<a:ObjectID>D5DEC085-F267-4A8D-8C89-2A3E34E008D8</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {822B75C1-7F23-4F64-922B-EB2A866FA07F}
DAT 1576512834</a:History>
<c:InstantiationClass>
<o:Class Ref="o39"/>
</c:InstantiationClass>
</o:UMLObject>
<o:UMLObject Id="o200">
<a:ObjectID>D3BE50E3-D046-44E1-9F7D-7B052127FB73</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9439A23E-7F24-413D-8F20-0E76B8229173}
DAT 1576512834</a:History>
<c:InstantiationClass>
<o:Class Ref="o40"/>
</c:InstantiationClass>
</o:UMLObject>
<o:UMLObject Id="o203">
<a:ObjectID>357E7F64-552C-4368-BFCB-43572A00848C</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>Planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {4E7340C7-04FB-4AAA-91D7-965F8B325006}
DAT 1576512834</a:History>
</o:UMLObject>
<o:UMLObject Id="o206">
<a:ObjectID>C038C3D9-C4A8-411D-A3C7-664DB69BD852</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {32409693-48AE-4EBB-80FB-927A8403AB93}
DAT 1576512834</a:History>
<c:InstantiationClass>
<o:Class Ref="o48"/>
</c:InstantiationClass>
</o:UMLObject>
</c:Package.Objects>
<c:Messages>
<o:Message Id="o125">
<a:ObjectID>6BA3A46B-F377-41BA-8F35-CBE104895145</a:ObjectID>
<a:Name>acheterBillet</a:Name>
<a:Code>acheterBillet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F06C3E6E-97B1-40F4-A4EC-A8EC1B5A4295}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o128">
<a:ObjectID>0B0DAD1F-C5DD-44A9-9F26-E41A2F664B17</a:ObjectID>
<a:Name>getListJournee()</a:Name>
<a:Code>getListJournee__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {60E5FF26-6675-4D5B-BD92-60A20F5FD9FD}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o203"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o130">
<a:ObjectID>211B08F4-D6E3-4E27-A7CD-03C21D4BDF7C</a:ObjectID>
<a:Name>listJournee</a:Name>
<a:Code>listJournee</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {038A8FDE-A4E4-420F-AABF-EABEE7A739AB}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o203"/>
</c:Object2>
</o:Message>
<o:Message Id="o132">
<a:ObjectID>EFBB1D37-4040-4A12-B51E-3B65A2E713B8</a:ObjectID>
<a:Name>Afficher journees</a:Name>
<a:Code>Afficher_journees</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {33C07A34-F460-4FD8-B3AD-02F2E9D91E7F}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o135">
<a:ObjectID>A20BBF63-DBAA-48F0-8708-818ACDDB0757</a:ObjectID>
<a:Name>Choix journee et cours</a:Name>
<a:Code>Choix_journee_et_cours</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {FCADF34B-3294-4C64-9CD9-0B3F8AEC8726}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o138">
<a:ObjectID>4B530961-99B6-482E-8FF0-1175813E75F1</a:ObjectID>
<a:Name>categorieDisponible()</a:Name>
<a:Code>categorieDisponible__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F53D2600-0EC6-4008-AD7B-9320F0BCA824}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o200"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o141">
<a:ObjectID>DFB2CC4F-B8FF-4EDC-BE71-7DC64D05BF93</a:ObjectID>
<a:Name>placeDisponible()</a:Name>
<a:Code>placeDisponible__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6B9E6B47-DBB4-4E0C-8232-EA3BBBC4E418}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o206"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o200"/>
</c:Object2>
</o:Message>
<o:Message Id="o143">
<a:ObjectID>BDEF092D-63B8-4DF4-BA92-4840B5E7B647</a:ObjectID>
<a:Name>listPlace</a:Name>
<a:Code>listPlace</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AB752281-D21A-4E0F-9CB4-121C7534E93C}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o200"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o206"/>
</c:Object2>
</o:Message>
<o:Message Id="o145">
<a:ObjectID>12787858-964A-4E15-B50F-C7C5460DC78C</a:ObjectID>
<a:Name>listCategorie</a:Name>
<a:Code>listCategorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {B4280BFB-E462-430C-9343-111A8B82EFCC}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o200"/>
</c:Object2>
</o:Message>
<o:Message Id="o148">
<a:ObjectID>38830856-1E0D-49AA-BF23-41FB6743F2F7</a:ObjectID>
<a:Name>getNombrePlacesRestantes(court)</a:Name>
<a:Code>getNombrePlacesRestantes_court_</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {68C520A2-908D-4FC3-BFE8-8BE6E67CE70A}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o206"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o150">
<a:ObjectID>9580BFC5-ADB3-4ED3-B615-FEB5BF9D8D78</a:ObjectID>
<a:Name>nombrePlaces</a:Name>
<a:Code>nombrePlaces</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {55E5FB79-0908-46C9-BB12-A74A6337495F}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o206"/>
</c:Object2>
</o:Message>
<o:Message Id="o152">
<a:ObjectID>58F28049-F2CE-4357-9E5C-495C292232DF</a:ObjectID>
<a:Name>choixEmplacement)</a:Name>
<a:Code>choixEmplacement_</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9879A3B1-5EAA-4D2A-9071-2D92366294A9}
DAT 1576512834</a:History>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o155">
<a:ObjectID>E4AB0E2F-776F-4061-A288-C11E468C4AAB</a:ObjectID>
<a:Name>testDisponibilité()</a:Name>
<a:Code>testDisponibilite__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {830D4C9C-8272-4B20-8AB8-9AA9FCA84F54}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o206"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o157">
<a:ObjectID>A4B533C3-3D16-41AB-AC23-B8205B9A5CAA</a:ObjectID>
<a:Name>estDisponible</a:Name>
<a:Code>estDisponible</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {93FDEBC9-073E-423D-A0E2-2E7E5F942B2D}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o206"/>
</c:Object2>
</o:Message>
<o:Message Id="o159">
<a:ObjectID>875D6F26-943E-4765-A3C0-E022E3B94500</a:ObjectID>
<a:Name>choixNombrePlaces</a:Name>
<a:Code>choixNombrePlaces</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {235F9593-72F2-4340-A382-0F7A4F789436}
DAT 1576512834</a:History>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o162">
<a:ObjectID>49E90D7F-5DCD-4A3A-A781-A564563F4636</a:ObjectID>
<a:Name>getNombrePlacesRestantes(court)</a:Name>
<a:Code>getNombrePlacesRestantes_court_</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {B3CA2FEF-94A4-4714-BE50-98E9EAE70507}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o206"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o164">
<a:ObjectID>9367649C-CAB2-48FE-8609-67F41716BDD8</a:ObjectID>
<a:Name>nombrePlaces</a:Name>
<a:Code>nombrePlaces</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {3094EE6A-82FB-4E5D-B993-62C432F72627}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o206"/>
</c:Object2>
</o:Message>
<o:Message Id="o166">
<a:ObjectID>633EAF58-E270-47B5-B5F6-349E10375782</a:ObjectID>
<a:Name>ok</a:Name>
<a:Code>ok</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1BDF6A64-2132-45C5-AF97-15BCAA1A2C33}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o169">
<a:ObjectID>59EB2E94-A556-4B93-88EF-C94AB6B30959</a:ObjectID>
<a:Name>procéderAchat()</a:Name>
<a:Code>procederAchat__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AE33744E-3AE4-49E7-9344-28DE437F08E8}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o172">
<a:ObjectID>E9D677FE-103E-4CEE-AA86-1427CA69C59F</a:ObjectID>
<a:Name>calculerPrix()</a:Name>
<a:Code>calculerPrix__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {28CCFBC2-DBFF-45C7-BCF9-3E6EFE8DB902}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o197"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o174">
<a:ObjectID>E4B74C43-6F6E-4A27-82D1-76C29CC8D5DF</a:ObjectID>
<a:Name>prix</a:Name>
<a:Code>prix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {33BB481D-1F5F-41E8-A5A5-60279F0F926D}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o197"/>
</c:Object2>
</o:Message>
<o:Message Id="o176">
<a:ObjectID>E0379718-4B41-4398-8E27-CF8B036959DD</a:ObjectID>
<a:Name>prixTotal</a:Name>
<a:Code>prixTotal</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {DA3CFBA6-0F91-4454-A835-202D3FD03DB6}
DAT 1576512834</a:History>
<c:Object1>
<o:Actor Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o178">
<a:ObjectID>AC14F6FE-2790-4FCD-A93E-56890BE46CDF</a:ObjectID>
<a:Name>paiement</a:Name>
<a:Code>paiement</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {FAD949A7-A934-451F-B970-074F28C4987B}
DAT 1576512834</a:History>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:Message>
<o:Message Id="o181">
<a:ObjectID>FA2490AC-8B8B-4757-8493-8227F216B16D</a:ObjectID>
<a:Name>validationPaiement()</a:Name>
<a:Code>validationPaiement__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6E70A044-1713-47D1-B14B-097BC6D595FA}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<a:Delay>1</a:Delay>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o183">
<a:ObjectID>40D871E9-3D46-48CC-BBC0-55B13FE6DCE4</a:ObjectID>
<a:Name>paiementValidé</a:Name>
<a:Code>paiementValide</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6A7E974E-F167-4B3F-956F-8920944FCB31}
DAT 1576512834</a:History>
<c:Object1>
<o:Actor Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o186">
<a:ObjectID>F25D20B7-1FC2-443D-AFEB-1A7A30846DC6</a:ObjectID>
<a:Name>exporterPDF()</a:Name>
<a:Code>exporterPDF__</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A9D41935-EFD4-486E-B1DF-AF5501C014C3}
DAT 1576512834</a:History>
<a:ControlFlow>C</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o197"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
<o:Message Id="o188">
<a:ObjectID>C0FFF215-23FD-4824-A3B9-C61C865C81AC</a:ObjectID>
<a:Name>PDF</a:Name>
<a:Code>PDF</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BC295B94-D929-4E71-A713-DD566C8A493B}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:UMLObject Ref="o194"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o197"/>
</c:Object2>
</o:Message>
<o:Message Id="o190">
<a:ObjectID>7CDE7AD2-0F34-41D4-8D93-5F0F798CFFD6</a:ObjectID>
<a:Name>PDF</a:Name>
<a:Code>PDF</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1DB6C81C-50EE-4E9C-AA6A-5D958B38DC56}
DAT 1576512834</a:History>
<a:ControlFlow>R</a:ControlFlow>
<c:Object1>
<o:Actor Ref="o107"/>
</c:Object1>
<c:Object2>
<o:UMLObject Ref="o194"/>
</c:Object2>
</o:Message>
</c:Messages>
<c:UseCaseAssociations>
<o:UseCaseAssociation Id="o70">
<a:ObjectID>9232924A-0F7A-4B21-8ED7-E04BDED3748F</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {FA7E00BC-152D-4E6B-8E30-5390907E2C03}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o110"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o113"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o72">
<a:ObjectID>341ECBFA-DA13-4A0A-8C50-84892C76FD16</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A8E9E958-6B45-421E-850D-84BE6E40CA83}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o111"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o113"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o82">
<a:ObjectID>B22E08EB-66F3-4EB3-90EA-B423D9E3E24B</a:ObjectID>
<a:Name>Association_8</a:Name>
<a:Code>Association_8</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {76C14B4F-A79A-4F5F-89DB-63210827F1BB}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o116"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o113"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o87">
<a:ObjectID>7172A98C-AC8D-4130-BCB9-C1BCCBDD7956</a:ObjectID>
<a:Name>Association_9</a:Name>
<a:Code>Association_9</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {733C54E1-B695-4E9E-9A2E-B33B6711633A}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o117"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o113"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o94">
<a:ObjectID>0D6B1187-422E-430C-A839-7E7C5E790740</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {36DB298C-0062-44DB-BCAA-F49724CD39D3}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o104"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o107"/>
</c:Object2>
</o:UseCaseAssociation>
</c:UseCaseAssociations>
<c:Flows>
<o:ActivityFlow Id="o252">
<a:ObjectID>5DD4476D-BEB6-4DB6-94F1-68CE4E295C09</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o253"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o254"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o255">
<a:ObjectID>0D51514E-A72A-43C2-8A86-29B3C02955C7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o256"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o253"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o257">
<a:ObjectID>3650403B-DD71-49D4-9AFC-DD0134477E9A</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o258"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o259"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o260">
<a:ObjectID>6542C2ED-513E-4B7E-94AC-942E620B4DC7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o261"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o258"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o262">
<a:ObjectID>DD9C9ECB-D7E7-40FA-94B7-485FBCF51306</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>oui</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o256"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o263"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o264">
<a:ObjectID>648AC046-76D2-479F-AEFE-3B6FEA7EEB9B</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>non</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o265"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o263"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o266">
<a:ObjectID>843BBAAC-DA2F-44DF-AF5D-F98DA130FE7E</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o267"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o265"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o268">
<a:ObjectID>76FCBF2D-6BDE-46C6-81F2-A2682DE0C77D</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>oui</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o259"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o269"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o270">
<a:ObjectID>3BDF7C16-7F9F-4079-AA81-A880E93F88E7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Decision Ref="o263"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o261"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o271">
<a:ObjectID>A4726EB2-80C4-49A3-9C66-766CF726625E</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o272"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o267"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o273">
<a:ObjectID>84C626AE-8DA9-4D2D-8E07-93C17398FA76</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o274"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o272"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o275">
<a:ObjectID>664E0FFE-B56E-405C-A64A-0175490B2F7C</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Decision Ref="o269"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o276"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o277">
<a:ObjectID>D9BED6C9-1982-4540-B1B8-24543806D02D</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o278"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o279"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o280">
<a:ObjectID>535B2F2B-AF96-4641-935A-E896983231C2</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o281"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o282"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o283">
<a:ObjectID>538EA4C3-3CCA-4228-95A2-8E39C07FE6B0</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o284"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o281"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o285">
<a:ObjectID>3FEE5B0E-4BD4-4D5D-895A-406013AC156D</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o286"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o284"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o287">
<a:ObjectID>372D57FB-7BE4-497D-9CF2-7AD962A0DCBE</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o288"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o278"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o289">
<a:ObjectID>9A6F700F-3140-46D9-AA50-153DDBCF2054</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o282"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o288"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o290">
<a:ObjectID>BDC23172-F9D0-4366-B343-2C5E5D43CDA7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o291"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o292"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o293">
<a:ObjectID>FB00716B-3CB7-45A0-831C-32244FC2651E</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o294"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o291"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o295">
<a:ObjectID>BFA58106-2505-4404-B4A8-9AABA5793601</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o296"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o294"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o297">
<a:ObjectID>312672B5-940F-4A58-994E-C733EDCE8865</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o284"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o296"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o298">
<a:ObjectID>6C71343C-F85A-4EF0-8AD5-B0B95F698D83</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o299"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o284"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o300">
<a:ObjectID>DBF0EF86-DB12-4174-B313-8DA62F2FA110</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o301"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o302"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o303">
<a:ObjectID>FA3CC0AD-77CA-462F-99C7-3DC571670371</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o304"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o301"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o305">
<a:ObjectID>7A3DF5A2-2504-418E-9D7D-0E6B4AA841A4</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o306"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o304"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o307">
<a:ObjectID>79FF7EA4-5FC1-4F90-AD5B-72A3B456DC96</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o284"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o306"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o308">
<a:ObjectID>C7781347-6017-49D1-ACF8-E93ED609BC89</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o309"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o284"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o310">
<a:ObjectID>DEE63270-B4AC-44CC-809E-D8063B8195D7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o276"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o256"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o311">
<a:ObjectID>AF08F199-A453-4D84-8EAE-09D7E9AECDAD</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:ConditionAlias>non</a:ConditionAlias>
<c:Object1>
<o:Activity Ref="o312"/>
</c:Object1>
<c:Object2>
<o:Decision Ref="o269"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o313">
<a:ObjectID>A2037DE0-019A-4EAC-A973-74167CF21B79</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o258"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o312"/>
</c:Object2>
</o:ActivityFlow>
</c:Flows>
<c:Activities>
<o:Activity Id="o256">
<a:ObjectID>CA002986-671C-41C7-937D-C6E446326A35</a:ObjectID>
<a:Name>Choisir journée</a:Name>
<a:Code>Choisir_journee</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {88D5C940-0EC0-4AF2-9E25-4AD30B29E2CA}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o276">
<a:ObjectID>20384FA9-97E0-417E-83FE-7A4D841E53F6</a:ObjectID>
<a:Name>Choisir option Court Central</a:Name>
<a:Code>Choisir_option_Court_Central</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {86582E4D-526B-4998-B2B4-306C2A26E719}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o259">
<a:ObjectID>498D5C0B-CDB0-4B90-90FB-DC254A226908</a:ObjectID>
<a:Name>Choisir emplacement</a:Name>
<a:Code>Choisir_emplacement</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {4E24D9ED-15B9-47AD-93BD-8351E28E09C0}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o253">
<a:ObjectID>88351819-69F7-4F89-B7A3-03B5D2880D72</a:ObjectID>
<a:Name>Verfifier disponibilité</a:Name>
<a:Code>Verfifier_disponibilite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AE2D94B8-DA5F-4DE0-9934-55EF9C440D51}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o315"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o258">
<a:ObjectID>9FE26AFD-5DED-4A70-BDC0-178618D0E16A</a:ObjectID>
<a:Name>Verifier choix du client</a:Name>
<a:Code>Verifier_choix_du_client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8A1B9FD8-2CB8-4BC5-9538-DE6445F507AF}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o261">
<a:ObjectID>C9AE236E-E6E4-4DDA-A30A-0EF47F2A08AF</a:ObjectID>
<a:Name>Calculer prix</a:Name>
<a:Code>Calculer_prix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C7E35BAB-1245-477A-A0BA-81A064B3CBA9}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o265">
<a:ObjectID>23D7F9FD-F0B0-46A5-AA90-8EF06AC77D4A</a:ObjectID>
<a:Name>Comfirmer commande</a:Name>
<a:Code>Comfirmer_commande</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {24619233-6D04-4F32-94B5-351C8C057037}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o267">
<a:ObjectID>57FF38FE-6630-4824-AFF6-E6912FFC890B</a:ObjectID>
<a:Name>Payer commande</a:Name>
<a:Code>Payer_commande</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {87469F1C-2C43-41AF-A366-EEED8850C4A8}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o272">
<a:ObjectID>F1CC15AC-82CE-4689-A1D8-B6AAAE97FB9D</a:ObjectID>
<a:Name>Envoyer billet par mail</a:Name>
<a:Code>Envoyer_billet_par_mail</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BBAF3CE4-DD27-4637-92D2-FA00FBB7E883}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o278">
<a:ObjectID>C416A607-34B4-42EF-A20C-1E155AB02251</a:ObjectID>
<a:Name>Choisir categorie</a:Name>
<a:Code>Choisir_categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F7366256-5DF1-4603-A76A-40A4258C05C9}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o316"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o282">
<a:ObjectID>63ED96DC-9742-41C5-AA81-84446C10FDA5</a:ObjectID>
<a:Name>Definir nb places</a:Name>
<a:Code>Definir_nb_places</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {878B6999-1E2F-4F1D-9AA9-A2F533D8AADC}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o316"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o284">
<a:ObjectID>15FEE0D3-CC42-4722-B4DD-B604608A93A7</a:ObjectID>
<a:Name>Confirmer</a:Name>
<a:Code>Confirmer</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {B96524E0-6CBA-4C7C-9CA7-5BE3014426F6}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o316"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o281">
<a:ObjectID>77A6E90F-A8B1-407E-9A94-D1B598E96432</a:ObjectID>
<a:Name>Verifier coherence</a:Name>
<a:Code>Verifier_coherence</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {25BBF6AA-AEB3-4D4C-A2E5-6CC284628DD4}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o317"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o288">
<a:ObjectID>A8B721A5-FAFF-48ED-A436-AAA1ED0D9520</a:ObjectID>
<a:Name>Selectioner billets concernés</a:Name>
<a:Code>Selectioner_billets_concernes</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D3D13AA4-E935-4DBE-A209-8B18E382540B}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o316"/>
</c:OrganizationUnit>
</o:Activity>
<o:Activity Id="o291">
<a:ObjectID>3EFBC8A1-A8BB-4443-9C56-C7439227C442</a:ObjectID>
<a:Name>Choisir type de billet</a:Name>
<a:Code>Choisir_type_de_billet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1CAFB3D4-05A5-4F1C-A27B-20C160A31866}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o294">
<a:ObjectID>C88B7577-3564-4D32-B19F-BA3D8C160102</a:ObjectID>
<a:Name>Definir nb billets</a:Name>
<a:Code>Definir_nb_billets</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6686056F-B514-47CC-B591-24E08FB2F371}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o296">
<a:ObjectID>FC431999-FAF7-4D69-A6E5-6B808F28B0F6</a:ObjectID>
<a:Name>Verifier coherence des données</a:Name>
<a:Code>Verifier_coherence_des_donnees</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1989E0BB-95B8-463D-96DB-FD3BE27E6E6A}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o301">
<a:ObjectID>5CC984C2-C4C1-44FE-8082-87493E795C93</a:ObjectID>
<a:Name>Choisir billets GrandPublic ou Big Match</a:Name>
<a:Code>Choisir_billets_GrandPublic_ou_Big_Match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {61AEDE7C-A542-4B28-AAC3-5800B04C953D}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o304">
<a:ObjectID>4D94F151-0324-470F-98D5-4718D527434A</a:ObjectID>
<a:Name>Definir prix</a:Name>
<a:Code>Definir_prix</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {858358E6-3B2C-4BDB-A0B0-78A9774512AA}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o306">
<a:ObjectID>25AF8C36-B620-44A6-9E40-0950256F2775</a:ObjectID>
<a:Name>Verifier compatibilité des données</a:Name>
<a:Code>Verifier_compatibilite_des_donnees</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {83BA0C91-0C66-4047-B20B-B51E16C949A4}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o312">
<a:ObjectID>4326D83F-2968-4CE8-8D4D-DCBB1631390C</a:ObjectID>
<a:Name>Choisir nombre de personne</a:Name>
<a:Code>Choisir_nombre_de_personne</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1869D27C-354C-4D5F-BCAD-87D0EC70B13D}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
<c:OrganizationUnit>
<o:OrganizationUnit Ref="o314"/>
</c:OrganizationUnit>
</o:Activity>
</c:Activities>
<c:Decisions>
<o:Decision Id="o263">
<a:ObjectID>430E4982-ACD3-499A-B263-EA6D7DF799BC</a:ObjectID>
<a:Name>Ajouter billet</a:Name>
<a:Code>Ajouter_billet</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D7B85C17-A53A-4DEE-ABAF-963BF3AFAC91}
DAT 1576512834</a:History>
</o:Decision>
<o:Decision Id="o269">
<a:ObjectID>AABF4689-E083-40FA-886D-C3551B5E038B</a:ObjectID>
<a:Name>Court Central sélectionné</a:Name>
<a:Code>Court_Central_selectionne</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E99103C8-FD94-4BE4-8551-D405E71F56F9}
DAT 1576512834</a:History>
</o:Decision>
</c:Decisions>
<c:ActivityDiagrams>
<o:ActivityDiagram Id="o318">
<a:ObjectID>B84ED227-A4B3-438D-8CD6-FB913402F7A0</a:ObjectID>
<a:Name>4. Activité Achat de Billet - Billetterie</a:Name>
<a:Code>4__Activite_Achat_de_Billet___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {43370109-4910-4570-B101-E8219BF8FD65}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:FlowSymbol Id="o319">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((16178,-20079), (25169,-13809))</a:Rect>
<a:ListOfPoints>((16178,-13809),(25169,-13809),(25169,-20079))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o320"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o321"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o313"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o322">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((645,7183), (25050,11322))</a:Rect>
<a:ListOfPoints>((645,11322),(645,7183),(25050,7183))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o323"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o324"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o252"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o325">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-224,1483), (25200,6358))</a:Rect>
<a:ListOfPoints>((25200,6358),(25200,1483),(-224,1483))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o324"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o326"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o255"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o327">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((699,-20749), (23783,-17679))</a:Rect>
<a:ListOfPoints>((699,-17679),(699,-20749),(23783,-20749))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o328"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o321"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o257"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o329">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((23783,-25256), (24233,-21306))</a:Rect>
<a:ListOfPoints>((24008,-21306),(24008,-25256))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o321"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o330"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o260"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o331">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((309,-27996), (23581,-25277))</a:Rect>
<a:ListOfPoints>((23581,-25277),(309,-25277),(309,-27996))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o330"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseDecisionSymbol Ref="o332"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o270"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o333">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((482,-44549), (23829,-41414))</a:Rect>
<a:ListOfPoints>((482,-41414),(482,-44549),(23829,-44549))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o334"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o335"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o271"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o336">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((971,-51211), (24654,-44952))</a:Rect>
<a:ListOfPoints>((24654,-44952),(24654,-51211),(971,-51211))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o335"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o337"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o273"/>
</c:Object>
</o:FlowSymbol>
<o:SwimlaneGroupSymbol Id="o338">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((16701,-53912), (16701,15667))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o339">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((16701,-53912), (32775,15667))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:ActivitySymbol Id="o324">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((20693,6258), (28809,8257))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o253"/>
</c:Object>
</o:ActivitySymbol>
<o:NoteSymbol Id="o340">
<a:Text>unitaire et total</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((24387,-28892), (29186,-25293))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:NoteSymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o315"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
<o:SwimlaneGroupSymbol Id="o341">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13449,-53912), (-13449,15667))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o342">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13449,-53912), (19165,15667))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:FlowSymbol Id="o343">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((8098,-13974), (15105,-9540))</a:Rect>
<a:ListOfPoints>((8098,-10714),(15105,-10714),(15105,-13974))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o344"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o320"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o311"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o345">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((1051,-5025), (1501,1050))</a:Rect>
<a:ListOfPoints>((1276,1050),(1276,-5025))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o326"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o346"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o310"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o347">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((647,-8734), (1097,-4772))</a:Rect>
<a:ListOfPoints>((872,-4772),(872,-8734))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o346"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseDecisionSymbol Ref="o344"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o275"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o348">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((773,-17243), (3210,-12694))</a:Rect>
<a:ListOfPoints>((998,-12694),(998,-17243))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o344"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o328"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o268"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o349">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((384,-41663), (834,-37463))</a:Rect>
<a:ListOfPoints>((609,-37463),(609,-41663))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o350"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o334"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o266"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o351">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((121,-36713), (2783,-31956))</a:Rect>
<a:ListOfPoints>((346,-31956),(346,-36713))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o332"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o350"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o264"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o352">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-10874,-31150), (-749,1766))</a:Rect>
<a:ListOfPoints>((-3532,-29976),(-10874,-29976),(-10874,1766),(-749,1766))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseDecisionSymbol Ref="o332"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o326"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o262"/>
</c:Object>
</o:FlowSymbol>
<o:ActivitySymbol Id="o326">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2295,688), (3796,2687))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o256"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o346">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-4159,-5592), (6357,-3593))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o276"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o328">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-4033,-18228), (4233,-16229))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o259"/>
</c:Object>
</o:ActivitySymbol>
<o:BaseDecisionSymbol Id="o332">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3571,-31976), (4189,-27977))</a:Rect>
<a:LineColor>32896</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DEXP 0 Arial,8,N
DEXN 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Decision Ref="o263"/>
</c:Object>
</o:BaseDecisionSymbol>
<o:ActivitySymbol Id="o350">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3787,-37802), (4854,-35803))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o265"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o334">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2887,-42140), (4104,-40141))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o267"/>
</c:Object>
</o:ActivitySymbol>
<o:StartSymbol Id="o323">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((46,10723), (1245,11922))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o254"/>
</c:Object>
</o:StartSymbol>
<o:EndSymbol Id="o337">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((222,-51961), (1721,-50462))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o274"/>
</c:Object>
</o:EndSymbol>
<o:BaseDecisionSymbol Id="o344">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-6115,-12714), (8169,-8715))</a:Rect>
<a:LineColor>32896</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DEXP 0 Arial,8,N
DEXN 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Decision Ref="o269"/>
</c:Object>
</o:BaseDecisionSymbol>
<o:NoteSymbol Id="o353">
<a:Text>Pour chaque personne
(categorie + place)</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-9734,-20780), (-438,-18392))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:NoteSymbol>
<o:ActivitySymbol Id="o320">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((7748,-15053), (18414,-13054))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o312"/>
</c:Object>
</o:ActivitySymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o314"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
<o:ActivitySymbol Id="o321">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((19425,-21432), (28142,-19433))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o258"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o330">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((20783,-26012), (26782,-24013))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o261"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o335">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((19763,-45300), (28554,-43301))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o272"/>
</c:Object>
</o:ActivitySymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o354">
<a:ObjectID>A71A309E-042E-4796-A8E4-176456FB186C</a:ObjectID>
<a:Name>5. Activité Definir nombre de billets/categories - Billetterie</a:Name>
<a:Code>5__Activite_Definir_nombre_de_billets_categories___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {B2C4B325-A39B-498A-93B6-CF391DDA7A15}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:FlowSymbol Id="o355">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-7689,-750), (4650,3300))</a:Rect>
<a:ListOfPoints>((-7689,3300),(-7689,-750),(4650,-750))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o356"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o357"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o280"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o358">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-6302,-6900), (3825,-1125))</a:Rect>
<a:ListOfPoints>((3825,-1125),(3825,-6900),(-6302,-6900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o357"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o359"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o283"/>
</c:Object>
</o:FlowSymbol>
<o:SwimlaneGroupSymbol Id="o360">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-3700,-17576), (-3700,22424))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o361">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-3700,-17576), (10350,22424))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:ActivitySymbol Id="o357">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((143,-1825), (7359,174))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o281"/>
</c:Object>
</o:ActivitySymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o317"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
<o:SwimlaneGroupSymbol Id="o362">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-17349,-17576), (-17349,22424))</a:Rect>
<a:LineColor>0</a:LineColor>
<c:SubSymbols>
<o:SwimlaneSymbol Id="o363">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17349,-17576), (-1652,22424))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:SubSymbols>
<o:FlowSymbol Id="o364">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8274,3300), (-7824,9551))</a:Rect>
<a:ListOfPoints>((-8049,9551),(-8049,3300))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o365"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o356"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o289"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o366">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8312,9096), (-7862,15150))</a:Rect>
<a:ListOfPoints>((-8087,15150),(-8087,9096))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o367"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o365"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o287"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o368">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-7839,-15525), (-7389,-6975))</a:Rect>
<a:ListOfPoints>((-7614,-6975),(-7614,-15525))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o359"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o369"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o285"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o370">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8552,15000), (-8102,19199))</a:Rect>
<a:ListOfPoints>((-8327,19199),(-8327,15000))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o371"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o367"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o277"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o371">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8927,18600), (-7728,19799))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o279"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o367">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-11899,14525), (-5209,16524))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o278"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o356">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-11448,2675), (-4757,4674))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o282"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o359">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10427,-7675), (-4428,-5676))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o284"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o369">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8364,-16275), (-6865,-14776))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o286"/>
</c:Object>
</o:EndSymbol>
<o:ActivitySymbol Id="o365">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-13736,9050), (-2846,11049))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o288"/>
</c:Object>
</o:ActivitySymbol>
<o:NoteSymbol Id="o372">
<a:Text>Grand Public ou Big Match</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-16599,6053), (-9485,8849))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:NoteSymbol>
</c:SubSymbols>
<c:Object>
<o:OrganizationUnit Ref="o316"/>
</c:Object>
</o:SwimlaneSymbol>
</c:SubSymbols>
</o:SwimlaneGroupSymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o373">
<a:ObjectID>E668938B-FD63-4700-9003-E749834256F1</a:ObjectID>
<a:Name>6. Activité Definir nombre billets/types - Billetterie</a:Name>
<a:Code>6__Activite_Definir_nombre_billets_types___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {EFB0ED0D-4FD1-453E-8E7B-76F367E0CFFB}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o374">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-22423,14685), (-5758,-23595))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o375">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-22588,14850), (-5758,11138))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o376">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-4603,14933), (12392,-24585))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o377">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-4686,15015), (12475,11138))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:TextSymbol Id="o378">
<a:Text>Client</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-16449,14832), (-11649,11234))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:TextSymbol Id="o379">
<a:Text>Systeme</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((1617,14626), (6417,11028))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:FlowSymbol Id="o380">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-16054,1815), (-15054,8332))</a:Rect>
<a:ListOfPoints>((-15575,8332),(-15534,1815))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o381"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o382"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o290"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o383">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-15581,-4538), (-14581,1980))</a:Rect>
<a:ListOfPoints>((-15081,1980),(-15081,-4538))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o382"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o384"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o293"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o385">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-14586,-11138), (1997,-5280))</a:Rect>
<a:ListOfPoints>((-14586,-5280),(-14586,-11138),(1997,-11138))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o384"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o386"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o295"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o387">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-14008,-16088), (3565,-10973))</a:Rect>
<a:ListOfPoints>((3565,-10973),(3565,-16088),(-14008,-16088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o386"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o388"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o297"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o389">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-15127,-22028), (-14127,-16583))</a:Rect>
<a:ListOfPoints>((-14668,-16583),(-14586,-22028))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o388"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o390"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o298"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o381">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16175,7733), (-14976,8932))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o292"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o382">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-19349,1228), (-11308,3227))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o291"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o384">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-18434,-5949), (-11893,-3950))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o294"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o386">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-1790,-11559), (10076,-9560))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o296"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o388">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17832,-17005), (-11833,-15006))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o284"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o390">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-15336,-22778), (-13837,-21279))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o299"/>
</c:Object>
</o:EndSymbol>
</c:Symbols>
</o:ActivityDiagram>
<o:ActivityDiagram Id="o391">
<a:ObjectID>F11B1571-0002-4FB2-A7F6-05E1D0884CDB</a:ObjectID>
<a:Name>7. Activité Definir prix des billets - Billetterie</a:Name>
<a:Code>7__Activite_Definir_prix_des_billets___Billetterie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {DC5273E9-8067-48F0-8733-C513F75B0D9C}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o392">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-27344,17305), (-10679,-20975))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o393">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-27509,17470), (-10679,13758))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o394">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-3949,17966), (13046,-21552))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o395">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-4032,18048), (13129,14171))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:TextSymbol Id="o396">
<a:Text>Client</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-22195,17865), (-17395,14267))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:TextSymbol Id="o397">
<a:Text>Systeme</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((2271,17659), (7071,14061))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:TextSymbol>
<o:FlowSymbol Id="o398">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20487,6038), (-19487,12037))</a:Rect>
<a:ListOfPoints>((-20062,12037),(-19912,6038))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o399"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o400"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o300"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o401">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20149,-1762), (-19149,6413))</a:Rect>
<a:ListOfPoints>((-19649,6413),(-19649,-1762))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o400"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o402"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o303"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o403">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-19537,-6862), (788,-1762))</a:Rect>
<a:ListOfPoints>((-19537,-1762),(-19537,-6862),(788,-6862))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o402"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o404"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o305"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o405">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-18562,-11962), (638,-7462))</a:Rect>
<a:ListOfPoints>((638,-7462),(638,-11962),(-18562,-11962))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o404"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o406"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o307"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o407">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-19568,-16912), (-18568,-12112))</a:Rect>
<a:ListOfPoints>((-19049,-12112),(-19087,-16912))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o406"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o408"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o308"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o399">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-20662,11438), (-19463,12637))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o302"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o400">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-26708,5488), (-11621,7487))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o301"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o402">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-22012,-2387), (-16013,-388))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o304"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o404">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2480,-8311), (10358,-6312))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o306"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o406">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-22011,-12811), (-16012,-10812))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o284"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o408">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-19837,-17662), (-18338,-16163))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o309"/>
</c:Object>
</o:EndSymbol>
</c:Symbols>
</o:ActivityDiagram>
</c:ActivityDiagrams>
<c:Starts>
<o:Start Id="o254">
<a:ObjectID>E4F0D27C-4E1F-4470-82D6-6AD5866CAD11</a:ObjectID>
<a:Name>Debut_1</a:Name>
<a:Code>Debut_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CFEE3D33-913B-4DB9-9BE8-DD1B7B639B1C}
DAT 1576512834</a:History>
</o:Start>
<o:Start Id="o279">
<a:ObjectID>091713C8-6A69-4C3E-AC58-85E9CB83D3AD</a:ObjectID>
<a:Name>Debut_2</a:Name>
<a:Code>Debut_2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {60FBC2F6-76FF-4705-BE6E-FD0DD2AD18F8}
DAT 1576512834</a:History>
</o:Start>
<o:Start Id="o292">
<a:ObjectID>4AFDF1E0-8F44-48F2-BBED-0283AA86DBD1</a:ObjectID>
<a:Name>Debut_3</a:Name>
<a:Code>Debut_3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9E0B66E1-C034-4242-905A-39CE9AF7438A}
DAT 1576512834</a:History>
</o:Start>
<o:Start Id="o302">
<a:ObjectID>5EB1B22E-9736-474B-88AF-B3D4FB5496D3</a:ObjectID>
<a:Name>Debut_4</a:Name>
<a:Code>Debut_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0A39D4BE-2C44-42A3-A525-104B323278ED}
DAT 1576512834</a:History>
</o:Start>
</c:Starts>
<c:Ends>
<o:End Id="o274">
<a:ObjectID>C3066C9D-E254-4ED2-A92D-25B8D81D6FB9</a:ObjectID>
<a:Name>Fin_1</a:Name>
<a:Code>Fin_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D5D900C3-6869-437C-9A5E-7C190C471B01}
DAT 1576512834</a:History>
</o:End>
<o:End Id="o286">
<a:ObjectID>F72F3CB3-2C35-4E9D-A7AA-EC04FF3CB3A6</a:ObjectID>
<a:Name>Fin_2</a:Name>
<a:Code>Fin_2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0D7E4D8D-FFF1-4852-8BBC-06859BBE9572}
DAT 1576512834</a:History>
</o:End>
<o:End Id="o299">
<a:ObjectID>F5EB0DED-845E-415B-9A9B-9908A7EAEF25</a:ObjectID>
<a:Name>Fin_3</a:Name>
<a:Code>Fin_3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A21E77F0-3985-4132-9D23-FD817FEE2ECE}
DAT 1576512834</a:History>
</o:End>
<o:End Id="o309">
<a:ObjectID>D3F49574-3BC7-4505-BDC2-56EEB814F61D</a:ObjectID>
<a:Name>Fin_4</a:Name>
<a:Code>Fin_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F831B2C6-0D15-4EFC-8B23-CE97A209BAB4}
DAT 1576512834</a:History>
</o:End>
</c:Ends>
</o:Package>
<o:Package Id="o409">
<a:ObjectID>453B72C0-C679-4EA2-9285-28AA9DC92069</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BFEC5C98-43E9-4C5E-A0E0-A6DF1EC3C338}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:ClassDiagrams>
<o:ClassDiagram Id="o410">
<a:ObjectID>2D55F810-40F1-4F69-A837-16B0A80B9736</a:ObjectID>
<a:Name>2. Classes - Planning</a:Name>
<a:Code>2__Classes___Planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {44D18D7F-B500-49D3-80F5-78636C0AC4EA}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=Yes
Generalization.DisplayName=No
Generalization.DisplayedRules=Yes
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Realization.DisplayedStereotype=Yes
Realization.DisplayName=No
Realization.DisplayedRules=Yes
Realization_SymbolLayout=
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=Yes
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Class.Stereotype=Yes
Class.Constraint=Yes
Class.Attributes=Yes
Class.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Class.Attributes._Limit=-3
Class.Operations=Yes
Class.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Class.Operations._Limit=-3
Class.InnerClassifiers=Yes
Class.Comment=No
Class.IconPicture=No
Class.TextStyle=No
Class_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de classe&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Interface.Stereotype=Yes
Interface.Constraint=Yes
Interface.Attributes=Yes
Interface.Attributes._Columns=DisplayVisibilityMarker Stereotype DataType InitialValue
Interface.Attributes._Limit=-3
Interface.Operations=Yes
Interface.Operations._Columns=DisplayVisibilityMarker Stereotype SignatureWithParameters ReturnType
Interface.Operations._Limit=-3
Interface.InnerClassifiers=Yes
Interface.Comment=No
Interface.IconPicture=No
Interface.TextStyle=No
Interface_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom d&amp;#39;interface&quot; Attribute=&quot;DisplayedQualifiedName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;Constraint&quot; Prefix=&quot;{&quot; Suffix=&quot;}&quot; Alignment=&quot;RGHT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Attributs&quot; Collection=&quot;Attributes&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nDisplayName Yes\r\nDataType No\r\nDomain No\r\nInitialValue No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Opérations&quot; Collection=&quot;Operations&quot; Columns=&quot;DisplayVisibilityMarker No\r\nDisplayVisibilityKeyword No\r\nDisplayVisibilityIcon No\r\nStereotype No\r\nSignatureWithoutParameters No\r\nSignatureWithParameters No\r\nReturnType No&quot; HasLimit=&quot;Yes&quot; HideEmpty=&quot;No&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardCollection Name=&quot;Classificateurs internes&quot; Collection=&quot;InnerClassifiers&quot; Columns=&quot;DisplayInnerIcon Yes\r\nDisplayNameGeneric Yes&quot; HasLimit=&quot;No&quot; HideEmpty=&quot;Yes&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Port.IconPicture=No
Port.TextStyle=No
Port_SymbolLayout=
Association.RoleAMultiplicity=Yes
Association.RoleAName=Yes
Association.RoleAOrdering=Yes
Association.DisplayedStereotype=No
Association.DisplayName=No
Association.DisplayedRules=Yes
Association.RoleBMultiplicity=Yes
Association.RoleBName=Yes
Association.RoleBOrdering=Yes
Association.RoleMultiplicitySymbol=No
Association_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité A&quot; Attribute=&quot;RoleAMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle A&quot; Attribute=&quot;RoleAName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre A&quot; Attribute=&quot;RoleAOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Multiplicité B&quot; Attribute=&quot;RoleBMultiplicity&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Rôle B&quot; Attribute=&quot;RoleBName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Ordre B&quot; Attribute=&quot;RoleBOrdering&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
RequireLink.DisplayedStereotype=Yes
RequireLink.DisplayName=No
RequireLink.DisplayedRules=Yes
RequireLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
PortShowName=Yes
PortShowType=No
PortShowMult=No

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=3 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0 0 0
QDNMFont=Arial,8,N
QDNMFont color=0 0 0
CNTRFont=Arial,8,N
CNTRFont color=0 0 0
AttributesFont=Arial,8,N
AttributesFont color=0 0 0
OperationsFont=Arial,8,N
OperationsFont color=0 0 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=250 241 211
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=2
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=2
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 64 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:GeneralizationSymbol Id="o411">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-25025,6391), (-12149,7391))</a:Rect>
<a:ListOfPoints>((-25025,6900),(-19650,6900),(-19650,6881),(-12149,6881))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o412"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o413"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o414"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o415">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-24599,892), (-10164,1892))</a:Rect>
<a:ListOfPoints>((-24599,1392),(-10164,1392))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o416"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o413"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o417"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o418">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-48178,-14925), (-37578,-6150))</a:Rect>
<a:ListOfPoints>((-48178,-14925),(-37578,-14925),(-37578,-6150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o419"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o420"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o421"/>
</c:Object>
</o:GeneralizationSymbol>
<o:GeneralizationSymbol Id="o422">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-33754,-15319), (-21674,-8900))</a:Rect>
<a:ListOfPoints>((-21674,-15319),(-33754,-15319),(-33754,-8900))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>7</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o423"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o420"/>
</c:DestinationSymbol>
<c:Object>
<o:Generalization Ref="o424"/>
</c:Object>
</o:GeneralizationSymbol>
<o:AssociationSymbol Id="o425">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576513087</a:ModificationDate>
<a:Rect>((-47924,-37002), (-40255,-15176))</a:Rect>
<a:ListOfPoints>((-47924,-16350),(-40380,-16350),(-40924,-37002))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o419"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o426"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o427"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o428">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-18171,-7002), (-4575,-4654))</a:Rect>
<a:ListOfPoints>((-18171,-5828),(-4575,-5828))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>3592</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o429"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o430"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o431"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o432">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-40175,-7488), (-27674,6450))</a:Rect>
<a:ListOfPoints>((-38188,-7488),(-38188,6450),(-27674,6450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o420"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o412"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o433"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o434">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-35301,-6150), (-27749,1618))</a:Rect>
<a:ListOfPoints>((-34364,-6150),(-34364,375),(-27749,375))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o420"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o416"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o435"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o436">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-33908,-7303), (-23096,-4955))</a:Rect>
<a:ListOfPoints>((-33908,-6129),(-23096,-6129))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o420"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o429"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o437"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o438">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-49243,-7991), (-35283,-5643))</a:Rect>
<a:ListOfPoints>((-35283,-6817),(-49243,-6817))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o420"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o439"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o440"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o441">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576513101</a:ModificationDate>
<a:Rect>((-51495,-38554), (-43735,-22147))</a:Rect>
<a:ListOfPoints>((-44957,-38554),(-44064,-23321),(-51495,-23321))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o426"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o442"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o443"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o444">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-58724,-24140), (-51896,-6961))</a:Rect>
<a:ListOfPoints>((-55209,-22966),(-58724,-22966),(-58724,-6961),(-51896,-6961))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o442"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o439"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o445"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o446">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576513087</a:ModificationDate>
<a:Rect>((-27085,-43381), (-24884,-25245))</a:Rect>
<a:ListOfPoints>((-25831,-25245),(-24994,-43381))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o447"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o426"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o448"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o449">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:SourceTextOffset>(1537, 688)</a:SourceTextOffset>
<a:Rect>((-24991,-22853), (-21355,-17603))</a:Rect>
<a:ListOfPoints>((-23904,-17603),(-23904,-22853))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o423"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o447"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o450"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o451">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-62189,-2883), (-39437,-375))</a:Rect>
<a:ListOfPoints>((-62189,-1549),(-50936,-1549),(-50936,-1709),(-39437,-1709))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>4194432</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N
SOURCE 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o452"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o420"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o453"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o420">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-41092,-11545), (-31308,-756))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o454"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o419">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-51299,-18186), (-45751,-14365))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o455"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o423">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-25274,-18561), (-19726,-14740))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o456"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o426">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576513087</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-48026,-49753), (-21274,-30348))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o457"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o413">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-16125,-671), (-7269,8170))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o458"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o416">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-28760,-1161), (-23587,2660))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o459"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o412">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-28724,4839), (-23176,8660))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o460"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o429">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-27146,-7986), (-16204,-4165))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o461"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o430">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-9201,-9410), (351,-3641))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o462"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o439">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-53290,-9092), (-44356,-4297))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o463"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o442">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-56176,-26303), (-47474,-20534))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o464"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o447">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-26545,-26114), (-21746,-22293))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o465"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o452">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-71434,-5009), (-59360,1734))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
QDNM 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o466"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o467"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o467">
<a:ObjectID>46C5BFAB-9482-440D-9055-5B4C37DE74B5</a:ObjectID>
<a:Name>1. Cas d&#39;Utilisation - Planning</a:Name>
<a:Code>1__Cas_d_Utilisation___Planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {684E4FF0-FFE0-47B8-90BD-1B7325280FB9}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o468">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-11943,41361), (40198,28079))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o469">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-11628,20602), (41750,-36674))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>128</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o470">
<a:Text>Reservation entrainements</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-10672,38036), (513,41634))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:TextSymbol Id="o471">
<a:Text>Assistance planning</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-12569,21057), (-1136,17459))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:UseCaseAssociationSymbol Id="o472">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((4517,33870), (31082,33970))</a:Rect>
<a:ListOfPoints>((31082,33870),(4517,33870))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o473"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o474"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o475"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o476">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-125,2801), (31660,12439))</a:Rect>
<a:ListOfPoints>((31660,2801),(31660,12439),(-125,12439))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o478"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o479"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o480">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((3010,4204), (31742,6251))</a:Rect>
<a:ListOfPoints>((31742,4204),(31742,6251),(3010,6251))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o481"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o482"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o483">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((887,-746), (32072,4039))</a:Rect>
<a:ListOfPoints>((32072,4039),(32072,-746),(887,-746))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o484"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o485"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o486">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((1960,-5862), (31990,4121))</a:Rect>
<a:ListOfPoints>((31990,4121),(31990,-5862),(1960,-5862))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o487"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o488"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o489">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((1877,-13782), (31495,3544))</a:Rect>
<a:ListOfPoints>((31495,3544),(31495,-13782),(1877,-13782))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o490"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o491"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:UseCaseAssociationSymbol Id="o492">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((4351,-20550), (31651,2400))</a:Rect>
<a:ListOfPoints>((31651,2400),(31651,-20550),(4351,-20550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o477"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o493"/>
</c:DestinationSymbol>
<c:Object>
<o:UseCaseAssociation Ref="o494"/>
</c:Object>
</o:UseCaseAssociationSymbol>
<o:DependencySymbol Id="o495">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-4761,-30450), (263,-21975))</a:Rect>
<a:ListOfPoints>((-599,-21975),(-599,-25762),(-3899,-25762),(-3899,-30450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>CENTER 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:UseCaseSymbol Ref="o493"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:UseCaseSymbol Ref="o496"/>
</c:DestinationSymbol>
<c:Object>
<o:Dependency Ref="o497"/>
</c:Object>
</o:DependencySymbol>
<o:UseCaseSymbol Id="o474">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-7940,30510), (8455,35909))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o498"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o478">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5234,9750), (7262,15149))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o499"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o484">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-2411,-2625), (5486,2774))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o500"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o487">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1486,-9224), (7411,-3825))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o501"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o481">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-1987,3450), (5212,8849))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o502"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o490">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-911,-16296), (7586,-10897))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o503"/>
</c:Object>
</o:UseCaseSymbol>
<o:ActorSymbol Id="o473">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((28847,33004), (33646,36603))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Shortcut Ref="o504"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o477">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((29425,1482), (34224,5081))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Shortcut Ref="o505"/>
</c:Object>
</o:ActorSymbol>
<o:UseCaseSymbol Id="o493">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-4122,-24073), (7274,-18674))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o506"/>
</c:Object>
</o:UseCaseSymbol>
<o:UseCaseSymbol Id="o496">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-9299,-35325), (-2100,-29926))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>10473471</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>16</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:UseCase Ref="o507"/>
</c:Object>
</o:UseCaseSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:Classes>
<o:Class Id="o454">
<a:ObjectID>30D91730-7B8B-4822-B9B1-61D6A58BD909</a:ObjectID>
<a:Name>Match</a:Name>
<a:Code>Match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {3C38294D-4BBA-442E-A9F0-7F5B5EA664AD}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o508">
<a:ObjectID>B4C34B4A-1F64-4F7A-BE8B-898EEFF4662E</a:ObjectID>
<a:Name>idMatch</a:Name>
<a:Code>idMatch</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {94D4EAC6-3452-43DE-BDBE-1665F050C46A}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o509">
<a:ObjectID>CEA6A5D7-3F72-4A56-B602-B38FE7204681</a:ObjectID>
<a:Name>date</a:Name>
<a:Code>date</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5EA96C18-7D8F-487D-8A1D-5ED94F4AFF2A}
DAT 1576512834</a:History>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o510">
<a:ObjectID>9416732C-68E6-48CC-803A-AED4C0FF1A74</a:ObjectID>
<a:Name>horaire</a:Name>
<a:Code>horaire</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {74B10D01-1E37-41ED-A49F-C74EA07267FC}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o511">
<a:ObjectID>42E89CAE-636B-438C-B3EE-1BD1F0AF85EC</a:ObjectID>
<a:Name>score</a:Name>
<a:Code>score</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {20971DFF-06FF-4AE9-B4B0-4441AAA583CB}
DAT 1576512834</a:History>
<a:DataType>int[]</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>*</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o512">
<a:ObjectID>F1A835D6-0D2F-4DE7-8F84-1126207AD066</a:ObjectID>
<a:Name>indexGagnant</a:Name>
<a:Code>indexGagnant</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2849A47C-5060-495B-ABA0-6364F73172F6}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o513">
<a:ObjectID>E8137C0A-5A5F-49FF-B6E2-50F542F11448</a:ObjectID>
<a:Name>SetScore</a:Name>
<a:Code>setScore</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {82D2D4AD-EEEC-4AE6-9F98-4D2EEFEDE7A8}
DAT 1576512834</a:History>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o514">
<a:ObjectID>6491AC6F-3279-46F6-B719-B3301352EF74</a:ObjectID>
<a:Name>deplacer</a:Name>
<a:Code>deplacer</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A1EE337C-7983-4D51-B04F-1C0AFF701690}
DAT 1576512834</a:History>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o515">
<a:ObjectID>12139898-B19F-4027-B61C-0A1EE6A46D89</a:ObjectID>
<a:Name>modifier</a:Name>
<a:Code>modifier</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AB34D354-3374-42E8-A79B-B6D0A921E1D5}
DAT 1576512834</a:History>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o455">
<a:ObjectID>1C14FD0D-937A-413F-9EC0-B7456BAFFE25</a:ObjectID>
<a:Name>Match Simple</a:Name>
<a:Code>MatchSimple</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {FA3CB00F-7D2F-4764-80A8-09C75BF6CD5B}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o456">
<a:ObjectID>B80C1257-1929-4225-9762-12B0ED429B0B</a:ObjectID>
<a:Name>Match Double</a:Name>
<a:Code>MatchDouble</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F7BB1605-6259-4E3F-B2DF-2667796EFDE6}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o457">
<a:ObjectID>B1F4EE31-3CD5-40F8-B59B-82BF3A28CA7F</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C2B8F3C6-2CA0-4CAC-89A0-6CE5FE0D6273}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o516">
<a:ObjectID>8503059E-C403-4D6B-AB7A-8062AA343A4C</a:ObjectID>
<a:Name>idJoueur</a:Name>
<a:Code>idJoueur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BEA35800-2F85-4773-AAE7-371CE7A1C147}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o517">
<a:ObjectID>ECA16C71-027F-4959-B61D-8CA371A00061</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5365F4A0-8701-461A-953E-D04778E4862B}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o518">
<a:ObjectID>640A17E8-A792-432A-9C9E-4252B70E7BA4</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {53E657D5-A08A-4382-9161-222D24F99B40}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o519">
<a:ObjectID>B032357A-5E7D-443F-91DE-3F6A9F792151</a:ObjectID>
<a:Name>ATP</a:Name>
<a:Code>atp</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {758A60CD-7E1E-4A87-B066-7A5AC51433EA}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o520">
<a:ObjectID>22000C56-74A0-4969-A2EC-3CA12E81F759</a:ObjectID>
<a:Name>nationalite</a:Name>
<a:Code>nationalite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E20FC741-A0E0-426B-858B-ACD8A094AB96}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o521">
<a:ObjectID>FECFE764-8B99-4DDD-B5EE-5B4385F3BC18</a:ObjectID>
<a:Name>demanderEntrainement</a:Name>
<a:Code>demanderEntrainement</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2254C8C9-D4B3-471E-929C-20FF2BA5D2CD}
DAT 1576512834</a:History>
<a:ReturnType>int</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
<o:Operation Id="o522">
<a:ObjectID>9BF4EC74-8FF8-4964-8263-10A1FE995CBE</a:ObjectID>
<a:Name>getIdJoueur</a:Name>
<a:Code>getIdJoueur</a:Code>
<a:CreationDate>1576513071</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>int</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o516"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o523">
<a:ObjectID>7BF6E292-702B-47C5-A852-E8B86B3CD4B8</a:ObjectID>
<a:Name>setIdJoueur</a:Name>
<a:Code>setIdJoueur</a:Code>
<a:CreationDate>1576513071</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o524">
<a:ObjectID>0C6EB169-73CF-4C1C-B893-7474300BDE52</a:ObjectID>
<a:Name>newIdJoueur</a:Name>
<a:Code>newIdJoueur</a:Code>
<a:CreationDate>1576513071</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>int</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o516"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o525">
<a:ObjectID>DC201C9C-0D7F-48B3-807B-9B60EDFE8307</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1576513073</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Copy constructor</a:Stereotype>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o526">
<a:ObjectID>34C326BF-590E-4A44-9776-5001A89F6E1E</a:ObjectID>
<a:Name>oldJoueur</a:Name>
<a:Code>oldJoueur</a:Code>
<a:CreationDate>1576513073</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>Joueur</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
<c:ObjectDataType>
<o:Class Ref="o457"/>
</c:ObjectDataType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Class Ref="o457"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o527">
<a:ObjectID>B9864602-E33A-4C69-9A85-E0532180CAC0</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1576513074</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513082</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Constructor</a:Stereotype>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Class Ref="o457"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o528">
<a:ObjectID>2B726968-E5DD-439C-A87B-0B577B8E5F48</a:ObjectID>
<a:Name>getNom</a:Name>
<a:Code>getNom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>String</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o517"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o529">
<a:ObjectID>FE0FDD29-80F1-41B2-BB5F-2CE7B06903BD</a:ObjectID>
<a:Name>setNom</a:Name>
<a:Code>setNom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o530">
<a:ObjectID>EEAE706C-8A5C-4C33-8E56-8CC033FA3EF8</a:ObjectID>
<a:Name>newNom</a:Name>
<a:Code>newNom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>String</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o517"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o531">
<a:ObjectID>17942F5F-0CC1-4971-9FEA-A5D3A8324574</a:ObjectID>
<a:Name>getPrenom</a:Name>
<a:Code>getPrenom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>String</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o518"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o532">
<a:ObjectID>B8E36923-647C-4EB7-8F44-DAD9113883E2</a:ObjectID>
<a:Name>setPrenom</a:Name>
<a:Code>setPrenom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o533">
<a:ObjectID>1352FC0F-4994-415A-9B66-9E9A36EB3987</a:ObjectID>
<a:Name>newPrenom</a:Name>
<a:Code>newPrenom</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>String</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o518"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o534">
<a:ObjectID>40134070-C394-457B-8FB2-4FE7D8CAFAAE</a:ObjectID>
<a:Name>getAtp</a:Name>
<a:Code>getAtp</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>int</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o519"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o535">
<a:ObjectID>D6BC8872-9FB6-40D5-98EC-00F29ECCF763</a:ObjectID>
<a:Name>setAtp</a:Name>
<a:Code>setAtp</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o536">
<a:ObjectID>535EA97D-8D9F-4184-82E3-530438D1A39E</a:ObjectID>
<a:Name>newATP</a:Name>
<a:Code>newAtp</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>int</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o519"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o537">
<a:ObjectID>F9E4A7D0-DE24-4363-8EBA-6EAC7606C0F0</a:ObjectID>
<a:Name>getNationalite</a:Name>
<a:Code>getNationalite</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Getter</a:Stereotype>
<a:ReturnType>String</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:InfluentObject>
<o:Attribute Ref="o520"/>
</c:InfluentObject>
</o:Operation>
<o:Operation Id="o538">
<a:ObjectID>D4712F53-DB72-4496-AEBD-D9B5D559F96F</a:ObjectID>
<a:Name>setNationalite</a:Name>
<a:Code>setNationalite</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Stereotype>Setter</a:Stereotype>
<a:ReturnType>void</a:ReturnType>
<a:Automatic>1</a:Automatic>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
<c:Parameters>
<o:Parameter Id="o539">
<a:ObjectID>AD9F2B2D-B923-499A-92E0-4705E4F3358A</a:ObjectID>
<a:Name>newNationalite</a:Name>
<a:Code>newNationalite</a:Code>
<a:CreationDate>1576513109</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576513110</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:Parameter.DataType>String</a:Parameter.DataType>
<a:ParameterType>I</a:ParameterType>
</o:Parameter>
</c:Parameters>
<c:InfluentObject>
<o:Attribute Ref="o520"/>
</c:InfluentObject>
</o:Operation>
</c:Operations>
</o:Class>
<o:Class Id="o458">
<a:ObjectID>8200F899-16AF-45B1-9653-AECC4D8B6B40</a:ObjectID>
<a:Name>Arbitre</a:Name>
<a:Code>Arbitre</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {EFF7D607-4FB2-40C6-A3AB-CAADCD5E962D}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:Classifier.Abstract>1</a:Classifier.Abstract>
<c:Attributes>
<o:Attribute Id="o540">
<a:ObjectID>A505B6AF-22ED-4237-B643-99FA560F4D84</a:ObjectID>
<a:Name>idArbitre</a:Name>
<a:Code>idArbitre</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {78EC6E6B-63B0-4E7B-B267-5C7AF275FA2C}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o541">
<a:ObjectID>E39DE784-922C-47EF-88D5-E3A0CB0EF18D</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6CAF6F5C-D347-4B58-B5B0-00C8E768EC36}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o542">
<a:ObjectID>1F723A4D-523E-4A6E-9896-725F540C61C1</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {474CD38C-99FC-4AEA-8432-48942A43A506}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o543">
<a:ObjectID>BD1BDE32-CDAD-40F9-89AE-7C8AA64CDFF8</a:ObjectID>
<a:Name>nationalité</a:Name>
<a:Code>nationalite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0BA1B9E3-EEED-4001-95CA-1B1EB3580576}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o544">
<a:ObjectID>6C74B226-084F-4187-A6F8-DCA606146C69</a:ObjectID>
<a:Name>catégorie</a:Name>
<a:Code>categorie</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {141A8652-6D38-42D3-A5A3-71044AD442AF}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o459">
<a:ObjectID>954D14CF-F02C-45AF-AB47-10C87CD8BB4B</a:ObjectID>
<a:Name>Arbitre Ligne</a:Name>
<a:Code>ArbitreLigne</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A58F7127-4D15-49E4-A496-863318448A27}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o460">
<a:ObjectID>E25C88AE-534C-4878-9D4B-E9CF7CA203F7</a:ObjectID>
<a:Name>Arbitre Chaise</a:Name>
<a:Code>ArbitreChaise</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0DB7503A-B877-4A96-81C6-7822D6C088EF}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o461">
<a:ObjectID>60FF0136-346D-440B-B608-73266F3219DE</a:ObjectID>
<a:Name>Equipe Ramasseurs</a:Name>
<a:Code>EquipeRamasseurs</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BE35FF5A-0864-4A13-B97C-4F75EF2818E0}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o545">
<a:ObjectID>D9E6E590-10DA-468F-B16A-13CEA9AC0D29</a:ObjectID>
<a:Name>idEquipeRamasseur</a:Name>
<a:Code>idEquipeRamasseur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {79FA3526-48B4-4691-B82D-7BA01125303C}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o462">
<a:ObjectID>0DAB9301-80E6-4BD1-9E95-E5518A269500</a:ObjectID>
<a:Name>Ramasseur</a:Name>
<a:Code>Ramasseur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2C3C7175-878B-4084-82B7-8CC70139D96D}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o546">
<a:ObjectID>118517BD-BBDD-4EA3-BFCC-322550BBAEBE</a:ObjectID>
<a:Name>idRamasseur</a:Name>
<a:Code>idRamasseur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {84E0A021-EF36-4E7C-96C5-0055F999398F}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o547">
<a:ObjectID>2DE42044-E432-4AF6-A0B3-8A1A152D322B</a:ObjectID>
<a:Name>nom</a:Name>
<a:Code>nom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1CD9D8DB-EFFF-417D-A1B9-51DA62B5C150}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o548">
<a:ObjectID>5FB540B0-8ECD-4B87-B28E-2C031505739E</a:ObjectID>
<a:Name>prenom</a:Name>
<a:Code>prenom</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {3C46E87A-B182-4BB1-81DB-82B4F55A813D}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o463">
<a:ObjectID>7D02542F-7025-48B8-A188-1947891CE06A</a:ObjectID>
<a:Name>Court</a:Name>
<a:Code>Court</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {927C51A2-E870-49AF-A656-9C4CEC44C35A}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o549">
<a:ObjectID>21275C29-3800-481E-AF91-9B5B1AB9B431</a:ObjectID>
<a:Name>idCourt</a:Name>
<a:Code>idCourt</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1F28A0E4-7CC5-4604-876A-7D38304D2121}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o550">
<a:ObjectID>2ADFBC65-F92B-41ED-9E98-D6F6BDADF4A2</a:ObjectID>
<a:Name>principal</a:Name>
<a:Code>principal</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8AA9F72D-80B9-4841-A8EB-D3258F45F3FF}
DAT 1576512834</a:History>
<a:DataType>boolean</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o464">
<a:ObjectID>55004C49-AE7D-4F7C-88A3-07CB96B44D2E</a:ObjectID>
<a:Name>Entrainement</a:Name>
<a:Code>Entrainement</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {80C3ABDD-39F6-4460-BDAF-424D2F9281FA}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o551">
<a:ObjectID>9E92CCEE-2344-4404-809E-A801EB0EC4C6</a:ObjectID>
<a:Name>date</a:Name>
<a:Code>date</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {321EC2E5-3BE5-4FEB-B04C-452D06FAD65A}
DAT 1576512834</a:History>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o552">
<a:ObjectID>F5F66A1F-6C9E-4520-A99A-3C734C5DCC9E</a:ObjectID>
<a:Name>heureDebut</a:Name>
<a:Code>heureDebut</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AFD57AAA-2E4E-4976-AEB6-E75F6534B967}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o553">
<a:ObjectID>CE49AA38-CECD-4DF3-9CAD-31ACB0BE2882</a:ObjectID>
<a:Name>heureFin</a:Name>
<a:Code>heureFin</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {1485ADCC-2DAA-493C-935C-E4022D8AC2DA}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o465">
<a:ObjectID>C67503DC-4A21-437C-8A9F-090A5DB4AFAA</a:ObjectID>
<a:Name>DuoJoueur</a:Name>
<a:Code>DuoJoueur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {80B3E032-066F-4250-923B-2B7349BD2732}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
</o:Class>
<o:Class Id="o466">
<a:ObjectID>F6C8ADFD-F016-4D66-8707-7028DB5DF9BC</a:ObjectID>
<a:Name>Planning</a:Name>
<a:Code>Planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {67A5D4D1-AC45-4D3A-B7F7-0CABB7277124}
DAT 1576512834</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o554">
<a:ObjectID>040ABEF2-355B-459C-B34F-68B074F86EE2</a:ObjectID>
<a:Name>IdPlanning</a:Name>
<a:Code>idPlanning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E8AE4353-8F93-4BF5-8AC7-E67FA3806C0E}
DAT 1576512834</a:History>
<a:DataType>int</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o555">
<a:ObjectID>FCD7FFFE-7E31-4362-B648-EABB77FCAC24</a:ObjectID>
<a:Name>dateDebut</a:Name>
<a:Code>dateDebut</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6C7AFE51-93B1-4421-B599-BC17C486D1D3}
DAT 1576512834</a:History>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o556">
<a:ObjectID>834A4C72-571A-47C7-84ED-97E138FA7E83</a:ObjectID>
<a:Name>dateFin</a:Name>
<a:Code>dateFin</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {EE60769C-E02A-49DA-B0F2-01009C2E9601}
DAT 1576512834</a:History>
<a:DataType>Date</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o557">
<a:ObjectID>55EEAA00-B44C-46FD-B2BA-65F256482802</a:ObjectID>
<a:Name>Libelle</a:Name>
<a:Code>libelle</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A0E6176F-4453-4BCE-AD1A-1BC1608334EC}
DAT 1576512834</a:History>
<a:DataType>String</a:DataType>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o558">
<a:ObjectID>FE74A958-792C-4C53-90B8-BC191FD0995B</a:ObjectID>
<a:Name>genererPlanning</a:Name>
<a:Code>genererPlanning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E176B569-B73C-4CE2-82ED-14F8C55B9C20}
DAT 1576512834</a:History>
<a:ReturnType>Match[]</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o427">
<a:ObjectID>2D9023F2-BD03-41C5-94A2-1F8F2FC072D0</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>association1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {52F6D3FC-3019-4693-B3B5-392A1EC157ED}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>0..5</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o457"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o455"/>
</c:Object2>
</o:Association>
<o:Association Id="o431">
<a:ObjectID>4104301F-414C-4931-99FD-72DAF5D04CC9</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>association3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2B04FC53-99EC-4D84-8795-21EF52E65BE7}
DAT 1576512834</a:History>
<a:RoleAIndicator>C</a:RoleAIndicator>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>6</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o462"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o461"/>
</c:Object2>
</o:Association>
<o:Association Id="o433">
<a:ObjectID>D3447D7A-6FF7-4149-928D-0F701A57D55C</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>association4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0AECB810-2BF5-4D57-8952-CDE538F8A705}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>0..4</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o460"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o454"/>
</c:Object2>
</o:Association>
<o:Association Id="o435">
<a:ObjectID>63D139DD-2F20-4AFB-97E2-D78BA20BD971</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>association5</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {AB0D7C82-DF98-4B66-8533-F604606ACEF7}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>8</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o459"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o454"/>
</c:Object2>
</o:Association>
<o:Association Id="o437">
<a:ObjectID>482AD5E6-502B-49A0-A5F4-631BE8679DCB</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>association6</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F642972E-D8CF-4CE3-85DF-5E81221B5E92}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o461"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o454"/>
</c:Object2>
</o:Association>
<o:Association Id="o440">
<a:ObjectID>2BCFAC64-D847-4C03-9A81-250DB00F21BF</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>association7</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0DDDC26A-CE5C-4FFD-8721-56B7ED4271CB}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o463"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o454"/>
</c:Object2>
</o:Association>
<o:Association Id="o443">
<a:ObjectID>84A294BB-3D8D-421A-82E3-C573819AD03C</a:ObjectID>
<a:Name>Association_8</a:Name>
<a:Code>association8</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6113622F-DA0E-407A-A9D7-EC265FBB34F2}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>*</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o464"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o457"/>
</c:Object2>
</o:Association>
<o:Association Id="o445">
<a:ObjectID>74C42087-3175-4C7B-8401-C3977811F0CD</a:ObjectID>
<a:Name>Association_9</a:Name>
<a:Code>association9</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {5523D464-8B75-4ED2-A51D-E5C95698A127}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o463"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o464"/>
</c:Object2>
</o:Association>
<o:Association Id="o448">
<a:ObjectID>132E49A0-FB2E-4B6C-AD76-69351BD7C71D</a:ObjectID>
<a:Name>Association_10</a:Name>
<a:Code>association10</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {56EE0955-DAAE-468B-94B8-390CA26FA1B4}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o457"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o465"/>
</c:Object2>
</o:Association>
<o:Association Id="o450">
<a:ObjectID>9E81A1BF-131B-4EBB-A789-B12254EEF91D</a:ObjectID>
<a:Name>Association_11</a:Name>
<a:Code>association11</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {361D830A-AE60-4570-A9C0-87CD2365FAF5}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>0..4</a:RoleAMultiplicity>
<a:RoleBMultiplicity>2</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,224={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,17=java.util.HashSet

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o465"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o456"/>
</c:Object2>
</o:Association>
<o:Association Id="o453">
<a:ObjectID>951548D1-F73F-428C-AC66-4C7E920E8573</a:ObjectID>
<a:Name>Association_14</a:Name>
<a:Code>association14</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {90556AAD-4933-4541-8661-43D3BC8CBB45}
DAT 1576512834</a:History>
<a:RoleAMultiplicity>*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1</a:RoleBMultiplicity>
<a:ExtendedAttributesText>{0DEDDB90-46E2-45A0-886E-411709DA0DC9},Java,276={72FA5C48-5524-4DF7-8187-ABB19AB5AF9E},roleAContainer,6=&lt;None&gt;
{F6FFC71C-C472-4261-A710-B0BCC0BF4D58},roleAImplementationClass,6=&lt;None&gt;
{C11C9F66-6453-43A2-8824-6654518CF65A},roleBImplementationClass,6=&lt;None&gt;
{78C31404-0EE5-4FD0-9038-EE396B305F05},roleBContainer,6=&lt;None&gt;

</a:ExtendedAttributesText>
<c:Object1>
<o:Class Ref="o454"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o466"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:Generalizations>
<o:Generalization Id="o414">
<a:ObjectID>771ED10D-7E28-4546-A33F-ED8F34435B4D</a:ObjectID>
<a:Name>Generalisation_1</a:Name>
<a:Code>Generalisation_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {96F3CF05-8DC4-482D-A460-A1C6BBE8689D}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o458"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o460"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o417">
<a:ObjectID>0874D2F8-9856-49BB-BCA5-376BE30A99FD</a:ObjectID>
<a:Name>Generalisation_2</a:Name>
<a:Code>Generalisation_2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {30678D06-644C-4CCF-ACC2-EFCFE766B0DB}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o458"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o459"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o421">
<a:ObjectID>6A949723-19D2-42DB-851F-38FB21ACD4A1</a:ObjectID>
<a:Name>Generalisation_3</a:Name>
<a:Code>Generalisation_3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8553E124-8BA1-4DAF-B401-C7314CC8146B}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o454"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o455"/>
</c:Object2>
</o:Generalization>
<o:Generalization Id="o424">
<a:ObjectID>6F4B8CF9-8955-4F7C-B1C1-C906C301FF68</a:ObjectID>
<a:Name>Generalisation_4</a:Name>
<a:Code>Generalisation_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {66582669-18B8-4958-B29B-DCE735733053}
DAT 1576512834</a:History>
<c:Object1>
<o:Class Ref="o454"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o456"/>
</c:Object2>
</o:Generalization>
</c:Generalizations>
<c:Dependencies>
<o:Dependency Id="o497">
<a:ObjectID>69276215-1CAB-4374-B9FB-675AEFE0BD06</a:ObjectID>
<a:Name>Dependance_1</a:Name>
<a:Code>Dependance_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {9D964956-3D08-425B-B904-20A9383FE96A}
DAT 1576512834</a:History>
<a:Stereotype>include</a:Stereotype>
<c:Object1>
<o:UseCase Ref="o507"/>
</c:Object1>
<c:Object2>
<o:UseCase Ref="o506"/>
</c:Object2>
</o:Dependency>
</c:Dependencies>
<c:Actors>
<o:Shortcut Id="o504">
<a:ObjectID>794AD4C4-02CA-43BD-A817-4379563FBB55</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<a:History>ORG {7E626418-75E1-4134-ADE7-6574A096C196}
DAT 1576512834</a:History>
<a:TargetStereotype/>
<a:TargetID>DCFD99C8-A535-4EBE-8CE3-2C6B39E56CCB</a:TargetID>
<a:TargetClassID>18112101-1A4B-11D1-83D9-444553540000</a:TargetClassID>
<a:TargetPackagePath>&lt;Modèle&gt;</a:TargetPackagePath>
</o:Shortcut>
<o:Shortcut Id="o505">
<a:ObjectID>1ECDC183-041A-4997-8D65-4AAA56170648</a:ObjectID>
<a:Name>Responcable</a:Name>
<a:Code>Responcable</a:Code>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<a:History>ORG {984DD277-0979-4831-B0BD-B3091722FF4D}
DAT 1576512834</a:History>
<a:TargetStereotype/>
<a:TargetID>A07C6453-86AE-442B-91D3-D0164855AD4D</a:TargetID>
<a:TargetClassID>18112101-1A4B-11D1-83D9-444553540000</a:TargetClassID>
<a:TargetPackagePath>&lt;Modèle&gt;</a:TargetPackagePath>
</o:Shortcut>
</c:Actors>
<c:UseCases>
<o:UseCase Id="o498">
<a:ObjectID>C2C3D6A5-5943-4E73-A00D-D88B580CD3F9</a:ObjectID>
<a:Name>Reserver un cours d&#39;entrainement</a:Name>
<a:Code>Reserver_un_cours_d_entrainement</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {7D70D3E5-9AC3-46F2-9AF7-C83A20A08350}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o499">
<a:ObjectID>3A025F64-077B-4124-B873-AAF84744CC59</a:ObjectID>
<a:Name>Creer planning de match</a:Name>
<a:Code>Creer_planning_de_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {92E0A2F6-603C-4366-ABF5-7A7C99C60DC7}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o500">
<a:ObjectID>39CC440E-0D4B-4934-8105-5050A8971FB3</a:ObjectID>
<a:Name>Modifier match</a:Name>
<a:Code>Modifier_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CAF4705C-810B-4FDB-9BB5-915EE738E991}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o501">
<a:ObjectID>BFFC916F-CD3B-4D5A-A7E0-13D1643946E9</a:ObjectID>
<a:Name>Supprimer match</a:Name>
<a:Code>Supprimer_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {46158188-9A64-40D7-BD59-3F6F35C529F5}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o502">
<a:ObjectID>763B338A-4DC9-4C2B-A40A-495B23EF0212</a:ObjectID>
<a:Name>Inserer match</a:Name>
<a:Code>Inserer_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BC68BEAD-278A-429E-B453-EE4C92BB9C48}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o503">
<a:ObjectID>AA2B3140-835E-42EE-B18C-3AEB1D9837F2</a:ObjectID>
<a:Name>Déplacrer match</a:Name>
<a:Code>Deplacrer_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8D7C01EC-DA74-4035-AFFD-012CDB078B2F}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o506">
<a:ObjectID>D29D317B-3868-4245-9BEB-7EB0013B8A98</a:ObjectID>
<a:Name>Saisir feuille de match</a:Name>
<a:Code>Saisir_feuille_de_match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CD43E7BB-B314-4476-BA16-BDF715404127}
DAT 1576512834</a:History>
</o:UseCase>
<o:UseCase Id="o507">
<a:ObjectID>8EFC3E84-3239-4DC4-8A2A-594888B45582</a:ObjectID>
<a:Name>Rentrer score</a:Name>
<a:Code>Rentrer_score</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {8CBB6D44-C8A2-4727-BF7C-C76ACC5FA562}
DAT 1576512834</a:History>
</o:UseCase>
</c:UseCases>
<c:UseCaseAssociations>
<o:UseCaseAssociation Id="o475">
<a:ObjectID>0A1A78FC-8D3F-472E-A6CB-36AD69FE79C8</a:ObjectID>
<a:Name>Association_1</a:Name>
<a:Code>Association_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {149AF171-A199-4CF1-8148-4991B8CF9964}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o498"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o504"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o479">
<a:ObjectID>A2390461-76B1-49FE-9960-BEA6E9C4E149</a:ObjectID>
<a:Name>Association_2</a:Name>
<a:Code>Association_2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6193907A-1C74-4BA3-A587-0F9F5F6F97CC}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o499"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o482">
<a:ObjectID>EDAA1FC4-7A06-4223-AB90-6E2D7FE29507</a:ObjectID>
<a:Name>Association_3</a:Name>
<a:Code>Association_3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {6B1EF4E7-A898-4884-8A28-07F6A14D7639}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o502"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o485">
<a:ObjectID>7329F8B7-AD74-4AA4-8683-03433973088F</a:ObjectID>
<a:Name>Association_4</a:Name>
<a:Code>Association_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E4EF1E9B-DB74-4834-B4E2-0DEB4D84112D}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o500"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o488">
<a:ObjectID>6D1FEC3B-735B-483A-B739-68E0666865C3</a:ObjectID>
<a:Name>Association_5</a:Name>
<a:Code>Association_5</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {B9C77AFA-D76B-47B0-B244-8DE9756682C6}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o501"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o491">
<a:ObjectID>6F2101F6-D78C-4B51-B444-178BAFDCEBD5</a:ObjectID>
<a:Name>Association_6</a:Name>
<a:Code>Association_6</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {567F2486-2216-4CA4-AE9F-A5634E15FF22}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o503"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
<o:UseCaseAssociation Id="o494">
<a:ObjectID>6504B2DE-2DDA-4C60-9A5F-45B537A63FD6</a:ObjectID>
<a:Name>Association_7</a:Name>
<a:Code>Association_7</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {CA4BCDB6-8149-40A1-A7B6-1FE27D187CE3}
DAT 1576512834</a:History>
<c:Object1>
<o:UseCase Ref="o506"/>
</c:Object1>
<c:Object2>
<o:Shortcut Ref="o505"/>
</c:Object2>
</o:UseCaseAssociation>
</c:UseCaseAssociations>
<c:Flows>
<o:ActivityFlow Id="o559">
<a:ObjectID>E7A4406F-FEF6-4AA1-881A-93E8BFF801E1</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o560"/>
</c:Object1>
<c:Object2>
<o:Start Ref="o561"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o562">
<a:ObjectID>8369E3B8-8FC2-4D00-9BD2-4C31ADF7E801</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o563"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o564"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o565">
<a:ObjectID>93BE1185-E4C0-4BA6-8F4A-4076E9A61CA7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o566"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o564"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o567">
<a:ObjectID>3A0A7F6A-0E08-4817-BB18-4309FF02B094</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o568"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o563"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o569">
<a:ObjectID>A0D4003F-1D72-4A18-8D9B-B2744D26E06F</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o568"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o566"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o570">
<a:ObjectID>6C209519-66E1-4C65-8E94-FFB297B456C5</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o571"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o572"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o573">
<a:ObjectID>38BF9261-4EDA-44BF-9F4D-5F06F801B8A4</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o574"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o572"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o575">
<a:ObjectID>2F4B45BB-B70E-4851-9B68-5336A70D77B9</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o576"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o560"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o577">
<a:ObjectID>88CE298F-7C88-4C5E-A7B9-D5B33F0D4B04</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o564"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o576"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o578">
<a:ObjectID>28F9B50F-704C-460B-BDA0-CDD125744AC5</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o579"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o568"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o580">
<a:ObjectID>3B74B915-DD0E-4D3E-BE0D-2A72AE2E1BF7</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o581"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o579"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o582">
<a:ObjectID>C7294B76-6C36-4180-A59A-8EEDF3B04C02</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o583"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o581"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o584">
<a:ObjectID>545FA2B7-F4A9-4EDF-8070-A758588F092A</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o572"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o583"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o585">
<a:ObjectID>6B959902-1E3C-4C87-95B7-77F86CE77205</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o586"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o571"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o587">
<a:ObjectID>F0C2FD49-2624-417F-857A-7217FFC04FF2</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Synchronization Ref="o586"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o574"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o588">
<a:ObjectID>F6C9AD38-56FC-4A37-86AE-4410332635ED</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Activity Ref="o589"/>
</c:Object1>
<c:Object2>
<o:Synchronization Ref="o586"/>
</c:Object2>
</o:ActivityFlow>
<o:ActivityFlow Id="o590">
<a:ObjectID>F00B8032-4173-40D9-9657-D9DB39CFA5F8</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:End Ref="o591"/>
</c:Object1>
<c:Object2>
<o:Activity Ref="o589"/>
</c:Object2>
</o:ActivityFlow>
</c:Flows>
<c:Activities>
<o:Activity Id="o563">
<a:ObjectID>CBC5F93C-55D9-4891-BD08-79EF95B35A0D</a:ObjectID>
<a:Name>Choisir Horaire</a:Name>
<a:Code>Choisir_Horaire</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C03D0802-555F-4ACC-B9A4-281553EBC84E}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o560">
<a:ObjectID>3EEEC7EC-5DAE-4B7F-9F1F-0AAADB941D58</a:ObjectID>
<a:Name>Choisir Jour</a:Name>
<a:Code>Choisir_Jour</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {2EA8B997-1A2E-49F4-A1FF-D257EFDA7F71}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o581">
<a:ObjectID>74A64F8E-DFFB-465D-882A-2E27588AADFA</a:ObjectID>
<a:Name>Selectionner les joueurs</a:Name>
<a:Code>Selectionner_les_joueurs</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {4EB2F7CD-DF58-44F5-A64C-95F55E084560}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o566">
<a:ObjectID>1BF8A3BB-670A-443A-B4A4-F0A021F104D5</a:ObjectID>
<a:Name>Choisir cour</a:Name>
<a:Code>Choisir_cour</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C9735D5E-18E1-4343-B8AB-D73DE15EB843}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o571">
<a:ObjectID>4AFD5A18-EF87-4ADF-B5E9-208607A78490</a:ObjectID>
<a:Name>Selectioner arbitres</a:Name>
<a:Code>Selectioner_arbitres</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {0FC8831C-60E9-45A9-9FF4-7E8AF40F273D}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o574">
<a:ObjectID>1492B315-02D4-4AB5-875A-BB03233ACF6A</a:ObjectID>
<a:Name>Selectoionner rammasseur</a:Name>
<a:Code>Selectoionner_rammasseur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {BF19976F-FC58-4398-A72B-9B0053E6F4D2}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o576">
<a:ObjectID>88FED933-A45B-4316-9A93-B343326A972F</a:ObjectID>
<a:Name>Recuperer disponibilité</a:Name>
<a:Code>Recuperer_disponibilite</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {E3CE2007-4FEC-4492-A158-BD9733DC23DF}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o579">
<a:ObjectID>8741CF18-5E10-4A05-9E87-2ACEF46C28A1</a:ObjectID>
<a:Name>Recupérer disponibilité joueur</a:Name>
<a:Code>Recuperer_disponibilite_joueur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {92867553-0B78-4F3C-BBDF-B0AF6A8BFF59}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o583">
<a:ObjectID>47391E43-AB02-415C-8DE8-8B9C20B99233</a:ObjectID>
<a:Name>Récuperer arbitres possibles</a:Name>
<a:Code>Recuperer_arbitres_possibles</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {92AEC026-DCE5-4D7C-A648-3FA255EEE2AD}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
<o:Activity Id="o589">
<a:ObjectID>EAC926F3-821D-46D2-A544-C948DBCF041F</a:ObjectID>
<a:Name>Confirmer Match</a:Name>
<a:Code>Confirmer_Match</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {05E2531D-A472-464D-8B34-2DF37F282C68}
DAT 1576512834</a:History>
<a:ActionType>UNDEF</a:ActionType>
</o:Activity>
</c:Activities>
<c:Synchronizations>
<o:Synchronization Id="o564">
<a:ObjectID>10048F9D-AE29-4740-AA83-E6AFA9F9E0A7</a:ObjectID>
<a:Name>Synchronisation_1</a:Name>
<a:Code>Synchronisation_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {70F044BA-D5D1-474F-A2C4-FAC0ACFF8A72}
DAT 1576512834</a:History>
</o:Synchronization>
<o:Synchronization Id="o568">
<a:ObjectID>EC31D983-1495-48B2-8344-35CF36CD5323</a:ObjectID>
<a:Name>Synchronisation_2</a:Name>
<a:Code>Synchronisation_2</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {C076DC7C-AE6C-4E90-BF02-A36F71914575}
DAT 1576512834</a:History>
</o:Synchronization>
<o:Synchronization Id="o572">
<a:ObjectID>CB27566C-42D0-4FDD-A659-F4C713D96F51</a:ObjectID>
<a:Name>Synchronisation_3</a:Name>
<a:Code>Synchronisation_3</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D42C5591-948D-4EA7-ACCB-7E92B07F4ADC}
DAT 1576512834</a:History>
</o:Synchronization>
<o:Synchronization Id="o586">
<a:ObjectID>4364C4DE-03FA-482B-A847-F542AD022E59</a:ObjectID>
<a:Name>Synchronisation_4</a:Name>
<a:Code>Synchronisation_4</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {58184E6E-B6A8-46C2-A1A6-44638A7DD38D}
DAT 1576512834</a:History>
</o:Synchronization>
</c:Synchronizations>
<c:ActivityDiagrams>
<o:ActivityDiagram Id="o592">
<a:ObjectID>5E7F8A48-4852-4916-A246-7D73BBAF4F2A</a:ObjectID>
<a:Name>3. Activité Inserer un Match - Planning</a:Name>
<a:Code>3__Activite_Inserer_un_Match___Planning</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {15B0B3A9-9C56-4689-88C3-BA308D05E3D9}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\ACD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=No
Grid size=800
Graphic unit=2
Window color=255 255 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255 255 255
Show Swimlane=Yes
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=Yes
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Activity.Stereotype=Yes
Activity.Comment=No
Activity.ParametersDisplay=Yes
Activity.IconPicture=No
Activity.TextStyle=No
Activity.SubSymbols=Yes
Activity_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ObjectNode.Stereotype=Yes
ObjectNode.Comment=No
ObjectNode.IconPicture=No
ObjectNode.TextStyle=No
ObjectNode_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;Name&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Etat&quot; Attribute=&quot;States&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Variable.Stereotype=Yes
Variable.Comment=No
Variable.IconPicture=No
Variable.TextStyle=No
Variable_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ActivityParameter.DisplayName=Yes
ActivityParameter.InstanceDisplay=No
ActivityParameter.IconPicture=No
ActivityParameter.TextStyle=No
ActivityParameter_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Start.Stereotype=Yes
Start.DisplayName=No
Start.IconPicture=No
Start.TextStyle=No
Start_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
End.Stereotype=Yes
End.DisplayName=No
End.IconPicture=No
End.TextStyle=No
End_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Decision.Stereotype=Yes
Decision.DisplayedExpression=No
Decision.DisplayedExpressionOrName=Yes
Decision.IconPicture=No
Decision.TextStyle=No
Decision_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Expression&quot; Attribute=&quot;DisplayedExpression&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom en l&amp;#39;absence de condition&quot; Attribute=&quot;DisplayedExpressionOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
Synchronization.IconPicture=No
Synchronization.TextStyle=No
Synchronization_SymbolLayout=
OrganizationUnit.Stereotype=Yes
OrganizationUnit.IconPicture=No
OrganizationUnit.TextStyle=No
OrganizationUnit_SymbolLayout=
ActivityFlow.Stereotype=Yes
ActivityFlow_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Condition&quot; Attribute=&quot;DisplayedCondition&quot; Prefix=&quot;[&quot; Suffix=&quot;]&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LCNMFont=Arial,8,N
LCNMFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=-1

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMACTV]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ACDOBST]
STRNFont=Arial,8,N
STRNFont color=0 0 0
NAMEFont=Arial,8,N
NAMEFont color=0 0 0
STATFont=Arial,8,N
STATFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=205 156 156
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMVAR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=3975
Height=3000
Brush color=255 176 176
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMATPM]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=825
Height=800
Brush color=234 181 21
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSTRT]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1200
Height=1200
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMEND]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=No
Keep aspect=Yes
Keep center=Yes
Keep size=Yes
Width=1500
Height=1500
Brush color=255 128 0
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMDCSN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DEXPFont=Arial,8,N
DEXPFont color=0 0 0
DEXNFont=Arial,8,N
DEXNFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=Yes
Keep size=No
Width=6600
Height=4000
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMSYNC]
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=2400
Height=1500
Brush color=0 128 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=0 128 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 192
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMORGN]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=3225
Height=4800
Brush color=233 202 131
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=512
Brush gradient color=200 216 248
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 64
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMFLOW]
SOURCEFont=Arial,8,N
SOURCEFont color=0 0 0
DESTINATIONFont=Arial,8,N
DESTINATIONFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:RectangleSymbol Id="o593">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20399,27843), (10746,-39539))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o594">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((10646,27843), (34604,-39448))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o595">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-20499,27834), (10746,24140))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
</o:RectangleSymbol>
<o:RectangleSymbol Id="o596">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((10646,27835), (34604,24040))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16711680</a:LineColor>
<a:FillColor>16777215</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,8,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:RectangleSymbol>
<o:TextSymbol Id="o597">
<a:Text>Responsable</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8590,24179), (-194,27777))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,14,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:TextSymbol Id="o598">
<a:Text>Systeme</a:Text>
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((19456,24191), (26480,27790))</a:Rect>
<a:TextStyle>4130</a:TextStyle>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>0</a:LineColor>
<a:DashStyle>7</a:DashStyle>
<a:FillColor>0</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontName>Arial,14,N</a:FontName>
<a:ManuallyResized>1</a:ManuallyResized>
</o:TextSymbol>
<o:FlowSymbol Id="o599">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-6968,16050), (-5968,21899))</a:Rect>
<a:ListOfPoints>((-6562,21899),(-6374,16050))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:StartSymbol Ref="o600"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o601"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o559"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o602">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-12278,4763), (-11278,9750))</a:Rect>
<a:ListOfPoints>((-11699,9750),(-11699,7231),(-11858,7231),(-11858,4763))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o603"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o604"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o562"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o605">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((289,5550), (1289,9600))</a:Rect>
<a:ListOfPoints>((751,9600),(751,7231),(826,7231),(826,5550))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o603"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o606"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o565"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o607">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-12686,-412), (-11686,4013))</a:Rect>
<a:ListOfPoints>((-12186,4013),(-12186,-412))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o604"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o608"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o567"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o609">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((776,-562), (1776,4763))</a:Rect>
<a:ListOfPoints>((1276,4763),(1276,-562))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o606"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o608"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o569"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o610">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-14336,-24100), (-13336,-20275))</a:Rect>
<a:ListOfPoints>((-13836,-20275),(-13836,-24100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o611"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o612"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o570"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o613">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((2388,-24100), (3388,-19900))</a:Rect>
<a:ListOfPoints>((2888,-19900),(2888,-24100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o611"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o614"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o573"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o615">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-3374,12450), (25351,16200))</a:Rect>
<a:ListOfPoints>((-3374,16200),(25351,16200),(25351,12450))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o601"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o616"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o575"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o617">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5028,9150), (18676,11775))</a:Rect>
<a:ListOfPoints>((18676,11775),(-5028,11775),(-5028,9150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o616"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o603"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o577"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o618">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5999,-2775), (17551,75))</a:Rect>
<a:ListOfPoints>((-5999,75),(-5999,-2775),(17551,-2775))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o608"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o619"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o578"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o620">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-6299,-9150), (19201,-3000))</a:Rect>
<a:ListOfPoints>((19201,-3000),(19201,-5700),(-6299,-5700),(-6299,-9150))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o619"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o621"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o580"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o622">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-6524,-13800), (15601,-9600))</a:Rect>
<a:ListOfPoints>((-6524,-9600),(-6524,-13800),(15601,-13800))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o621"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o623"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o582"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o624">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5699,-20400), (20101,-14625))</a:Rect>
<a:ListOfPoints>((20101,-14625),(20101,-17325),(-5699,-17325),(-5699,-20400))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o623"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o611"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o584"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o625">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-13811,-27300), (-12811,-24000))</a:Rect>
<a:ListOfPoints>((-13311,-24000),(-13311,-27300))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o612"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o626"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o585"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o627">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((2726,-27000), (3726,-24525))</a:Rect>
<a:ListOfPoints>((3226,-24525),(3226,-27000))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o614"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:BaseSynchronizationSymbol Ref="o626"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o587"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o628">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5411,-32100), (-4411,-27225))</a:Rect>
<a:ListOfPoints>((-4911,-27225),(-4911,-32100))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:BaseSynchronizationSymbol Ref="o626"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ActivitySymbol Ref="o629"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o588"/>
</c:Object>
</o:FlowSymbol>
<o:FlowSymbol Id="o630">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-5140,-37582), (-4140,-32025))</a:Rect>
<a:ListOfPoints>((-4668,-32025),(-4612,-37582))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>1</a:ArrowStyle>
<a:LineColor>16711680</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>SOURCE 0 Arial,8,N
DESTINATION 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActivitySymbol Ref="o629"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:EndSymbol Ref="o631"/>
</c:DestinationSymbol>
<c:Object>
<o:ActivityFlow Ref="o590"/>
</c:Object>
</o:FlowSymbol>
<o:StartSymbol Id="o600">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-7162,21300), (-5963,22499))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:Start Ref="o561"/>
</c:Object>
</o:StartSymbol>
<o:ActivitySymbol Id="o604">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-15562,3875), (-9563,5874))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o563"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o601">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-9037,15200), (-3038,17199))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o560"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o621">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10809,-10350), (-1569,-8351))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o581"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o606">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-1987,3875), (4012,5874))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o566"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o612">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-17183,-24800), (-9592,-22801))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o571"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o614">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2256,-24950), (7884,-22951))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o574"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o616">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((18292,11075), (27233,13074))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o576"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o619">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((15481,-3548), (26897,-1549))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o579"/>
</c:Object>
</o:ActivitySymbol>
<o:ActivitySymbol Id="o623">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((15106,-15149), (25622,-13150))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o583"/>
</c:Object>
</o:ActivitySymbol>
<o:EndSymbol Id="o631">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-5362,-38332), (-3863,-36833))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>33023</a:LineColor>
<a:FillColor>33023</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12632256</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<a:KeepSize>1</a:KeepSize>
<c:Object>
<o:End Ref="o591"/>
</c:Object>
</o:EndSymbol>
<o:BaseSynchronizationSymbol Id="o603">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-14623,8588), (4051,10087))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o564"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o608">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-14475,-712), (3901,787))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o568"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o611">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-16704,-20800), (8102,-19301))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o572"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:BaseSynchronizationSymbol Id="o626">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-17175,-27776), (8306,-26277))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>12615680</a:LineColor>
<a:FillColor>12615680</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>512</a:GradientFillMode>
<a:GradientEndColor>12615680</a:GradientEndColor>
<a:ManuallyResized>1</a:ManuallyResized>
<c:Object>
<o:Synchronization Ref="o586"/>
</c:Object>
</o:BaseSynchronizationSymbol>
<o:ActivitySymbol Id="o629">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8484,-32724), (-1868,-30725))</a:Rect>
<a:LineColor>33023</a:LineColor>
<a:FillColor>8637161</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Activity Ref="o589"/>
</c:Object>
</o:ActivitySymbol>
</c:Symbols>
</o:ActivityDiagram>
</c:ActivityDiagrams>
<c:Starts>
<o:Start Id="o561">
<a:ObjectID>C4B060BA-36CD-4C03-8FA3-53C44DA681B1</a:ObjectID>
<a:Name>Debut_1</a:Name>
<a:Code>Debut_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {853762F5-C5EA-491A-8AD4-2B83DC64C78C}
DAT 1576512834</a:History>
</o:Start>
</c:Starts>
<c:Ends>
<o:End Id="o591">
<a:ObjectID>1D4E6EAC-EF9D-4945-98E5-723A55CBCD48</a:ObjectID>
<a:Name>Fin_1</a:Name>
<a:Code>Fin_1</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {3CAC68B4-BA82-44A3-A413-D427D78D8BD7}
DAT 1576512834</a:History>
</o:End>
</c:Ends>
</o:Package>
</c:Packages>
<c:DefaultDiagram>
<o:UseCaseDiagram Ref="o632"/>
</c:DefaultDiagram>
<c:UseCaseDiagrams>
<o:UseCaseDiagram Id="o632">
<a:ObjectID>030BC394-95BD-4E23-9EB3-6713E241B4FB</a:ObjectID>
<a:Name>Packages</a:Name>
<a:Code>Packages</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {58DDB428-B582-4D52-A5BB-4C8D74662863}
DAT 1576512834</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\UCD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=Yes
Grid size=0
Graphic unit=2
Window color=255, 255, 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255, 255, 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Show Icon=No
Mode=0
Trunc Length=80
Word Length=80
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject.TextStyle=No
ExtendedObject_SymbolLayout=
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.TextStyle=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=
Package.Stereotype=Yes
Package.Comment=No
Package.IconPicture=No
Package.TextStyle=No
Package_SymbolLayout=
Display Model Version=Yes
Generalization.DisplayedStereotype=No
Generalization.DisplayName=No
Generalization.DisplayedRules=No
Generalization_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Dependency.DisplayedStereotype=Yes
Dependency.DisplayName=No
Dependency.DisplayedRules=No
Dependency_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;DisplayedStereotype&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Contrainte&quot; Attribute=&quot;DisplayedRules&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
Actor.Stereotype=Yes
Actor.IconPicture=No
Actor.TextStyle=No
Actor_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
UseCaseAssociation.Stereotype=No
UseCaseAssociation.DisplayName=No
UseCaseAssociation.DisplayDirection=No
UseCaseAssociation_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
UseCase.Stereotype=Yes
UseCase.Comment=No
UseCase.IconPicture=No
UseCase.TextStyle=No
UseCase_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Forcer l&amp;#39;alignement en haut&quot; Attribute=&quot;TextStyle&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LCNMFont=Arial,8,N
LCNMFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=2400
Height=2400
Brush color=255 255 255
Fill Color=No
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDACTR]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=Yes
Keep center=Yes
Keep size=No
Width=4800
Height=3600
Brush color=128 64 64
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDASSC]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\UCDUCAS]
STRNFont=Arial,8,N
STRNFont color=0 0 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0 0 0
LABLFont=Arial,8,N
LABLFont color=0 0 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=7200
Height=5400
Brush color=255 207 159
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=16
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 150 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 0 0
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0 0 0
Line style=2
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 0 0
Shadow color=192 192 192
Shadow=0</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:ExtendedDependencySymbol Id="o633">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8175,13088), (3975,16238))</a:Rect>
<a:ListOfPoints>((-8175,16238),(-1350,16238),(-1350,13088),(3975,13088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o634"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o635"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o636"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o637">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8175,7988), (3675,13088))</a:Rect>
<a:ListOfPoints>((-8175,7988),(3675,7988),(3675,13088))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o638"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o635"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o639"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o640">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-8475,-1649), (4200,-1049))</a:Rect>
<a:ListOfPoints>((-8475,-1349),(4200,-1349))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o641"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o642"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o643"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:ExtendedDependencySymbol Id="o644">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Rect>((-7275,-7762), (4950,-1837))</a:Rect>
<a:ListOfPoints>((-7275,-7762),(4950,-7762),(4950,-1837))</a:ListOfPoints>
<a:CornerStyle>2</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>128</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>OBJXSTR 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ActorSymbol Ref="o645"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:PackageSymbol Ref="o642"/>
</c:DestinationSymbol>
<c:Object>
<o:ExtendedDependency Ref="o646"/>
</c:Object>
</o:ExtendedDependencySymbol>
<o:PackageSymbol Id="o635">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((2250,12338), (7049,15937))</a:Rect>
<a:LineColor>11711154</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Package Ref="o7"/>
</c:Object>
</o:PackageSymbol>
<o:PackageSymbol Id="o642">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((2250,-2737), (7049,862))</a:Rect>
<a:LineColor>11711154</a:LineColor>
<a:FillColor>12648447</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Package Ref="o409"/>
</c:Object>
</o:PackageSymbol>
<o:ActorSymbol Id="o634">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10575,13388), (-5776,16987))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o647"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o641">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10500,-3262), (-5701,337))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o648"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o645">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10200,-10312), (-5401,-6713))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o649"/>
</c:Object>
</o:ActorSymbol>
<o:ActorSymbol Id="o638">
<a:CreationDate>1576512833</a:CreationDate>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:IconMode>-1</a:IconMode>
<a:Rect>((-10350,7463), (-5551,11062))</a:Rect>
<a:LineColor>128</a:LineColor>
<a:LineWidth>1</a:LineWidth>
<a:FillColor>4210816</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<a:KeepAspect>1</a:KeepAspect>
<a:KeepCenter>1</a:KeepCenter>
<c:Object>
<o:Actor Ref="o650"/>
</c:Object>
</o:ActorSymbol>
</c:Symbols>
</o:UseCaseDiagram>
</c:UseCaseDiagrams>
<c:Actors>
<o:Actor Id="o647">
<a:ObjectID>FEC6F1FB-E9D0-4CC5-92C9-0D22CFA7BDE9</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {18AC2CF6-6A46-43AA-BB91-5D4B2C703E3D}
DAT 1576512834</a:History>
</o:Actor>
<o:Actor Id="o648">
<a:ObjectID>DCFD99C8-A535-4EBE-8CE3-2C6B39E56CCB</a:ObjectID>
<a:Name>Joueur</a:Name>
<a:Code>Joueur</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F0FC516A-20FD-478F-9E16-F9A0F3FA3839}
DAT 1576512834</a:History>
</o:Actor>
<o:Actor Id="o649">
<a:ObjectID>A07C6453-86AE-442B-91D3-D0164855AD4D</a:ObjectID>
<a:Name>Responcable</a:Name>
<a:Code>Responcable</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {F947A99C-4325-4786-B62D-E3D3D73FBAE4}
DAT 1576512834</a:History>
</o:Actor>
<o:Actor Id="o650">
<a:ObjectID>1729C55E-B8D1-40A8-BAC4-275F8C3B6997</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {4FDC965D-B5AD-48B9-AD13-7D737886A82C}
DAT 1576512834</a:History>
</o:Actor>
</c:Actors>
<c:OrganizationUnits>
<o:OrganizationUnit Id="o314">
<a:ObjectID>C0CCEC26-F36A-409C-A87D-6D4908C4D0BF</a:ObjectID>
<a:Name>Client</a:Name>
<a:Code>Client</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {A4913730-4591-4EF4-AE92-BCEC83EB4BDC}
DAT 1576512834</a:History>
<a:Stereotype>Rôle</a:Stereotype>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o315">
<a:ObjectID>FA39FB0E-AF5C-44EC-A343-71BA1BF6ECE9</a:ObjectID>
<a:Name>Systeme</a:Name>
<a:Code>Systeme</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {557228AD-E7E1-41A2-87CB-12112C36A9B6}
DAT 1576512834</a:History>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o316">
<a:ObjectID>CC7AED38-96B9-440A-94FC-2A6581375806</a:ObjectID>
<a:Name>Staff</a:Name>
<a:Code>Staff</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {D5BCF03C-5093-43E9-9B02-D4C775F6C859}
DAT 1576512834</a:History>
</o:OrganizationUnit>
<o:OrganizationUnit Id="o317">
<a:ObjectID>AD1F60A4-E977-450B-9AE6-BDCD1F7B7CD1</a:ObjectID>
<a:Name>System</a:Name>
<a:Code>System</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:History>ORG {3A6778F1-612D-4F79-859F-E3687DF07D9E}
DAT 1576512834</a:History>
</o:OrganizationUnit>
</c:OrganizationUnits>
<c:ChildExtendedDependencies>
<o:ExtendedDependency Id="o636">
<a:ObjectID>66FDD7DE-0562-4B49-B84B-35F6B1CFD6C8</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o7"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o647"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o639">
<a:ObjectID>C54A7634-41BF-4673-8FBA-5E040469D3E4</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o7"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o650"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o643">
<a:ObjectID>A540600B-9BF2-40C4-8843-8BBA3BAEB9AD</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o409"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o648"/>
</c:Object2>
</o:ExtendedDependency>
<o:ExtendedDependency Id="o646">
<a:ObjectID>3119F432-824C-472B-BE1C-AEE96FEA0925</a:ObjectID>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<c:Object1>
<o:Package Ref="o409"/>
</c:Object1>
<c:Object2>
<o:Actor Ref="o649"/>
</c:Object2>
</o:ExtendedDependency>
</c:ChildExtendedDependencies>
<c:TargetModels>
<o:TargetModel Id="o651">
<a:ObjectID>DC44691D-170C-48BF-B4C3-C7CA86966A33</a:ObjectID>
<a:Name>Java</a:Name>
<a:Code>Java</a:Code>
<a:CreationDate>1576512831</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512831</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///%_OBJLANG%/java5-j2ee14.xol</a:TargetModelURL>
<a:TargetModelID>0DEDDB90-46E2-45A0-886E-411709DA0DC9</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o5"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o652">
<a:ObjectID>A4A8715C-14B8-4259-A2F9-17E7DD1E85CB</a:ObjectID>
<a:Name>WSDL for Java</a:Name>
<a:Code>WSDLJava</a:Code>
<a:CreationDate>1576512833</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512833</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///%_XEM%/WSDLJ2EE.xem</a:TargetModelURL>
<a:TargetModelID>C8F5F7B2-CF9D-4E98-8301-959BB6E86C8A</a:TargetModelID>
<a:TargetModelClassID>186C8AC3-D3DC-11D3-881C-00508B03C75C</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o6"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o653">
<a:ObjectID>95C55A25-543D-4067-8E09-F036FA2BED4A</a:ObjectID>
<a:Name>OpenTenisJava</a:Name>
<a:Code>OpenTenisJava</a:Code>
<a:CreationDate>1576512834</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576515363</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///.</a:TargetModelURL>
<a:TargetModelID>C3A78955-4511-433C-97FE-B21679B46701</a:TargetModelID>
<a:TargetModelClassID>18112060-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o504"/>
<o:Shortcut Ref="o505"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o654">
<a:ObjectID>E8DD8989-4CBE-4D1E-820A-4EA8E1E43EE5</a:ObjectID>
<a:Name>OpenTenis</a:Name>
<a:Code>OpenTenis</a:Code>
<a:CreationDate>1576512834</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576512834</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///Z|/Mes documents/CPOA/uml/OpenTenis - Conception.moo</a:TargetModelURL>
<a:TargetModelID>CFC1D5AD-E0DF-4218-BC3F-18980C26122F</a:TargetModelID>
<a:TargetModelClassID>18112060-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o655">
<a:ObjectID>1F007642-0B4F-4145-8981-C1B0A9177A95</a:ObjectID>
<a:Name>OpenTenisJava</a:Name>
<a:Code>OPENTENISJAVA</a:Code>
<a:CreationDate>1576513352</a:CreationDate>
<a:Creator>p1800666</a:Creator>
<a:ModificationDate>1576515358</a:ModificationDate>
<a:Modifier>p1800666</a:Modifier>
<a:TargetModelURL>file:///Z|/Mes documents/CPOA/uml/OpenTenisJava.mpd</a:TargetModelURL>
<a:TargetModelID>ED9C452C-1B2E-4CAF-90B6-7C5F7AB6F5E9</a:TargetModelID>
<a:TargetModelClassID>CDE44E21-9669-11D1-9914-006097355D9B</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>